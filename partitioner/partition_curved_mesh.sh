#!/bin/bash

EXEC_PART=src/driver_partition

PATHNAME=`dirname "$1"`
MESHNAME=`basename "$1"`

for i in `seq "$2" "$3"`
do

    echo "Partitioning with ${i} PARTS"
    
    rm -rf ${PATHNAME}/${MESHNAME}_npart${i}
    
    mkdir ${PATHNAME}/${MESHNAME}_npart${i} 

    ${EXEC_PART} -ipart ${PATHNAME}/${MESHNAME}_npart${i}.part \
                 -npart ${i} \
                 -f ${PATHNAME}/${MESHNAME} \
                 -fec ${PATHNAME}/${MESHNAME} \
                 -B ${PATHNAME}/${MESHNAME} \
                 -oprefix "${PATHNAME}/${MESHNAME}_npart${i}/"
    
    cp ${PATHNAME}/${MESHNAME}.bdry \
       ${PATHNAME}/${MESHNAME}_npart${i}/${MESHNAME}.bdry

    echo ""
    
done
