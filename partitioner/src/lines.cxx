/*
 * lines.cxx
 *
 * Functions and data structures related to the lines.
 */

#include "lines.hxx"
#include "mesh.hxx"
#include <math.h>
#include <algorithm>
#include <cstdio>
#include "vector"

/****************************************************************************
 *                                Comparing
 ****************************************************************************/
double const * CmpUniso::wgts;
int CmpUniso::ncells;


/****************************************************************************\
 *                                  LineNode
\****************************************************************************/

PetscErrorCode LineNode::revert()
{
	for (CellNode *it = nodebeg ; it!=NULL ; it = it->next )
	{
		it->prev = it->next;
	}
	nodebeg->next = NULL;
	for (CellNode *it = nodebeg ; it!=nodeend ; it = it->prev )
	{
		it->prev->next = it;
	}

	CellNode *tmp;
	tmp = nodeend;
	nodeend = nodebeg;
	nodebeg = tmp;

	return 0;

}

PetscErrorCode LineNode::print()
{
	printf("line %d: MEMS: %d, beg: %d end:%d \n", inum, nmems, nodebeg->icell, nodeend->icell);
	for (CellNode *it = nodebeg ; it != NULL ; it=it->next)
	{
		printf("%d ", it->icell);
	}
	printf("\n");

	return 0;
}

/****************************************************************************
 *                             LinesMaster - old code
 ****************************************************************************/

PetscErrorCode LinesMaster::merge_two_lines(LineNode *line_keep, LineNode *line_delete)
{

	/*
	  Changes for lm
	*/
	nlines--;

	/*
	  Changes for the CellNodes
	*/
	
	// connect the cells
	line_keep->nodeend->next = line_delete->nodebeg;
	line_delete->nodebeg->prev = line_keep->nodeend;

	// set the father correctly
	for (CellNode *it = line_delete->nodebeg ; it!=NULL ; it = it->next )
	{
		it->father = line_keep;
	}

	/*
	  Changes for line LineNodes
	 */
	
	// fix nodeend  
	line_keep->nodeend = line_delete->nodeend;

	// fix number of members
	line_keep->nmems += line_delete->nmems;

	// delete line_delete from linked list

	// connect prev node to next
	if(line_delete->prev)
		line_delete->prev->next = line_delete->next;

	// connect next node to prev
	if(line_delete->next)
		line_delete->next->prev = line_delete->prev;

	// release the memory
	delete line_delete;

	
	return 0;

}


void LinesMaster::create(Mesh& msh)
{

	double *pri,  /* The value dist(max)/dist(ave) for each cell */
		*mmratio; /* The value dist(max)/dist(min) for each cell */
	
	int *srt,  /* Array storing priorities */
		iroot, /* Current cell used as root of the line */
		iline; /* Number of the current line being created */

	//object used for sorting cells
    CmpUniso cmp; 

	//writing data
	int vardim[] = {1,1,1,1,1};
	int centering[] = {0,0,0,0,0};
	double *vars[5] = {NULL, NULL, NULL, NULL, NULL};

	/*******************************************
	 *
	 * Create pre-requisits for lines
	 *
	 ********************************************/
	/*
	  allocate data
	*/
	mmratio = new double[msh.ncell];
    pri = new double[msh.ncell];
	srt = new int[msh.ncell];
	nodes = new CellNode[msh.ncell];
	
    /*
	  find the priorities
	*/
	for (int ii=0; ii<msh.ncell; ii++){
		double dmax, dave, dmin;

		//also fill srt
		srt[ii] = ii;

		//also set line node
		nodes[ii].icell = ii;

		msh.vCellDistInfo(ii, &dmax, &dmin, &dave);
		dave /= msh.ncellface[ii];

		//compute the properties
		pri[ii] = dmax/ dave;
		if (msh.ncellface[ii] > msh.ncellneigh[ii] ) pri[ii] +=g_alpha * fabs(msh.ncellneigh[ii] - msh.ncellface[ii]);
		mmratio[ii] = dmax/ dmin;
	}
	
	vars[0] = pri;
	vars[1] = mmratio;
	msh.writeVtk("pri.vtk", 2, vardim, centering, vars);

	/*
	  find srt according to the pri and delete pri
	*/
	CmpUniso::setVals(pri, msh.ncell);
	std::sort(srt, srt+msh.ncell, cmp);	
	delete[] pri;

	/*******************************************
	 *
	 * Find the lines
	 *
	 ********************************************/
	
	iroot = 0; iline = 0;	
	for (; iroot < msh.ncell; iroot++){

		/*
		  Required Variables
		*/
		int crnt, /* current cell in the line */
			inext, /* next cell to choose in the line */
			ilnext; /* local number for the neighbour of current cell */
		
		double wgt[MAXADJ], /* weight for sorting the neighbours of a cell*/
			dist; /* distance */
		
		int neigh[MAXADJ]; /* priority for the neighbours of a cell */
		
		LineNode *itinf; /* iterator for iterating through lineinfo */

		
	    /*
		  current cell is now the root
		*/
		crnt = srt[iroot];
		VERB printf("\n===Root is %d \n", crnt);

		/*
		  if the root is already in a line skip
		*/
	    if ( nodes[ crnt ].qline){
			VERB printf("root %d is already in a line, skiping ...\n", crnt);
			continue;
		}

		/*
		  Info for the current line
		*/
    	if(iroot == 0){
			infohead = new LineNode;
			itinf = infohead;
		} else{
			itinf->next = new LineNode;
			itinf = itinf->next;
		}
		itinf->inum = iline;
		itinf->nmems = 1;
		itinf->nodeker = &nodes[crnt];
				
		/*
		  start creating the line until you reach the ending criteria
		*/
		do{

			// print the current
			VERB printf("current is %d\n", crnt);
			
			//well crnt is now in the line
			nodes[crnt].qline = 1;
			nodes[crnt].father = itinf;
			
			// find the neighbours of crnt and sort according to connection stregth
			VERB printf("neighbours:");
			for (int ii=0 ; ii<msh.ncellneigh[crnt]; ii++){				
				neigh[ii] = ii;
				dist = msh.dCCDist(crnt, msh.icelltocell[crnt*MAXADJ+ii] );
			    wgt[ii] = msh.dave / dist;
				VERB printf(" (%lf, %d) ", wgt[ii],  msh.icelltocell[crnt*MAXADJ+ii]);
			}
			VERB printf("\n");
			CmpUniso::setVals(wgt, msh.ncellneigh[crnt]);
			std::sort(neigh, neigh+msh.ncellneigh[crnt], cmp);

			// printf the sorted neighbours
			VERB{
				printf("sorted neighs: ");
				for (int ii=0 ; ii<msh.ncellneigh[crnt]; ii++)
					printf(" (%d) ", msh.icelltocell[crnt*MAXADJ+ neigh[ii] ]);
				printf("\n");
			}
			
			// find the most strongly connected neighbour which is not self coinciding
			ilnext = 0;
			do{
				inext = msh.icelltocell[crnt*MAXADJ + neigh[ilnext] ];
				ilnext++;
			}while( (nodes[crnt].prev) && (inext == nodes[crnt].prev->icell) );

			// on rare case inext can be negative, check if it is so end this line
			// check the selected neighbour to be added to the line
			if ( (inext >= 0) && (mmratio[inext] > g_beta) &&  (!nodes[inext].qline) ){
				nodes[crnt].next = &nodes[inext];
				nodes[inext].prev = &nodes[crnt];
				crnt = inext;
				itinf->nmems++;
				VERB printf("Adding cell %d to line\n", crnt);
			} else{
				VERB printf("cell %d is already a line member skipping (mmrat %lf )...\n", inext, mmratio[inext]);
				break;
			}

		}while(1);
		
		

		/*
		  Now start creating the backward line
		*/
		crnt = srt[iroot];
		VERB printf("BACKWARD\n");
		do{

			// print the current
			VERB printf("current is %d\n", crnt);
			 
			//well crnt is now in the line (safe for crnt = src[root])
			nodes[crnt].qline = 1;
			nodes[crnt].father = itinf;
			
			// find the neighbours of crnt and sort according to connection stregth
			VERB printf("neighbours:");
			for (int ii=0 ; ii<msh.ncellneigh[crnt]; ii++){				
				neigh[ii] = ii;
				dist = msh.dCCDist(crnt, msh.icelltocell[crnt*MAXADJ+ii] );
			    wgt[ii] = msh.dave / dist;
				VERB printf(" (%lf, %d) ", wgt[ii],  msh.icelltocell[crnt*MAXADJ+ii]);
			}
			VERB printf("\n");
			CmpUniso::setVals(wgt, msh.ncellneigh[crnt]);
			std::sort(neigh, neigh+msh.ncellneigh[crnt], cmp);

			// printf the sorted neighbours
			VERB{
				printf("sorted neighs: ");
				for (int ii=0 ; ii<msh.ncellneigh[crnt]; ii++)
					printf(" (%d) ", msh.icelltocell[crnt*MAXADJ+ neigh[ii] ]);
				printf("\n");
			}
			
			// find the most strongly connected neighbour which is not self coinciding
			ilnext = 0;
			do{
				inext = msh.icelltocell[crnt*MAXADJ + neigh[ilnext] ];
				ilnext++;
			}while( (nodes[crnt].next) && (inext == nodes[crnt].next->icell) );


			// check if we really nead checking backward
			if ( (crnt == srt[iroot]) && (msh.ncellneigh[crnt] < msh.ncellface[crnt])){

				//check if there is next
				if(!nodes[crnt].next){
					VERB printf("Root %d has no back and deservers no forward skipping .\n", crnt);
					break;
				}

				// for a triangle on boundary do not go to -1
				if (inext < 0)
				{
					break;
				}

				// check distance ( do not attach boundary to left or right)
				double distback, distfront;
				distback = msh.dCCDist(crnt, nodes[crnt].next->icell);				
				distfront = msh.dCCDist(crnt, inext);
			    if( distfront/distback > 2){
					VERB printf("Root %d's next cell is too far for backward line. skipping .\n", crnt);
					break;
				}
			}

			// check the selected neighbour to be added to the line
			if ( (mmratio[inext] > g_beta) &&  (!nodes[inext].qline) ){
				nodes[crnt].prev = &nodes[inext];
				nodes[inext].next = &nodes[crnt];
				crnt = inext;
				itinf->nmems++;
				VERB printf("Adding cell %d to line\n", crnt);
			} else{
				//set the head of the line
				VERB printf("cell %d is already a line member skipping ...\n", inext);
				break;
			}

		}while(1);

		/*
		  update the begin node for this line
		*/
		itinf->nodebeg = &nodes[crnt];
		iline++;
	}

	/*
	  update number of lines
	*/
	nlines = iline;

	/*
	  delete temporary stuff
	*/
	delete[] srt;
	delete[] mmratio;
}

/****************************************************************************
 *                              Create the lines
 ****************************************************************************/

// This function gives the neighbouring faces
// ordered by their stregth.
PetscErrorCode sort_faces(const int n_face, const double wgt[], int sorted_faces[])
{
	// create an array holding the natural ordering
	for (int ilf=0 ; ilf< n_face; ilf++)
	{				
		sorted_faces[ilf] = ilf;
	}

	// now sort the copy to get the sorted ordering
	CmpUniso cmp;
	CmpUniso::setVals(wgt, n_face);
	std::sort(sorted_faces, sorted_faces+n_face, cmp);

	return 0;
}

// create a line in either forward or backward direction
// input :
//   mesh
//   cell_nodes array containing information about cells
//   face_wgts array containing weights of the faces
//   line pointer to the LineNode containing information about the current line
//        nodeker must be set before calling
//   direction direction towards which the line is created
PetscErrorCode create_one_line(const Mesh* mesh, CellNode cell_nodes[],
							   const double global_face_wgts[], LineNode* line,
							   const Direction direction)
{
	//PetscErrorCode ierr;

	// check that nodeker be set
	assert(line->nodeker);
	assert(line->nodebeg);
	assert(line->nodeend);
	// the line must be numbered
	assert(line->inum != -1);
	
	// get the number of the current cell
	int current_cell = line->nodeker->icell;

	// start adding cells to the line in the relevant direction
	do
	{
		// set the relevant data for the new node
		cell_nodes[current_cell].father = line;
		cell_nodes[current_cell].qline = 1;

		// some aliasing
		int  n_faces = mesh->ncellface[current_cell];
		int  n_neighs = mesh->ncellneigh[current_cell];
		int *faces = mesh->icelltoface + current_cell*MAXADJ;
		int *neighs = mesh->icelltocell + current_cell*MAXADJ; 
		

		// find the local weights
		int local_sorted_faces[MAXADJ];
		double face_weights[MAXADJ];
		for(int ilf=0; ilf< n_faces ; ilf++)
		{
			face_weights[ilf] = global_face_wgts[faces[ilf]];
		}

		// sort the faces
		sort_faces( n_faces, face_weights, local_sorted_faces);
	    
		// find the highest connectivity face which is not part of a
		// line
		//
		// Note: It is possible that we are on the kernel cell
		// and we tried to select the heaviest face and we failed. In
		// the backward try we go for the heavy face again, while we
		// should have gone for the second heaviest face. My first
		// remedy for that was pushing the boundary to the second
		// highest face. This does not work for the case when the
		// heaviest face which has failed is not boundary.       
		//       if (local_sorted_faces[0] >= n_neighs)
		//       {
		// 	          int tmp;
		// 	          tmp = local_sorted_faces[1];
		// 	          local_sorted_faces[1] = local_sorted_faces[0];
		// 	          local_sorted_faces[0] = tmp;			
		//       }
		// The second remedy is to check this condition explicitly.
		int highest_face;
		int local_highest_face;
		int highest_cell;
		int ifp;
		for(ifp=0; ifp< n_faces ; ifp++)
		{
			// find the highest face, while checking for the
			// kernel heaviest fail case explicitly.
			if( (current_cell == line->nodeker->icell) && (ifp == 0) && (direction == BACKWARD) )
			{
				continue;
			}
			local_highest_face = local_sorted_faces[ifp];
			highest_face = faces[local_highest_face];

			// if not on boundary find the highest cell
			highest_cell=-1;
			if (local_highest_face<n_neighs)
			{
				highest_cell = neighs[local_highest_face];
			}

			// ignore if face is self intersecting
			bool found_candidate = true;
			if ( (highest_cell!=-1) && cell_nodes[highest_cell].father == line )
				found_candidate = false;
			if (found_candidate)
				break;
		}
			
		// make sure that we have chosen a face
		if (ifp >= n_faces)
		{
			break;
			SETERRQ(PETSC_COMM_WORLD,1,"Could not find next candidate face line making.");
		}

		// check if the found cell is worthy of adding / or terminate
		// 1- face must not be boundary
		// 2- corresponding cell is on a line
		// 3- face is not a top priority
		// 4- the face should be either the first or second priority of neigh as well
		bool meet_conditions = true;

		if ( highest_cell == -1)
			meet_conditions = false;
		else if ( cell_nodes[highest_cell].qline )
			meet_conditions = false;
		else if ( ifp >= 2)
			meet_conditions = false;
		else
		{
			// TODO: we probably can avoid doing this operation twice
			// some aliasing for the nieghbour cell
			int  n_faces_neigh = mesh->ncellface[highest_cell];
			int *faces_neigh = mesh->icelltoface + highest_cell*MAXADJ;
		
			// find the local weights of the neighbour cell
			int local_sorted_faces_neigh[MAXADJ];
			double face_weights_neigh[MAXADJ];
			for(int ilf=0; ilf< n_faces_neigh ; ilf++)
			{
				face_weights_neigh[ilf] = global_face_wgts[faces_neigh[ilf]];
			}

			// sort the faces
			sort_faces( n_faces_neigh, face_weights_neigh, local_sorted_faces_neigh);

			// check the current cell to be either first or second priority of neigh
			bool is_first_two_prio = false;
			
			if (faces_neigh[local_sorted_faces_neigh[0]] == highest_face)
				is_first_two_prio =true;
			else if (faces_neigh[local_sorted_faces_neigh[1]] == highest_face)
				is_first_two_prio =true;
				
			if (!is_first_two_prio)
				meet_conditions = false;
		}
			
		if(!meet_conditions) break;

		// now add the cell to the current line and proceed
		switch (direction)
		{
		case FORWARD:
			cell_nodes[current_cell].next = &cell_nodes[highest_cell];
			cell_nodes[highest_cell].prev = &cell_nodes[current_cell];
			line->nodeend = &cell_nodes[highest_cell];
			break;
		case BACKWARD:
			cell_nodes[current_cell].prev = &cell_nodes[highest_cell];
			cell_nodes[highest_cell].next = &cell_nodes[current_cell];
			line->nodebeg = &cell_nodes[highest_cell];
			break;
		}
		current_cell = highest_cell;
		line->nmems++;

	}while(true);

	return 0;
}

PetscErrorCode create_all_lines(LinesMaster *list_lines, Mesh *mesh, double global_face_wgts[])
{
	PetscErrorCode ierr;
	
	/*
	  create the line nodes
	 */
	list_lines->nodes = new CellNode[mesh->ncell];
	for (int i=0; i<mesh->ncell; i++)
		list_lines->nodes[i].icell = i;

	/*
	  start creating lines
	*/
	int iker = 0,  iline = 0;	
	for (; iker < mesh->ncell; iker++)
	{
		LineNode *current_line;

		// if the ker is already in line continue
		if (list_lines->nodes[iker].qline) continue;
		
		// create the next line
		if(iker == 0)
		{
			list_lines->infohead = new LineNode(list_lines->nodes+iker, iline);
			current_line = list_lines->infohead;
		} else
		{
			current_line->next = new LineNode(list_lines->nodes+iker, iline);
			current_line->next->prev = current_line;
			current_line = current_line->next;
		}

		//create the lines
		ierr=create_one_line(mesh, list_lines->nodes, global_face_wgts, current_line, FORWARD);CHKERRQ(ierr);
		ierr=create_one_line(mesh, list_lines->nodes, global_face_wgts, current_line, BACKWARD);CHKERRQ(ierr);

		// increment line number
		iline++;
	}

	// set number of lines
	list_lines->nlines = iline;

	return 0;
}


/****************************************************************************
 *                             Merge the lines
 ****************************************************************************/

PetscErrorCode find_boundary_or_endline_faces
(LinesMaster *lm, Mesh *mesh, double global_face_wgts[],
 const int cell, 
 int &n_target_local_faces, int target_local_faces[])
{
	// some aliasing
	int  n_faces = mesh->ncellface[cell];
	int  n_neighs = mesh->ncellneigh[cell];
	int *faces = mesh->icelltoface + cell*MAXADJ;
	int *neighs = mesh->icelltocell + cell*MAXADJ; 

	/*
	  find faces that are either boundary or endpoint
	*/
	n_target_local_faces = 0;
	for (int ilf = 0 ; ilf < n_faces ; ilf++)
	{
		// check if the face has a neighbour which is end point
		if (ilf < n_neighs)
		{
			int cell_neigh = neighs[ilf];
			CellNode *cnn = &lm->nodes[cell_neigh];
			CellNode *self = &lm->nodes[cell];

			// save the face is cell_neigh is an endpoint of a different line
			if  (
				(cnn->father != self->father) &&
				( (cnn->father->nodeend == cnn) || (cnn->father->nodebeg == cnn) )
				)
			{
				target_local_faces[n_target_local_faces] = ilf;
				n_target_local_faces++;
			}
		}

		// check if the face is boundary
		else
		{
			target_local_faces[n_target_local_faces] = ilf;
			n_target_local_faces++;
		}
		
	}

	/*
	  sort these faces
	 */

	// find the weight for each target face
	double face_weights[MAXADJ];
	for(int i=0; i< n_target_local_faces ; i++)
	{
		face_weights[target_local_faces[i]] = global_face_wgts[faces[ target_local_faces[i] ]];
	}

	// now sort the target faces
	CmpUniso cmp;
	cmp.setVals(face_weights, n_faces);
	std::sort(target_local_faces, target_local_faces+n_target_local_faces, cmp);

	return 0;
}


PetscErrorCode merge_end_cell
(LinesMaster *lm, Mesh *mesh, double global_face_wgts[],
 const int cell,
 bool &merge_needed)
{
	PetscErrorCode ierr;
	
	// get pointer to cell
	CellNode *self = &lm->nodes[cell];
	
	// check cell to be truelly end point
	if( self != self->father->nodeend )
	{
		SETERRQ2(PETSC_COMM_SELF, 1, "cell %d is not endpoint of line %d", self->icell, self->father->inum);
	}

	// no merge unless we do it
	merge_needed = false;

    // get the target faces of that cell
	int target_local_faces[MAXADJ];
	int n_target_faces;
	ierr=find_boundary_or_endline_faces(lm, mesh, global_face_wgts, cell, n_target_faces, target_local_faces);
	CHKERRQ(ierr);

	// see if any candidate for connection exist
	if(n_target_faces == 0)
		return 0;
	
	// some aliasing
	int  n_neighs = mesh->ncellneigh[cell];
	int *faces = mesh->icelltoface + cell*MAXADJ;
	int *neighs = mesh->icelltocell + cell*MAXADJ; 
	
	// see if the first one is not boundary
	if (target_local_faces[0] >= n_neighs)
		return 0;
	
	// find the global highest face
	int highest_face = faces[target_local_faces[0]];
		
	// find the associated neighbour
	int highest_cell = neighs[target_local_faces[0]];

	// now check this cell to be neighbours high connectivity

	// find the neighs local target faces
	int target_local_faces_neigh[MAXADJ];
	int n_target_faces_neigh;
	ierr=find_boundary_or_endline_faces(lm, mesh, global_face_wgts, highest_cell, n_target_faces_neigh, target_local_faces_neigh);
	CHKERRQ(ierr);

	
	// find the global number of neigh's highest face
	int *faces_neigh = mesh->icelltoface + highest_cell*MAXADJ; 
	int highest_face_neigh = faces_neigh[target_local_faces_neigh[0]];

	// is cell highest connectivity of neigh
	if (highest_face_neigh != highest_face)
		return 0;

	// get pointer to neigh's CellNode
	CellNode *cnn = &lm->nodes[highest_cell];
			
	// revert the neighbouring line if cnn is the end cell
	if (cnn == cnn->father->nodeend)
	{
		cnn->father->revert();
	}
	else if (cnn != cnn->father->nodebeg)
	{
		SETERRQ4(PETSC_COMM_SELF, 1, "cell %d found cell %d to connect to but it is not an endline(b: %d e: %d)",
				 cell, highest_cell, cnn->father->nodebeg->icell, cnn->father->nodeend->icell);
	}

	// merge cells father and cnn
	lm->merge_two_lines(self->father, cnn->father);
			
	// announce success
	merge_needed = true;
			
	return 0;
}

PetscErrorCode merge_all_lines
(LinesMaster *lm, Mesh *mesh, double global_face_wgts[])
{
	PetscErrorCode ierr;

	// merge all lines
	//bool was_final_pass;
	//int n_pass = 0;
	//do
	//{
	//was_final_pass = true;
	//n_pass++;

	for (LineNode *it = lm->infohead ; it != NULL ; it=it->next)
	{
		bool merge_needed;
		
		// try to merge from ass
		//do
		//{
		ierr=merge_end_cell(lm, mesh, global_face_wgts, it->nodeend->icell, merge_needed);		    
		CHKERRQ(ierr);
		//if(merge_needed) was_final_pass = false;
		//}while(merge_needed);

		//try to merge from ass (former head)
		it->revert();
		//do
		//{
		ierr=merge_end_cell(lm, mesh, global_face_wgts, it->nodeend->icell, merge_needed);
		CHKERRQ(ierr);
		//if(merge_needed) was_final_pass = false;
		//}while(merge_needed);
	}
	//}while(!was_final_pass);

	// renumber lines
	int i = 0;
	for (LineNode *it = lm->infohead ; it != NULL ; it=it->next)
	{
		it->inum = i;
		i++;
	}
	printf("%d %d \n" , i, lm->nlines);
	assert(i==lm->nlines);

	//printf("%d line creation passes \n" , n_pass);
	
 	return 0;
}


/****************************************************************************
 *                             Face Weight Functions
 ****************************************************************************/

PetscErrorCode face_weight_distance(Mesh *mesh, double global_face_wgts[])
{
	for (int iface=0; iface<mesh->nface; iface++)
	{

		// cell to cell distance
		if (mesh->ifacetocell[iface*2+1] > 0)
		{
			global_face_wgts[iface] = 1./mesh->dCCDist(mesh->ifacetocell[iface*2], mesh->ifacetocell[iface*2+1]);
		}
		// cell to mid edge distance times two
		else
		{
			//double adf[2]; mesh->dFaceLength(iface, adf);
			//double *adc = &mesh->dcellcentre[mesh->ifacetocell[iface*2]];
			//global_face_wgts[iface] = 1./2./ sqrt(pow(adc[0] - adf[0] ,2) + pow(adc[1] - adf[1],2));
			global_face_wgts[iface] = 1e10;
		}
	}

	return 0;
}

// Face Search Helper
class FaceSH
{
	int _cv_left, _cv_right;
	int _iface;

public:
	FaceSH(const int cv_left, const int cv_right, const int iface):
	_cv_left(cv_left),
	_cv_right(cv_right),
	_iface(iface)
		{}

	bool operator< (const FaceSH &of) const
		{
			if (_cv_left < of._cv_left)
				return true;
			else if ( (_cv_left == of._cv_left) &&
					  (_cv_right < of._cv_right) )
				return true;
			else
				return false;
		}

	bool operator== (const FaceSH &of) const
		{
			return (
				(_cv_left == of._cv_left) &&
				(_cv_right == of._cv_right)
				);
		}

	int iface() const {return _iface;}
	int cvl() const {return _cv_left;}
	int cvr() const {return _cv_right;}
	
	
};

PetscErrorCode face_weight_file(Mesh *mesh, double global_face_wgts[], const char file_name[])
{
	//PetscErrorCode ierr;

	FILE *fl;

	// open the file
	fl = fopen(file_name, "r");
	if (!fl) SETERRQ1(PETSC_COMM_WORLD, 1, "File %s not found", file_name);

	// create a vector of faces and sort it
	std::vector<FaceSH> vec_faces;
	vec_faces.reserve(mesh->nface);
	
	for (int i=0 ; i<mesh->nface; i++)
	{
		FaceSH face_sh(mesh->ifacetocell[i*2],mesh->ifacetocell[i*2+1], i);
		vec_faces.push_back(face_sh);
	}

	std::sort(vec_faces.begin(), vec_faces.end());

	
	// read the stuff
	for (int i=0 ; i<mesh->nface; i++)
	{
		int nread;
		int iface, cv_left, cv_right;
		double face_weight;

		nread = fscanf(fl, "%d %lf %d %d", &iface, &face_weight, &cv_left, &cv_right);
		if (nread < 2) SETERRQ1(PETSC_COMM_WORLD, 1,"File %s ended too soon", file_name);

		// if solver did not rename
		//global_face_wgts[iface] = face_weight;

		// apparently solver renames faces
		// this is a very slow but working fix
		// int j = 0;
		// for (; j<mesh->nface; j++)
		// {
		// 	bool match = false;
			
		// 	if ((mesh->ifacetocell[j*2]==cv_left) && (mesh->ifacetocell[j*2+1]==cv_right) )
		// 		match = true;
		// 	else if ((mesh->ifacetocell[j*2]==cv_right) && (mesh->ifacetocell[j*2+1]==cv_left) )
		// 		match = true;

		// 	if (match) break;
		// }
		// if (j >= mesh->nface)
		// {
		// 	SETERRQ2(PETSC_COMM_WORLD, 1, "want to find face with neighbours %d %d but not found", cv_left, cv_right);
		// }
		// global_face_wgts[j] = face_weight;

		// apparently solver renames faces
		// this is a faster fix
		FaceSH face_sh(cv_left, cv_right, -1);
		std::vector<FaceSH>::iterator it = std::lower_bound(vec_faces.begin(), vec_faces.end(), face_sh);
		assert(it->cvl() == cv_left && it->cvr() == cv_right);
		global_face_wgts[it->iface()] = face_weight;

		//printf("%d   %.8lf %d %d \n", iface, global_face_wgts[iface], mesh->ifacetocell[iface*2], mesh->ifacetocell[iface*2+1]);
	}

	//close the file
	fclose(fl);

	return 0;
}

/****************************************************************************
 *                             IO
 ****************************************************************************/

void printLinesMaster(LinesMaster &lines)
{
	LineNode *itinfo;
	CellNode *itcell;

	for (itinfo = lines.infohead ; itinfo != NULL ; itinfo=itinfo->next){
		printf("line %d: MEMS: %d, beg: %d \n", itinfo->inum, itinfo->nmems, itinfo->nodebeg->icell);
		for (itcell = itinfo->nodebeg; itcell !=NULL ; itcell=itcell->next){
			printf(" %d " , itcell->icell);
}
		printf("\n");
	}
}


PetscErrorCode face_weight_write_vtk(Mesh *mesh, double global_face_wgts[], const char file_name[])
{
	FILE *fl;
	double face_center[2];

	fl = fopen(file_name, "w");
	assert(fl);

	// write the header
	fprintf(fl, "# vtk DataFile Version 2.0\n");
	fprintf(fl, "Test for lines\n");
	fprintf(fl, "ASCII\n");
	fprintf(fl, "DATASET UNSTRUCTURED_GRID\n\n");

	// write the points
	fprintf(fl, "POINTS %d double\n", mesh->nface);
	for(int iface=0; iface<mesh->nface ; iface++)
	{
		mesh->dFaceLength(iface, face_center);
		fprintf(fl, "%-25.20lf ", face_center[dirX]);
		fprintf(fl, "%-25.20lf ", face_center[dirY]);
		fprintf(fl, "0.00\n");
	}
	fprintf(fl, "\n");


	//write face weight
	fprintf(fl, "POINT_DATA %d\n", mesh->nface);
	fprintf(fl, "SCALARS face_weight double\n");
	fprintf(fl, "LOOKUP_TABLE default\n");
	for(int ii=0; ii<mesh->nface ; ii++)
	{
		fprintf(fl, "%lf\n", global_face_wgts[ii]);
	}
	
	fclose(fl);

	return 0;
}

// write the lines in line format
PetscErrorCode read_lines_from_file(LinesMaster *lm , const int ncell, const char file_name[])
{

	FILE *fl;

	// open the file
	fl = fopen(file_name, "r");
	assert(fl);

	// read the number of line
	int ncell2, nline;
	fscanf(fl, "%d %d ", &nline, &ncell2);
	assert(ncell2 == ncell);
	
	/*
	  changes to LineMaster
	*/
	// create the cellnodes
	lm->nodes = new CellNode[ncell];
	for (int cell = 0 ; cell < ncell ; cell++)
	{
		lm->nodes[cell].icell = cell;
	}

	// nline
	lm->nlines = nline;

	// create the line head
	lm->infohead = new LineNode;
	LineNode *current_line = lm->infohead;

	/*
	  Changes to lines and cells
	*/
	int il;
	for (il = 0; il < nline; il++ )
	{
		// set line number
		current_line->inum = il;

		// set number of members
		fscanf(fl, "%d", &current_line->nmems);

		/*
		  set the member cells
		*/
		int cell, cellp=-1;
			
		for (int ic = 0; ic < current_line->nmems; ic++ )
		{
			
			// read the cell no 
			fscanf(fl, "%d", &cell);

			// set nodebeg
			if (ic == 0 ) current_line->nodebeg = &lm->nodes[cell];
			else if (ic == current_line->nmems-1) current_line->nodeend = &lm->nodes[cell];

			// set father
			lm->nodes[cell].qline = 1;
			lm->nodes[cell].father = current_line;

			// set prev and next
			if (cellp != -1)
			{
				lm->nodes[cellp].next = &lm->nodes[cell];
				lm->nodes[cell].prev = &lm->nodes[cellp];				
			}

			// save previous cell
			cellp = cell;
		}

		// go to the next line
		if (il != nline -1)
		{
			current_line->next = new LineNode;
			current_line->next->prev = current_line;
			current_line = current_line->next;
		}
	}

	// close the file
	fclose(fl);

	return 0;
}

// read the lines from the dat format
PetscErrorCode write_lines_to_file(LinesMaster *lm, Mesh *ms, const char file_name[])
{
	FILE *fl;

	// open the file
	fl = fopen(file_name, "w");
	assert(fl);

	// write the the header
	// # lines # cells
	fprintf(fl, "%d %d \n\n", lm->nlines, ms->ncell);

	// for each line write ncells and number of cells
	for (LineNode *line = lm->infohead ; line != NULL ; line = line->next)
	{
		fprintf(fl, "%d ", line->nmems);
		for (CellNode *cell = line->nodebeg ; cell != NULL ; cell = cell->next )
		{
			fprintf(fl, "%d ", cell->icell);
		}
		fprintf(fl, "\n");
	}
	
	// close the file
	fclose(fl);
	
	return 0;
}
