/*
 * global.hxx
 *
 * Global variables and definitions.
 */

#ifndef GLOBAL_HXX
#define GLOBAL_HXX


/****************************************************************************
 *                               Common includes
 ****************************************************************************/
#include <cassert>
#include <cstdlib>
#include <petscsys.h>

/****************************************************************************
 *                               Definitions
 ****************************************************************************/

#define MAXADJ 4
#define MAXRECON2 13
#define MAXRECON3 26
#define MAXRECON4 50
#define MAXNODE 30
#define NBPATCH 90

#define VERB if(g_bverbos)

#define GMAX(x,y) ((x) > (y) ? (x) : (y))
#define GMIN(x,y) ((x) > (y) ? (y) : (x))

enum EnumDirection{ dirX=0, dirY };
enum PartMethod{ PART_CELLS, PART_LINES, COLOR_LINES };


/****************************************************************************
 *                               Variables
 ****************************************************************************/
extern int g_bverbos;
extern int g_refpt;
extern double g_alpha, g_beta;
extern int g_npart;
extern int g_wgt;
extern int g_fec;
extern char g_fname[300];
extern char *g_fname_fonly;
extern char g_out_prefix[300];
extern char g_part_fname[300];
extern char g_fecname[300];
extern int g_bpatch;
extern PartMethod g_partmethod;
extern char g_flw_fname[300];

/****************************************************************************
 *                               Functions
 ****************************************************************************/
PetscErrorCode g_vInitialize();


#endif /*GLOBAL_HXX*/
