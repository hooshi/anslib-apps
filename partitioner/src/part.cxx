/*
 * part.cxx
 *
 * Functions and data structures related to partitioning.
 */

#include "part.hxx"
#include "petscerror.h"
#include "petscmat.h"
#include "petscsnes.h"

#include <cstdio>
#include <cassert>
#include <cstring>
#include <algorithm>
#include <vector>
#include <set>

using std::set;
using std::vector;

int iEdgeWeight(Mesh *msh, const int ic1, const int ic2){

    // 	double dist;
    // 	double dmin1, dmax1, dsum1;
    // 	double dmin2, dmax2, dsum2;
    // 	double dave, avemax, avemin;
    // 	int iwgt;
	
    // 	dist = msh->dCCDist(ic1, ic2);
    // 	msh->vCellDistInfo(ic1, &dmax1, &dmin1, &dsum1);
    // 	msh->vCellDistInfo(ic2, &dmax2, &dmin2, &dsum2);

    // 	//wgt = (msh->ncellface[ic1]*dsum1 + msh->ncellface[ic2]*dsum2);
    // 	//wgt /= (msh->ncellface[ic1] + msh->ncellface[ic2]);
    // 	//wgt = 1;
    // 	//wgt /= dist;

    // 	avemax = GMAX(dmax1 ,dmax2);
    // 	avemin = GMIN(dmin1 ,dmin2);
	
    //    //return GMAX(iwgt,1);//+ 1;

    // 	return (int)( GMIN(avemax/avemin/g_beta, 560));
    return 1;	
}

PetscErrorCode vMeshPartitionCells(Mesh *msh, LinesMaster *lns, const int inpart, idx_t *aicelltopart)
{
	    
    int ierr;
    int ii;

    //core - metis
    idx_t ncon = 1;
    idx_t nparts = inpart;
    idx_t objval;
    idx_t *xadj = (idx_t*)NULL;
    idx_t *adjncy =  (idx_t*)NULL;

    //weights
    idx_t *adjwgt;

    //options
    idx_t options[METIS_NOPTIONS];
	

    /*
     * metis options
     */
    ierr=METIS_SetDefaultOptions(options);assert(ierr==METIS_OK);
    options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;
    options[METIS_OPTION_NUMBERING] = 0;
    options[METIS_OPTION_CONTIG] = 1;
    //options[METIS_OPTION_UFACTOR] = 25;
		
    /*
     * compute adjacency for metis
     */
	
    //allocate memory
    xadj = (idx_t*)calloc((size_t)(msh->ncell+1), sizeof(idx_t));
    adjncy = (idx_t*)calloc((size_t)(msh->ncell*MAXADJ), sizeof(idx_t));
    adjwgt = (idx_t*)calloc((size_t)(msh->ncell*MAXADJ), sizeof(idx_t));
	
    // create metis adjacency and weigts
    vMesh2MetisGraph(msh, xadj, adjncy, adjwgt);

    /************************************************
     * WEIGHT
     ***********************************************/
    int iweight;
	
    if (g_wgt == 0){
    }
	
    /*
      only lines have weight
    */
    else if (g_wgt == 1){
        LineNode *inf;
        CellNode *it;
        int ilc1, ilc2;
		
        for (inf = lns->infohead ; inf != NULL; inf = inf->next){
			
            double dmax, dmin;
            int iwgt;
            msh->vCellDistInfo(inf->nodeker->icell, &dmax, &dmin, NULL);
            iwgt = GMIN( GMAX( (int)( dmax/dmin/g_beta ) ,1) , 100 );
    
            for (it = inf->nodebeg ; (it != NULL) && (it->next != NULL) ; it = it->next){
				
                vCell2MetisEdge(xadj, adjncy, it->icell, it->next->icell, ilc1, ilc2);
                iweight = iEdgeWeight(msh, it->icell, it->next->icell);
                adjwgt[ilc1]= iwgt;
                adjwgt[ilc2]= iwgt;

                printf("root[%d] c(%d, %d) %d\n", inf->nodeker->icell, adjncy[ilc1], adjncy[ilc2],  iwgt);
            }
        }
		
    }


    /*
      all have weight
    */
    else if (g_wgt == 2){
		
        int iclr, icll;
        int icgl, icgr;
		
        for ( int ii = 0 ; ii < msh->nface ; ii++){
			
            icgl = msh->ifacetocell[2*ii];
            icgr = msh->ifacetocell[2*ii+1];
            if (icgr < 0 ) continue;

            vCell2MetisEdge(xadj, adjncy, icgl, icgr, icll, iclr);
            iweight = iEdgeWeight(msh, icgl, icgr);
            adjwgt[icll]= iweight;
            adjwgt[iclr]= iweight;

            printf("[%d] c(%d, %d) %d\n", ii, icgl, icgr,  iweight);
        }
    }

    /************************************************
     * DEBUG
     ***********************************************/
    VERB vPrintMetisGraph(msh->ncell, xadj, adjncy, NULL, stdout);
		

    /************************************************
     * PARTITION
     ***********************************************/
    /*
     * partition using metis
     */
    ierr=METIS_PartGraphKway(&msh->ncell, &ncon, xadj, adjncy,
                             NULL, NULL, adjwgt, &nparts, NULL,
                             NULL, options, &objval, aicelltopart);
    // ierr=METIS_PartGraphRecursive(&msh->ncell, &ncon, xadj, adjncy,
    // 						 NULL, NULL, adjwgt, &nparts, NULL,
    // 						 NULL, options, &objval, aicelltopart);
    assert(ierr==METIS_OK);
	
    // test partitioning
    VERB{
        printf("objval: %d\n", objval); 
        for (ii=0; ii < msh->ncell ; ii++){ 
            printf("[%d] -> part[%d]\n", ii, aicelltopart[ii]); 
        }
    }

    /*
      delete the metis graph
    */
    if(adjncy) free(adjncy);
    if(xadj) free(xadj);
    if(adjwgt) free(adjwgt);


    return 0;
}

PetscErrorCode vMeshPartitionLinesMaster(Mesh *msh, LinesMaster *lns, const int inpart, idx_t *aicelltopart, const PartMethod met)
{

    // error checking
    int ierr;

    //core - metis
    idx_t nparts = inpart;
    idx_t *xadj = (idx_t*)NULL;
    idx_t *adjncy =  (idx_t*)NULL;
    idx_t *vwgt;
    idx_t *ailinetopart;


    // debug print fathers
    //for (int icell = 0 ; icell < msh->ncell; icell++)
    //{
    //	printf("%d %p %d*\n",
    //		   lns->nodes[icell].icell, lns->nodes[icell].father,
    //		lns->nodes[icell].father->inum);
    //	
    //}

    // Create the adjacency list for the lines
    {
        // For each cell create a set that stores the adjacent lines.
        vector< set<int> > adj_set_vec;
        adj_set_vec.resize(lns->nlines);

        // For over all the lines, add all the lines of neighbour
        // cells of member cells to the corresponding set.
        for (LineNode *itline = lns->infohead; itline != NULL; itline=itline->next)
        {
            int num_line = itline->inum;
			
            // Go through each member
            for (CellNode *itcell = itline->nodebeg ; itcell != NULL; itcell = itcell->next)
            {
                int ic = itcell->icell;

                // Go through the neighbours of each member
                for (int iln = 0 ; iln < msh->ncellneigh[ic] ; iln++)
                {
                    // global number of neighbour
                    int ign = msh->icelltocell[ic * MAXADJ + iln];
                    //debug
                    //printf("%d glob_neigh: %d ", ic, ign);
					
                    // iter of neighbour cell
                    CellNode *it_neigh_cell = &lns->nodes[ign];
                    //debug
                    //printf(", %d ", it_neigh_cell->icell);
					
                    // iter of neighbour line
                    LineNode *it_neigh_line = it_neigh_cell->father;
                    //debug
                    //printf("%d \n", it_neigh_line->inum);
					
                    // number of neighbour line
                    int num_neigh_line = it_neigh_line->inum;

                    // add the neighbour to the corresponding set
                    // note: the neighbour might be in the same line
                    //       in this case we should not add it.
                    if (num_neigh_line != num_line)
                    {
                        adj_set_vec[num_line].insert(num_neigh_line);
                    }
                }
            }
        }

        // Now that the cell is created convert it to metis graph format
        // Count the number of members in adj list
        int n_adj_list = 0;
        for (int iline = 0 ; iline < lns->nlines ; iline++)
        {
            n_adj_list += adj_set_vec[iline].size();
        }

        // allocate memory for metis graph
        xadj = new idx_t[lns->nlines+1];
        adjncy = new idx_t[n_adj_list];
        vwgt = new idx_t[lns->nlines];

        // For over the lines to set the weight and the neighbours
        xadj[0] = 0;	
        for (LineNode *itline = lns->infohead; itline != NULL; itline=itline->next){

            // store number of line
            int iline = itline->inum;

            // xadj[iline+1] will finally show where the contrib. of
            // this line will end.
            xadj[iline+1] = xadj[iline];

            // for over neighbour lines to add them to ajncy
            set<int>::iterator it = adj_set_vec[iline].begin();
            const set<int>::iterator it_end = adj_set_vec[iline].end();
			
            for (; it != it_end ; it++){
                adjncy[ xadj[iline+1] ] = *it; 
                xadj[iline+1]++;
            }

            // now set the weight for each line equal to number of member
            // cells.
            vwgt[iline] = itline->nmems;
		
        }  // end of for over lines

    } //end of block for metis graph

    // debug - print the lines
    // vPrintMetisGraph(lns->nlines, xadj, adjncy, NULL, stdout);		
	
    // Allocate memory for metis linetopartition
    ailinetopart = new idx_t[lns->nlines];
		
    /*
     * Do the partitioning
     * This code should be refactored!!!
     */
    switch(met)
    {

	// color the lines
    case COLOR_LINES:
    {
        // find the number of nonzeros per row
        std::vector<int> n_row_nzero;
        n_row_nzero.resize(lns->nlines);
        int max_nzero = 0;
        for (int i=0 ; i<lns->nlines; i++)
        {
            n_row_nzero[i]=xadj[i+1] - xadj[i]+1;
            max_nzero = GMAX(max_nzero, n_row_nzero[i]);
        }

        // create the matrix
        Mat A;
        ierr=MatCreateSeqAIJ(PETSC_COMM_SELF, lns->nlines, lns->nlines, 0, &n_row_nzero[0], &A);CHKERRQ(ierr);
        //ierr=MatSetOption(A, MAT_ROW_ORIENTED, PETSC_TRUE);CHKERRQ(ierr);
        //ierr=MatSetOption(A, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);CHKERRQ(ierr);
        //ierr=MatSetOption(A, MAT_SYMMETRIC, PETSC_TRUE);CHKERRQ(ierr);

        /*
          assemble the adj matrix
        */
        double *value = new double[max_nzero];
        std::fill(value, value+max_nzero, 1.);
		
        for (int i=0 ; i<lns->nlines; i++)
        {
            ierr=MatSetValues(A, 1, &i, xadj[i+1] - xadj[i], adjncy+xadj[i], value, INSERT_VALUES);CHKERRQ(ierr);
            ierr=MatSetValues(A, 1, &i, 1, &i, value, INSERT_VALUES);CHKERRQ(ierr);			
        }

        delete[] value;
		
        ierr=MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
        ierr=MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

        // debug: view the matrix
        //ierr=MatView(A, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);

		
        // color the matrix
        MatColoring   mc;
        ISColoring    iscoloring;
        IS *is;
        int n_color;

        ierr = MatColoringCreate(A, &mc);CHKERRQ(ierr);
        ierr = MatColoringSetType(mc, "greedy");CHKERRQ(ierr);
        ierr = MatColoringSetDistance(mc, 1);CHKERRQ(ierr);
        ierr = MatColoringSetFromOptions(mc);CHKERRQ(ierr);	
        ierr = MatColoringApply(mc,&iscoloring);CHKERRQ(ierr);		
        ierr = MatColoringDestroy(&mc);CHKERRQ(ierr);

        ierr = ISColoringGetIS(iscoloring, &n_color, &is);CHKERRQ(ierr);

        //debug: view the iscoloring
        //ierr = ISColoringView(iscoloring, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
		
        for (int color=0 ; color< n_color; color++)
        {
            int const *mems;
            int n_mems;
            ierr=ISGetIndices(is[color], &mems);CHKERRQ(ierr);
            ierr=ISGetLocalSize(is[color], &n_mems);CHKERRQ(ierr);
				
            for (int li=0 ; li< n_mems; li++)
            {
                ailinetopart[ mems[li] ]= color;
            }

            ierr=ISRestoreIndices(is[color], &mems);CHKERRQ(ierr);
        }
		
        ierr = ISColoringRestoreIS(iscoloring,&is);CHKERRQ(ierr);
        ierr=ISColoringDestroy(&iscoloring);CHKERRQ(ierr);

        // destroy  the mat
        ierr=MatDestroy(&A);CHKERRQ(ierr);
    }
    break;

    // partition according to the lines
    case PART_LINES:
    {
        // assign metis options
        int objval =1;
        int ncon = 1;
        idx_t options[METIS_NOPTIONS];

        ierr=METIS_SetDefaultOptions(options);assert(ierr==METIS_OK);
        //options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;
        options[METIS_OPTION_NUMBERING] = 0;
        options[METIS_OPTION_CONTIG] = 1;
        //options[METIS_OPTION_UFACTOR] = 25;
        ierr=METIS_PartGraphRecursive(&lns->nlines, &ncon, xadj, adjncy,
                                      vwgt, NULL, NULL, &nparts, NULL,
                                      NULL, options, &objval, ailinetopart);
        // ierr=METIS_PartGraphKway(&lns->nlines, &ncon, xadj, adjncy,
        // 						 NULL, NULL, NULL, &nparts, NULL,
        // 						 NULL, options, &objval, aicelltopart);

        switch (ierr)
        {
        case METIS_OK:
            break;
        case METIS_ERROR_INPUT:
            SETERRQ(PETSC_COMM_WORLD, 1, "Metis Error input");
            break;
        case METIS_ERROR_MEMORY:
            SETERRQ(PETSC_COMM_WORLD, 1, "Metis Error memory");
            break;
        case METIS_ERROR:
            SETERRQ(PETSC_COMM_WORLD, 1, "Metis Error unknown");
            break;
        default:
            SETERRQ(PETSC_COMM_WORLD, 1, "Metis Error error!");
            break;
        }
    }
    break;

    }// end of partition switch

    // copy from linetopart to celltopart
    for (LineNode *itline = lns->infohead; itline != NULL; itline=itline->next)
    {
        for (CellNode *itcell = itline->nodebeg ; itcell != NULL; itcell = itcell->next)
        {
            aicelltopart[itcell->icell] = ailinetopart[itline->inum];
        }
    }

    // delete the metis graph
    delete[] xadj;
    delete[] adjncy;
    delete[] vwgt;
    delete[] ailinetopart;
	
	
    return 0;
}

void vCell2MetisEdge(idx_t xadj[], idx_t adjncy[], const int igc1,
                     const int igc2, int &ilc1, int &ilc2){

    ilc2=-1;
    ilc1=-1;

    //find local Current to Next
    for (int ii = xadj[igc1] ; ii < xadj[igc1+1] ; ii++){
        if (adjncy[ii] == igc2){
            ilc2 = ii;
            break;
        }
    }
    assert(ilc2!=-1);
    //printf("%d -> real:%d, found:%d\n", igc1, igc2 ,adjncy[ilc2]);

    //find local Next to Current
    for (int ii = xadj[igc2] ; ii < xadj[igc2+1] ; ii++){
        if (adjncy[ii] == igc1){
            ilc1 = ii;
            break;
        }
    }
    assert(ilc1!=-1);
    //printf("%d -> real:%d, found:%d\n", igc1, igc2 ,adjncy[ilc2]);


}

void vMesh2MetisGraph(Mesh *msh,idx_t xadj[], idx_t adjncy[], idx_t adjwgt[]){
	
    xadj[0] = 0;	
    for (int ii = 0 ; ii < msh->ncell ; ii++ ){

        xadj[ii+1] = xadj[ii];
		
        for (int jj = 0 ; jj < msh->ncellneigh[ii] ; jj++){
            int ineighG, ineighL;
            ineighG = msh->icelltocell[ii*MAXADJ+jj];
            ineighL = ineighG - msh->icellbeg;
            if ( ineighL < msh->ncell ){
                //set adjncy
                adjncy[ xadj[ii+1] ] = ineighL;
                if(adjwgt) adjwgt[ xadj[ii+1] ] = 1;			
            }
            xadj[ii+1]++;
        }
		
    }	
}

void vPrintMetisGraph(const int nnode,idx_t xadj[], idx_t adjncy[], idx_t adjwgt[], FILE *fout){
	
    for (int ii = 0 ; ii < nnode ; ii++ ){		
        fprintf(fout,"cell(%d), [%d]: ", ii, xadj[ii]);
		
        for (int jj = xadj[ii] ; jj < xadj[ii+1] ; jj++){
            fprintf(fout," %d " , adjncy[jj] );
        }

        if (adjwgt){
            fprintf(fout,"|    ");		
            for (int jj = xadj[ii] ; jj < xadj[ii+1] ; jj++){
                fprintf(fout," %d " , adjwgt[jj] );
            }
        }
		
        fprintf(fout,"\n");
    }
	
    fprintf(fout,"[%d]\n", xadj[nnode]);
}

// Extract the meshes
void vExtractPartitions(Mesh *msh, const int npart, idx_t aicelltopart[], Mesh submesh[]){

    /*
      count the number of cells in each submesh
    */
    for (int ii = 0; ii < npart ; ii++) submesh[ii].ncell = 0;
    for (int ii = 0; ii < msh->ncell; ii++)
        submesh[aicelltopart[ii]].ncell++;

    /*
      create the memory for cells and set icellbeg
    */
    {
        int ibeg = 0;
        for (int ii = 0; ii < npart; ii++){
            submesh[ii].ioldcell = new int[submesh[ii].ncell];
            submesh[ii].icellbeg = ibeg;
            ibeg += submesh[ii].ncell;
        }
    }

    /*
      fill the cell data
    */
    for (int ii = 0; ii < npart ; ii++) submesh[ii].ncell = 0;
    for (int ii = 0; ii < msh->ncell; ii++){
        int ipart = aicelltopart[ii];
		
        submesh[ipart].ioldcell[submesh[ipart].ncell] = ii;
        submesh[ipart].ncell++;
    }

    //print number of cells in each part
    fprintf(stderr, "Number of cells in each part:\n");
    for (int ii = 0; ii < npart ; ii++)
    {
        printf("%d\n", submesh[ii].ncell);
    }
	
    /*
      Print what you did
    */
    VERB{
        printf("NPART: %d \n", npart);
        for (int ii = 0; ii < npart ; ii++){
			
            printf("[%d] number of cells: %d icellbeg:%d\n", ii,
                   submesh[ii].ncell, submesh[ii].icellbeg);
			
            printf("====================================\n");
            for (int jj = 0 ; jj < submesh[ii].ncell ; jj++)
                printf("%-5d", submesh[ii].ioldcell[jj]);
			
            printf("\n");
        }
    }
	
	
}


void vCreateAO(const int ntotcell, const int npart, Mesh *submesh, std::map<int,int>& ao){


    //create the numberings
    for (int ii = 0; ii < npart ; ii++){		
        for (int jj = 0 ; jj < submesh[ii].ncell ; jj++){
            int inew, iold;

            iold = submesh[ii].ioldcell[jj];
            inew = jj + submesh[ii].icellbeg;
            ao[iold] = inew;
        }			
    }

    // create the AO, mypetsc->new numbering(NULL, 1-2-3-...), myapp->old numbering
    // map goes from old to new
    // AOCreateBasic(PETSC_COMM_SELF,ntotcell ,aiold,NULL , ao);

    VERB{
        // AOView(*ao, PETSC_VIEWER_STDOUT_SELF);
    }

}



PetscErrorCode vMigrateMesh(Mesh *msh, std::map<int,int>& ao, Mesh *submesh){

    Migrator mig;
    PetscErrorCode ierr;

    /*
      find the local faces
      also find icelltoface if you NEED!
    */
    {
        // fetch the ncellface of the subemsh
        assert(submesh->icelltoface == NULL);
        submesh->ncellface = new int[submesh->ncell];
        submesh->icelltoface = new int[submesh->ncell*MAXADJ];
	
        for (int ii = 0 ; ii < submesh->ncell; ii++){
            submesh->ncellface[ii] = msh->ncellface[ submesh->ioldcell[ii] ];
            memcpy(submesh->icelltoface+ (ii*MAXADJ),
                   msh->icelltoface + (submesh->ioldcell[ii] * MAXADJ),
                   (size_t)(sizeof(int)*MAXADJ) );
        }

        // get the local number of faces and the map
        ierr=vLocalizeIndices(submesh->ncell*MAXADJ, submesh->icelltoface, submesh->nface, submesh->ioldface, mig);CHKERRQ(ierr);

        // set the new value of icelltoface using the map ...
        // or delete them MUHAHAHA!!!
        delete[] submesh->icelltoface; submesh->icelltoface = (int*) NULL;

        // set the iIbdry
        {
            /* count the number of bdry face */
            submesh->iNbdryface = 0;
            for (int ilf = 0 ; ilf < submesh->nface ; ilf++){
                int igf = submesh->ioldface[ilf];
				
                if (msh->ifacetocell[igf*2+1]<0) submesh->iNbdryface++;
            }

            submesh->iIbdryface = new int[submesh->iNbdryface];
			
            /* set the local to global bf */
            int itmp = 0;

            for (int ilf = 0 ; ilf < submesh->nface ; ilf++){
                int igf, igbf, ilbf;

                igf = submesh->ioldface[ilf];

                if (msh->ifacetocell[igf*2+1]<0){

                    igbf = msh->mapGF2GB[igf];
                    //printf("%d, %d\n", igbf, igf);
                    ilbf = itmp;
	
                    submesh->iIbdryface[ilbf]=igbf;
						
                    itmp++;
                }
            }
            assert(itmp == submesh->iNbdryface);
        }
		
		
        mig.clear();
    }
    
	
    /*
      find iface to cell
    */
    {
        submesh->ifacetocell = new int[submesh->nface*2];
		
        // fetch ifacetocell with old numbering
        for(int ii = 0 ; ii < submesh->nface ; ii++){
            int iof = submesh->ioldface[ii];
            const int oleft = msh->ifacetocell[iof*2];
            const int oright = msh->ifacetocell[iof*2+1];
            submesh->ifacetocell[ii*2] = (oleft >= 0 ? ao[oleft] : oleft); 
            submesh->ifacetocell[ii*2+1] = (oright >= 0 ? ao[oright] : oright); 
        }

        // convert to the new numbering
        // ierr=AOApplicationToPetsc(ao, submesh->nface*2, submesh->ifacetocell);CHKERRQ(ierr);
    }

    /*
      fetch all local verts
    */
    {
        submesh->ifacetovert = new int[submesh->nface*2];
		
        // fetch ifacetovert with global numbering
        for(int ii = 0 ; ii < submesh->nface ; ii++){
            int iof = submesh->ioldface[ii];
			
            submesh->ifacetovert[ii*2] = msh->ifacetovert[iof*2];
            submesh->ifacetovert[ii*2+1] = msh->ifacetovert[iof*2+1];
        }

        //unify them and create the map
        ierr=vLocalizeIndices(submesh->nface*2, submesh->ifacetovert, submesh->nvert, submesh->ioldvert, mig);CHKERRQ(ierr);

        //change the numbering for face vertices to local
        for(int ii = 0 ; ii < submesh->nface ; ii++){
            submesh->ifacetovert[ii*2] = mig[ submesh->ifacetovert[ii*2] ];
            submesh->ifacetovert[ii*2+1] = 	mig[ submesh->ifacetovert[ii*2+1] ];
        }
		
        // clear the map
        mig.clear();
    }
	
	
    VERB{
        printf("Face data (%d):\n", submesh->nface);
        for (int ii = 0 ; ii < submesh->nface ; ii++){
            int cl, cr, vl_l, vr_l, vl_g, vr_g;

            cl = submesh->ifacetocell[ii*2];
            cr = submesh->ifacetocell[ii*2 + 1];
            vl_l = submesh->ifacetovert[ii*2];
            vr_l = submesh->ifacetovert[ii*2 + 1];
            vl_g = submesh->ioldvert[ vl_l ];
            vr_g = submesh->ioldvert[ vr_l ];
            printf("numold: %-5d cL:%d cR:%d vL:%d[%d] vR:%d[%d]\n",
                   submesh->ioldface[ii], cl, cr, vl_l, vl_g, vr_l, vr_g);
        }

        printf("Vert Data (%d): \n", submesh->nvert);
        for (int ii = 0 ; ii < submesh->nvert ; ii++)
            printf("%d[%d]   ", ii, submesh->ioldvert[ii]);
        printf("\n");
    }

    return 0;
}

PetscErrorCode vMigrateFECData(Mesh *msh, Mesh *submesh){

    PetscErrorCode ierr;
    Migrator mig;
	
    /* check the mesh to have the node data */
    assert(msh->dnodecoord);
    assert(msh->icelltonode);
    assert(msh->ncellnode);

    /* fetch all local nodes */
    assert(submesh->icelltonode == (int*)NULL);
    assert(submesh->ncellnode == (int*)NULL );

    submesh->ncellnode = new int[submesh->ncell];
    submesh->icelltonode = new int[submesh->ncell*MAXNODE];
	
    for (int ii = 0 ; ii < submesh->ncell; ii++){
        submesh->ncellnode[ii] = msh->ncellnode[ submesh->ioldcell[ii] ];
        memcpy(submesh->icelltonode + (ii*MAXNODE),
               msh->icelltonode + (submesh->ioldcell[ii] * MAXNODE),
               (size_t)(sizeof(int)*MAXNODE) );
    }

    /* get the local number of nodes and the map */
    ierr=vLocalizeIndices(submesh->ncell*MAXNODE, submesh->icelltonode, submesh->nnode, submesh->ioldnode, mig);CHKERRQ(ierr);

    /* set the local number of the nodes */
    for (int ii = 0 ; ii < submesh->ncell ; ii++)
        for(int jj =0 ; jj < submesh->ncellnode[ii] ; jj++){
            int ign = submesh->icelltonode[ii*MAXNODE + jj];
            submesh->icelltonode[ii*MAXNODE + jj] = mig[ign];
        }

    mig.clear();

    return 0;
	
}

PetscErrorCode vLocalizeIndices(const int nonunisize, const int* nonuniidx, int &unisize, int*& uniidx, Migrator& mig){

    mig.clear();

    //first sort nonuniidx
    int *nonuniidx_bup = new int [nonunisize];
    memcpy(nonuniidx_bup, nonuniidx, (size_t)(nonunisize*sizeof(int)));
    std::sort(nonuniidx_bup, nonuniidx_bup+nonunisize);

    // fill in the map
    unisize = 0;
    for (int ii=0; ii<nonunisize; ii++){
        //if(nonuniidx[ii] < 0)
        //	SETERRQ(PETSC_COMM_SELF, 1,"Index should not be negative");
        //assert(nonuniidx[ii] >= 0);
	    
        if ( (nonuniidx_bup[ii] >= 0 ) && (mig.find(nonuniidx_bup[ii]) == mig.end()) ){
            mig[ nonuniidx_bup[ii] ]  = unisize;
            unisize++;
        }
    }
    delete[] nonuniidx_bup;
	
    assert( (uint)unisize == mig.size());

    // fill the uniidx
    uniidx = new int[unisize];
    {
        int tmp = 0;
        for (Migrator::iterator it = mig.begin() ; it != mig.end() ; it++){
            tmp++;
            uniidx[it->second] = it->first;
        }
        assert(tmp == unisize);
    }

	
    return 0;

}


void vWriteMigrated_DotMesh(Mesh *msh, Mesh *submesh, const char fname[]){

    FILE *fl;

    /* open the file */
    fl=fopen(fname, "w"); assert(fl);

    /* write the header */
    fprintf(fl, "%d %d %d %d\n\n", submesh->ncell, submesh->nface, submesh->iNbdryface, submesh->nvert);

    /* write the vertices */
    for (int ii = 0 ; ii < submesh->nvert ; ii++){
        int igv;
        igv = submesh->ioldvert[ii];
        fprintf(fl, "%.15lf %.15lf \n", msh->dvertcoord[igv*2], msh->dvertcoord[igv*2 + 1]);
    }
    fprintf(fl ,"\n");

    /* write the face data */
    for (int ii = 0; ii < submesh->nface ; ii++){
        fprintf(fl, "%d %d ", submesh->ifacetocell[ii*2], submesh->ifacetocell[ii*2+1]);
        fprintf(fl, "%d %d\n", submesh->ifacetovert[ii*2], submesh->ifacetovert[ii*2+1]);		
    }

    /* close */
    fclose(fl);
}

void vWriteMigrated_FEC(Mesh *msh, Mesh *submesh, const char fname[]){

    FILE *fl;

    /* open the file */
    fl=fopen(fname, "w"); assert(fl);

    /* write the header */
    fprintf(fl, "%d %d %d\n\n", submesh->nnode, submesh->ncell, msh->nfemord);

    /* write the nodes */
    for (int ii = 0 ; ii < submesh->nnode ; ii++){
        int ign;
        ign = submesh->ioldnode[ii];
        fprintf(fl, "%.15lf %.15lf \n", msh->dnodecoord[ign*2], msh->dnodecoord[ign*2 + 1]);
    }
    fprintf(fl ,"\n");

    /* write nodes of a cell */
    for (int ii = 0; ii < submesh->ncell ; ii++){
        fprintf(fl, "%d ", submesh->ncellnode[ii]);
        for (int jj = 0 ; jj < submesh->ncellnode[ii] ; jj++)
            fprintf(fl, "%d ", submesh->icelltonode[ii*MAXNODE+jj]);
        fprintf(fl, "\n");
    }

    /* close */
    fclose(fl);
}

void vWriteMigrated_BPATCH(Mesh *msh, Mesh *submesh, const char fname[]){

    int cnt;
    int *ilb2lf = new int[submesh->iNbdryface];

    /* find the index of bdry faces */
    cnt = 0;
    for (int ii = 0 ; ii < submesh->nface ; ii++){
        if (submesh->ifacetocell[ii*2+1]<0){
            ilb2lf[cnt] = ii;
            cnt++;
        }
    }

    /* write the bpatch file */
    FILE *fl;
    fl = fopen(fname, "w");assert(fl);

    for (int ilb = 0 ; ilb < submesh->iNbdryface ; ilb++){
               
        int igb = submesh->iIbdryface[ ilb ];
        fprintf(fl, "%d ", ilb2lf[ilb]);
        //fprintf(fl, "%d ", igb);
               
        for (int jj = 0 ; jj < NBPATCH ; jj++){
            fprintf(fl, "%lf ", msh->adbdryface[igb][jj]);
        }
               
        fprintf(fl, "\n");
    }
       
    fclose(fl);

    delete[] ilb2lf;
}

void vWriteMigrated_Map(Mesh *msh, Mesh *submesh, const char fname[])
{
    FILE *fl;

    /* open the file */
    fl=fopen(fname, "w"); assert(fl);

    /* write nodes of a cell */
    for (int ii = 0; ii < submesh->ncell ; ii++)
    {
        fprintf(fl, "%d \n", submesh->ioldcell[ii]);
    }

    /* close */
    fclose(fl);
}





