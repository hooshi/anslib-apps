#include "mesh.hxx"
#include "lines.hxx"
#include "part.hxx"

#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdarg>
#include <vector>

const char help[] = "YELLOW!";

int main(int argc, char *argv[]){

    PetscErrorCode ierr;
    MeshGr gr;
    Mesh msh;
    LinesMaster lines;

    //writing vtk files
    int vardim[] = {1,1,1,1,1};
    int centering[] = {0,0,0,0,0};

    /*
      initialize
    */
    ierr=PetscInitialize(&argc, &argv, NULL, help);CHKERRQ(ierr);
    ierr=g_vInitialize();CHKERRQ(ierr);
	
    /*
      read the mesh
    */
    {
        char fname[200];

        if(g_fec) sprintf(fname, "%s.mod.mesh", g_fname);
        else sprintf(fname, "%s.mesh", g_fname);
        gr.readDotmesh(fname);
        msh.createFromGrummp(&gr);
        gr.destroy();
	    
    }
    //msh.print();


    //test face distance
    // int iface = msh.nface-1;
    // double dcent[2], dlen;
    // dlen = msh.dFaceLength(iface, dcent);
    // printf("face %d length: %lf cent:(%lf, %lf)\n", iface, dlen ,dcent[0], dcent[1]);

	
    /*
      Lines
    */
    //lines.create(msh);

	
    std::vector<double> face_wgt; face_wgt.resize(msh.nface);
    char weight_file_name[200];

    // read face weights
    sprintf(weight_file_name, "%s.weight", g_flw_fname);
    ierr=face_weight_file(&msh, &face_wgt[0], weight_file_name);CHKERRQ(ierr);
    //ierr=face_weight_distance(&msh, &face_wgt[0]);CHKERRQ(ierr);

    // write face weights
    sprintf(weight_file_name, "%s.weight.vtk", g_fname);
    ierr=face_weight_write_vtk(&msh, &face_wgt[0], weight_file_name);CHKERRQ(ierr);

    // create the lines
    ierr=create_all_lines(&lines, &msh, &face_wgt[0]);CHKERRQ(ierr);

	
    // create lines and output
    printf("Initial lines: %d \n", lines.nlines);
	
    //printLinesMaster(lines);
	
    sprintf(weight_file_name, "%s.l.vtk", g_flw_fname);
    msh.writeVtkLinesMaster(weight_file_name, lines);

    sprintf(weight_file_name, "%s.l", g_flw_fname);
    write_lines_to_file(&lines, &msh, weight_file_name);
	
    // merge and print again
    ierr=merge_all_lines(&lines, &msh, &face_wgt[0]);CHKERRQ(ierr);
    printf("Merged lines: %d \n", lines.nlines);
    sprintf(weight_file_name, "%s.lm.vtk", g_flw_fname);
    msh.writeVtkLinesMaster(weight_file_name, lines);
	
    /*
      partition
    */
    if(g_npart > 0)
    {
        Mesh *submesh = new Mesh[g_npart];
        idx_t *aicelltopart = new idx_t[msh.ncell];
        AO ao = NULL;

        // partition the mesh
        ierr=vMeshPartition(&msh, &lines, g_npart, aicelltopart);CHKERRQ(ierr);
	
        // extract submeshs
        vExtractPartitions(&msh, g_npart, aicelltopart, submesh);
	
        // reorder submeshes (Metis, lines whatever!!!) optional
        //void vReorderSubmesh();
        //delete[] aicelltopart;
	
        // create the ao and move to the new numbering
        vCreateAO(msh.ncell, g_npart, submesh, &ao);

        //write the partitioned mesh
        {		
            double *adcelltopart = new double[msh.ncell];
            double *dnewnumber = new double[msh.ncell];
            int *inewnumber = new int[msh.ncell];
            double *data[] = {adcelltopart, dnewnumber};
		
            for (int zz = 0 ; zz < msh.ncell ; zz++){
                adcelltopart[zz] = aicelltopart[zz];
                inewnumber[zz] = zz;
            }
            ierr=AOApplicationToPetsc(ao, msh.ncell, inewnumber);CHKERRQ(ierr);
            for (int zz = 0 ; zz < msh.ncell ; zz++){
                dnewnumber[zz] = inewnumber[zz];
            }
		
            msh.writeVtk("part.vtk", 2, vardim, centering, data);
		
            delete[] adcelltopart;
            delete[] inewnumber;
            delete[] dnewnumber;
        }

        /* delete these data */
        {
            delete[] msh.dcellcentre; msh.dcellcentre = (double *) NULL;
            delete[] msh.icelltovert; msh.icelltovert = (int *) NULL;
        }
	
        /* read the finite element if needed */
        if (g_fec){
            char fname[300];
            sprintf(fname, "%s.fec", g_fecname);
            gr.readFEC(fname);		
            msh.addNodeFromGrummp(&gr);		
            gr.destroy();
        }

        /* read bpatch if needed */
        if (g_bpatch){
            char fname[300];
            sprintf(fname, "%s.bpatch", g_fname);
            msh.readBPatch(fname);
        }
	
        /* migrate a mesh and then write it to see what happens */
        for (int ii= 0; ii < g_npart ; ii++)
        {
            char strfname[200];

            /* find the migrated mesh and write it */
            ierr=vMigrateMesh(&msh, ao, submesh+ii);CHKERRQ(ierr);
            if (g_fec) sprintf(strfname, "%s_%d.mod.mesh", g_fname, ii);
            else sprintf(strfname, "%s_%d.mesh", g_fname, ii);
            vWriteMigrated_DotMesh(&msh, submesh+ii, strfname);

            //write the bpatch
            if(g_bpatch) {
                sprintf(strfname, "%s_%d.bpatch", g_fname, ii);
                vWriteMigrated_BPATCH(&msh, submesh+ii, strfname);
            }
		

            delete[] submesh[ii].ifacetovert; submesh[ii].ifacetovert = (int *) NULL;
            delete[] submesh[ii].ioldvert; submesh[ii].ioldvert = (int *) NULL;
            delete[] submesh[ii].ioldface; submesh[ii].ioldface = (int *) NULL;
            delete[] submesh[ii].ifacetocell; submesh[ii].ifacetocell = (int *) NULL;

            /* find the migrated nodes and write it*/
            if(g_fec) {
                ierr=vMigrateFECData(&msh, submesh+ii);CHKERRQ(ierr);
                sprintf(strfname, "%s_%d.fec", g_fecname, ii);
                vWriteMigrated_FEC(&msh, submesh+ii, strfname);
            }
		
            /* destroy the mesh */
            submesh[ii].destroy();
        }

        // delete the partitioning stuff
        {
            delete[] aicelltopart;
            delete[] submesh;
            if(ao) AODestroy(&ao);
        }
    } // end of partitioning 
	
    /*
      destroy the mesh
    */
    msh.destroy();
    lines.destroy();
    ierr=PetscFinalize();

    return 0;
}
