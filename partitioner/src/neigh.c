#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <metis.h>
#include <mpi.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscao.h>
#include <time.h>
#include <visit_writer.h>

#define MAXADJ 4
#define MAXRECON2 13
#define MAXRECON3 26
#define MAXRECON4 50

#define dirX 0
#define dirY 1

#define CHK(x) if(!(x)){ SETERRQ(PETSC_COMM_WORLD,1,"INTERNAL ERROR OCCURED"); }
#define MIN__(x,y) ((x)<(y) ? (x) : (y) )
int mpi_rank;
int mpi_size;
char fname[100] = "square.mesh";

/* struct mesh
 *  
 * stores the connectivity for a mesh
 *
 */
struct mesh{
	int ncell;
	int nvert;
	int nface;

	int icellbeg;

	int *ncellface;
	int *ncellneigh;
	int *ioldcell;
	
	int *icelltocell;
	int *icelltovert;
	int *icelltoface;
	
	int *ifacetovert;
	int *ifacetocell;

	double *dvertcoord;
	double *dcellcentre;
	double dmin, dmax, dave;
};

PetscErrorCode mesh_dist(struct mesh* msh, const int cleft , const int cright, double *d){
	CHK ( (cleft >= 0) || (cright >= 0) );
	*d = sqrt( pow(msh->dcellcentre[cleft*2+dirX] - msh->dcellcentre[cright*2+dirX],2) +
			  pow(msh->dcellcentre[cleft*2+dirY] - msh->dcellcentre[cright*2+dirY],2) );

	return 0;
}

PetscErrorCode mesh_create(struct mesh *msh){
	//set all to zero
	msh->dmin = 0;
	msh->ncell = msh->nvert = msh->nface = msh->icellbeg = 0;

	//set int pointers to zero
	msh->icelltoface = msh->icelltovert =
		msh->ifacetovert = msh->icelltocell =
		msh-> ncellneigh = msh->ioldcell =
		msh->ifacetocell = msh->ncellface = (int*)NULL;

	//double[2] pointer
	msh->dvertcoord = msh->dcellcentre = (double *) NULL;

	return 0;
}

PetscErrorCode mesh_destroy(struct mesh *msh){
	if(msh->ncellneigh) free( msh->ncellneigh );
	if(msh->ncellface) free( msh->ncellface );
	
	if(msh->ioldcell) free( msh->ioldcell );
	
	if(msh->icelltocell) free(msh->icelltocell);
	if(msh->icelltovert) free(msh->icelltovert);
	if(msh->icelltoface) free(msh->icelltoface);
	if(msh->ifacetovert) free(msh->ifacetovert);
	if(msh->ifacetocell) free(msh->ifacetocell);
	
	if(msh->dvertcoord) free(msh->dvertcoord);
	if(msh->dcellcentre) free(msh->dcellcentre);
	
	//set all to null
	mesh_create(msh);

	return 0;
}

/* Reads a mesh from grummp
 *
 * The output is:
 * ncells, ncellneigh, icelltocell, dcellcentre.
 *
 * Others will be NULL.
 */
PetscErrorCode master_read_mesh(struct mesh *);

/* Partitions and sends to all or recieves from master.
 *
 * The output is:
 * ncells, ncellneigh, cellnumb(old), icelltocell(oldnumbering), dcellcentre, newnumberingbeg
 * ncellface, icellvert, dvertcoord
 *
 * Others will be NULL.
 */

PetscErrorCode master_compute_adjacency(struct mesh *);
PetscErrorCode slave_compute_adjacency(struct mesh *);

/* Debugging code */
PetscErrorCode master_metis_test();
PetscErrorCode print_adj(const int ncell, const int icellbeg,int const * nxx, int const * celltoxx, const int blocksize);

/* new funcs */
PetscErrorCode mesh_partition(struct mesh*, idx_t**, const double);
PetscErrorCode mesh_write(struct mesh*, idx_t*);

int main(int argc, char *argv[]){
	
	PetscErrorCode ierr;

	/* Adjacency for each processor  */
	/* int ncell , */
	/* 	icellbeg , */
	/* 	*oldcellnumbering = (int*)NULL, */
	/* 	*nneigh = (int*)NULL, */
	/* 	*celltocell = (int*)NULL; */
	struct mesh msh;
	idx_t *part = (idx_t*) NULL;
	double weight = 0;
	mesh_create(&msh);

	/* /\* some temporary arrays to create Petsc Objects *\/ */
	/* int *aoassist , matassist[2]; */
	/* PetscInt const *reconassist; */
	/* PetscScalar unity[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; */
	/* int ii,jj; */

	/* /\* ao for cell numbering *\/ */
	/* AO ao = (AO) NULL; */

	/* /\* matrices *\/ */
	/* Mat matbase = (Mat)NULL, */
	/* 	matgen1 = (Mat)NULL, */
	/* 	matgen2 = (Mat)NULL; */

	/* /\* reconstruction stencils *\/ */
	/* int *nrecon2 = (int*)NULL, */
	/* 	*irecon2 = (int*)NULL, */
	/*     *nrecon3 = (int*)NULL, */
	/* 	*irecon3 = (int*)NULL, */
	/* 	*nrecon4 = (int*)NULL, */
	/* 	*irecon4 = (int*)NULL; */

	/* clock_t clstart, clend; */
		


	//find rank
	ierr=PetscInitialize(&argc, &argv, (char*)NULL, (char*)NULL);CHKERRQ(ierr);
	ierr=MPI_Comm_rank(PETSC_COMM_WORLD, &mpi_rank);CHKERRQ(ierr);
	ierr=MPI_Comm_size(PETSC_COMM_WORLD, &mpi_size);CHKERRQ(ierr);

	//fake rank and size
	mpi_rank = 0 ;
	if (argc> 2) {
		sscanf(argv[2], "%d", &mpi_size);
	}else{
		mpi_size = 3 ;
	}

	/* find file name */
	if (argc > 1) memcpy(fname, argv[1], strlen(argv[1])+1 );
	if (argc > 3) sscanf(argv[3], "%lf", &weight);
	//test reading the mesh
	 ierr=master_read_mesh(&msh);CHKERRQ(ierr); 


    /**********************************************************
	 *
	 * read mesh and compute adj
	 *
	 **********************************************************/
	//clstart = clock();
	
	//if ( mpi_size == 1 ) SETERRQ(PETSC_COMM_WORLD, 1, "MPI size must be greater than one");

	if (mpi_rank == 0){
		//ierr=master_compute_adjacency(&msh);CHKERRQ(ierr);
		ierr=mesh_partition(&msh,&part,weight);CHKERRQ(ierr);
		ierr=mesh_write(&msh, part);CHKERRQ(ierr);
	} 
	/* else{ */
	/* 	ierr=slave_compute_adjacency(&ncell, &icellbeg, &oldcellnumbering, &celltocell, &nneigh);CHKERRQ(ierr); */
	/* } */

	/* clend = clock(); */
	/* ierr=PetscPrintf(PETSC_COMM_WORLD, "Time to read, partition and send to all: %lf \n", (double)(clend - clstart)/CLOCKS_PER_SEC);CHKERRQ(ierr); */
	
    /* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "====================PROCESS %d====================\n", mpi_rank); *\/ */
	/* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "ncelss: %d, newcellnumbegin: %d\n", ncell, icellbeg);CHKERRQ(ierr); *\/ */
	/* /\* ierr = print_adj(ncell, icellbeg, nneigh, celltocell,MAXADJ);CHKERRQ(ierr); *\/ */
    /* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "\n\n");CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedFlush(MPI_COMM_WORLD,stdout);CHKERRQ(ierr); *\/ */
	

	/* /\********************************************************** */
	/*  * create indices to create an ao */
	/*  * petsc -> old */
	/*  * my    -> new */
	/*  **********************************************************\/ */
	
	/* aoassist = (int*)calloc(ncell, sizeof(int)); */
	/* for ( ii = 0 ; ii < ncell ; ii++) */
	/* 	aoassist[ii] = ii + icellbeg ; */
	/* ierr=AOCreateBasic(PETSC_COMM_WORLD, ncell, aoassist, oldcellnumbering, &ao);CHKERRQ(ierr); */
	/* if(aoassist) free(aoassist); */
	/* if(oldcellnumbering) free(oldcellnumbering); */
	/* /\* ierr=AOView(ao, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); *\/ */

	/* // find the new adjacency */
	/* ierr=AOPetscToApplication(ao , ncell*MAXADJ, celltocell);CHKERRQ(ierr); */
	
	/* /\* ierr = print_adj(ncell, icellbeg, nneigh, celltocell,MAXADJ);CHKERRQ(ierr); *\/ */
    /* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "\n\n");CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedFlush(MPI_COMM_WORLD,stdout);CHKERRQ(ierr); *\/ */


	/* /\******************************************************** */
	/*  * */
	/*  * create the adjacency matrix matbase */
	/*  * */
	/*  ********************************************************\/ */
	/* clstart = clock(); */
	
	/* ierr=MatCreate(PETSC_COMM_WORLD, &matbase);CHKERRQ(ierr); */
    /* ierr=MatSetSizes(matbase, ncell, ncell, PETSC_DECIDE, PETSC_DECIDE);CHKERRQ(ierr); */
	/* ierr=MatSetFromOptions(matbase); */

	/* //we are not sure of the size */
	/* ierr=MatSetUp(matbase);CHKERRQ(ierr); */

	/* //start assembling */
	/* // NOTE: */
	/* // IT IS BETTER THAT THE ASSEMBLY BE WRITTEN USING FACES */
	/* // I have note saved the faces so using the cells should work find */
	/* for (ii = 0 ; ii < ncell ; ii++){ */
	/* 	matassist[0] = ii + icellbeg; */
	/* 	for (jj = 0 ; jj < nneigh[ii] ; jj++){ */
	/* 		matassist[1] = celltocell[MAXADJ*ii + jj]; */
	/* 		ierr=MatSetValues(matbase, 2, matassist, 2, matassist, unity, INSERT_VALUES);CHKERRQ(ierr); */
	/* 	} */
	/* } */
	/* ierr=MatAssemblyBegin(matbase, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */
	/* ierr=MatAssemblyEnd(matbase, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr); */

	/* clend = clock(); */
	/* ierr=PetscPrintf(PETSC_COMM_WORLD, "Time to create adj mat: %lf \n", (double)(clend - clstart)/CLOCKS_PER_SEC);CHKERRQ(ierr); */
	
	/* //see the matrix */
	/* //ierr=MatView(matbase, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); */

	/* //check owner ship range */
	/* /\* ierr=MatGetOwnershipRange(matbase, &ii, &jj);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "[%d]: from row: %d to %d\n",mpi_rank,ii,jj);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedFlush(MPI_COMM_WORLD,stdout);CHKERRQ(ierr); *\/ */

	/* /\******************************************************** */
	/*  * */
	/*  * Second layer of neighbours */
	/*  * */
	/*  ********************************************************\/ */
	/* clstart = clock(); */
	
	/* ierr=MatMatMult(matbase, matbase, MAT_INITIAL_MATRIX, 1.3, &matgen1);CHKERRQ(ierr); */
	
	/* /\* ierr=MatView(matgen1, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); *\/ */
	/* /\* ierr=MatGetOwnershipRange(matgen1, &ii, &jj);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "[%d]: from row: %d to %d\n",mpi_rank,ii,jj);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedFlush(MPI_COMM_WORLD,stdout);CHKERRQ(ierr); *\/ */

	/* //lets get the indices */
	/* nrecon2 = (int*) calloc(ncell, sizeof(int)); */
	/* irecon2 = (int*) calloc(ncell*MAXRECON2 , sizeof(int)); */

	/* for (ii = 0 ; ii < ncell ; ii++){ */
	/* 	ierr=MatGetRow(matgen1 , ii+icellbeg, nrecon2+ii, &reconassist, NULL);CHKERRQ(ierr); */
	/* 	for(jj = 0 ; jj < nrecon2[ii] ; jj++) */
	/* 		irecon2[ii*MAXRECON2+jj] = reconassist[jj]; */
	/* 	ierr=MatRestoreRow(matgen1, ii+icellbeg, NULL,  &reconassist, NULL);CHKERRQ(ierr); */
	/* } */

	/* clend = clock(); */
	/* ierr=PetscPrintf(PETSC_COMM_WORLD, "Time to find second layer: %lf \n", (double)(clend - clstart)/CLOCKS_PER_SEC);CHKERRQ(ierr); */
	
	/* /\* ierr = print_adj(ncell, icellbeg, nrecon2, irecon2, MAXRECON2);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "\n\n");CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedFlush(MPI_COMM_WORLD,stdout);CHKERRQ(ierr); *\/ */

	/* /\******************************************************** */
	/*  * */
	/*  * Third layer of neighbours */
	/*  * */
	/*  ********************************************************\/ */
	/* clstart = clock(); */
	
	/* ierr=MatMatMult(matbase, matgen1, MAT_INITIAL_MATRIX, 1.4, &matgen2);CHKERRQ(ierr); */
	
	/* //ierr=MatView(matgen2, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); */
	/* /\* ierr=MatGetOwnershipRange(matgen2, &ii, &jj);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "[%d]: from row: %d to %d\n",mpi_rank,ii,jj);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedFlush(MPI_COMM_WORLD,stdout);CHKERRQ(ierr); *\/ */

	/* //lets get the indices */
	/* nrecon3 = (int*) calloc(ncell, sizeof(int)); */
	/* irecon3 = (int*) calloc(ncell*MAXRECON3 , sizeof(int)); */

	/* for (ii = 0 ; ii < ncell ; ii++){ */
	/* 	ierr=MatGetRow(matgen2 , ii+icellbeg, nrecon3+ii, &reconassist, NULL);CHKERRQ(ierr); */
	/* 	for(jj = 0 ; jj < nrecon3[ii] ; jj++) */
	/* 		irecon3[ii*MAXRECON3+jj] = reconassist[jj]; */
	/* 	ierr=MatRestoreRow(matgen2, ii+icellbeg, NULL,  &reconassist, NULL);CHKERRQ(ierr); */
	/* } */

	/* clend = clock(); */
	/* ierr=PetscPrintf(PETSC_COMM_WORLD, "Time to find third layer: %lf \n", (double)(clend - clstart)/CLOCKS_PER_SEC);CHKERRQ(ierr); */
	
	/* /\* ierr = print_adj(ncell, icellbeg, nrecon3, irecon3, MAXRECON3);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "\n\n");CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedFlush(MPI_COMM_WORLD,stdout);CHKERRQ(ierr); *\/ */

	/* /\******************************************************** */
	/*  * */
	/*  * Forth layer of neighbours */
	/*  * */
	/*  ********************************************************\/ */
	/* clstart = clock(); */

	/* ierr=MatDestroy(&matgen1);CHKERRQ(ierr); //is there a better way?  */
	/* ierr=MatMatMult(matbase, matgen2, MAT_INITIAL_MATRIX, PETSC_DECIDE, &matgen1);CHKERRQ(ierr); */
	
	/* //ierr=MatView(matgen1, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); */
	/* /\* ierr=MatGetOwnershipRange(matgen1, &ii, &jj);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "[%d]: from row: %d to %d\n",mpi_rank,ii,jj);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedFlush(MPI_COMM_WORLD,stdout);CHKERRQ(ierr); *\/ */

	/* //lets get the indices */
	/* nrecon4 = (int*) calloc(ncell, sizeof(int)); */
	/* irecon4 = (int*) calloc(ncell*MAXRECON4 , sizeof(int)); */

	/* for (ii = 0 ; ii < ncell ; ii++){ */
	/* 	ierr=MatGetRow(matgen1 , ii+icellbeg, nrecon4+ii, &reconassist, NULL);CHKERRQ(ierr); */
	/* 	for(jj = 0 ; jj < nrecon4[ii] ; jj++) */
	/* 		irecon4[ii*MAXRECON4+jj] = reconassist[jj]; */
	/* 	ierr=MatRestoreRow(matgen1, ii+icellbeg, NULL,  &reconassist, NULL);CHKERRQ(ierr); */
	/* } */

	/* clend = clock(); */
	/* ierr=PetscPrintf(PETSC_COMM_WORLD, "Time to find forth layer: %lf \n", (double)(clend - clstart)/CLOCKS_PER_SEC);CHKERRQ(ierr); */
	
	/* /\* ierr = print_adj(ncell, icellbeg, nrecon4, irecon4, MAXRECON4);CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedPrintf(PETSCCOMM_WORLD, "\n\n");CHKERRQ(ierr); *\/ */
	/* /\* ierr=PetscSynchronizedFlush(MPI_COMM_WORLD,stdout);CHKERRQ(ierr); *\/ */



	/********************************************************
	 *
	 * Free memory
	 *
	 ********************************************************/

	/* if(celltocell) free(celltocell); */
	/* if(nneigh) free(nneigh); */
	mesh_destroy(&msh);
	if (part) free(part);
	
	/* if(nrecon2) free(nrecon2); */
	/* if(irecon2) free(irecon2); */
	/* if(nrecon3) free(nrecon3); */
	/* if(irecon3) free(irecon3); */
	/* if(nrecon4) free(nrecon4); */
	/* if(irecon4) free(irecon4); */
	/* if(matbase) { ierr=MatDestroy(&matbase);CHKERRQ(ierr); } */
	/* if(matgen1) { ierr=MatDestroy(&matgen1);CHKERRQ(ierr); } */
	/* if(matgen2) { ierr=MatDestroy(&matgen2);CHKERRQ(ierr); } */
	/* if(ao ) { ierr=AODestroy(&ao);CHKERRQ(ierr); } */
	ierr=PetscFinalize();
	
	return 0;
}

PetscErrorCode master_read_mesh(struct mesh *msh){
	FILE *fin;
	PetscErrorCode ierr;

	int vdown, vup;
	int cleft, cright;
	int ii, jj,  itmp, nread;

	double dist;
	/*
	  open the file
	*/
	fin=fopen(fname, "r");
	CHK(fin);
	nread=fscanf(fin, "%d %d %d %d", &msh->ncell, &msh->nface, &itmp, &msh->nvert);
	CHK(nread==4);

	/*
	  create data
	*/
	
	//will keep
    msh->icelltocell = (int*)calloc((size_t)(msh->ncell*MAXADJ), sizeof(int)); 
	msh->ncellneigh = (int*)calloc((size_t)msh->ncell, sizeof(int));
	msh->ncellface = (int*)calloc((size_t)msh->ncell, sizeof(int)); 
	msh->dcellcentre = (double*)calloc((size_t)msh->ncell*2, sizeof(double));

	memset(msh->ncellface, 0, (size_t)(msh->ncell*sizeof(int)) );
	memset(msh->ncellneigh, 0, (size_t)(msh->ncell*sizeof(int)) );
	memset(msh->icelltocell, -1, (size_t)(msh->ncell*sizeof(int)*MAXADJ) );

	//will destroy before end of this func
	msh->icelltoface = (int*)calloc((size_t)(msh->ncell*MAXADJ), sizeof(int));
	msh->icelltovert = (int*)calloc((size_t)(msh->ncell*MAXADJ), sizeof(int)); 
	msh->ifacetovert = (int*)calloc((size_t)(msh->nface*2), sizeof(int));
	msh->ifacetocell = (int*)calloc((size_t)(msh->nface*2), sizeof(int));
	msh->dvertcoord = (double*)calloc((size_t)(msh->nvert*2), sizeof(double));
	
	memset(msh->icelltoface, -1, (size_t)(msh->ncell*sizeof(int)*MAXADJ) );
	memset(msh->icelltovert, -1, (size_t)(msh->ncell*sizeof(int)*MAXADJ) );
	
	/* for (ii=0 ; ii < msh->ncell ; ii++) printf("%d\n",(msh->ncellneigh)[ii]); */
	/* for (ii=0 ; ii < msh->ncell*MAXADJ ; ii++) printf("%d\n",(msh->icelltocell)[ii]); */
	
    /*
	  read vertices
	*/
	for (ii=0 ; ii<msh->nvert ; ii++){
		nread=fscanf(fin,"%lf %lf", &msh->dvertcoord[ii*2], &msh->dvertcoord[ii*2+1]);
		CHK(nread==2);
	}

	
	/*
	  read faces
	*/
    for (ii=0; ii < msh->nface ; ii++){
		//read
		nread=fscanf(fin,"%d %d %d %d", &cleft, &cright, &vdown, &vup);
		CHK(nread==4);
		
		//fprintf(stderr,"[%d] %d %d\n", ii, cleft, cright);

		//put boundary to right
		if( cleft < 0 ){
			CHK(cright >= 0);
			itmp = cleft; cleft = cright; cright = itmp;
			itmp = vdown; vdown = vup; vup = itmp;
		}

		/* fill face data */
		msh->ifacetovert[ii*2] = vdown;
		msh->ifacetovert[ii*2 + 1] = vup;
		msh->ifacetocell[ii*2] = cleft;
		msh->ifacetocell[ii*2 + 1] = cright;

		/* fill the cell to cell data */
		if (cright >= 0){
			msh->icelltocell[cleft*MAXADJ + (msh->ncellneigh)[cleft] ] = cright;
			msh->ncellneigh[cleft]++;
			msh->icelltocell[cright*MAXADJ + (msh->ncellneigh)[cright] ] = cleft;
			msh->ncellneigh[cright]++;
		}
		
		/* fill the cell to face data */
		msh->icelltoface[cleft*MAXADJ + (msh->ncellface)[cleft] ] = ii;
		msh->ncellface[cleft]++;
		if (cright >= 0){
		msh->icelltoface[cright*MAXADJ + (msh->ncellface)[cright] ] = ii;
		msh->ncellface[cright]++;
		}		

	}//end read faces

	/* fill the cell to vert data */
	//over the cells
	for (ii = 0 ; ii < msh->ncell ; ii++)
	{
		// for the first face
		int nreadverts = 0;
		int igf = msh->icelltoface[ii*MAXADJ+0],ilf, igv;
		
	    // if the cell is on the left
		if (msh->ifacetocell[igf*2] == ii ){
			msh->icelltovert[ii*MAXADJ] = msh->ifacetovert[igf*2];
			msh->icelltovert[ii*MAXADJ+1] = msh->ifacetovert[igf*2+1];
		}
		//cell on the right
		else if(msh->ifacetocell[igf*2+1] == ii ){
			msh->icelltovert[ii*MAXADJ] = msh->ifacetovert[igf*2+1];
			msh->icelltovert[ii*MAXADJ+1] = msh->ifacetovert[igf*2];
		}else{
			SETERRQ4(PETSC_COMM_WORLD,1,"cell:%d , face: %d , cl: %d, cr:%d ",
					 ii, igf, msh->ifacetocell[igf*2], msh->ifacetocell[igf*2+1]);
		}
		nreadverts+=2;

		// for to find all the other verts
		for (;nreadverts < msh->ncellface[ii] ; nreadverts++){
			//loop over all faces to find the one containing the next vert
			igv = -1;
			for (ilf = 0 ; ilf < msh->ncellface[ii] ; ilf++){
				igf = msh->icelltoface[ii*MAXADJ+ilf];
				// if cell was to left and first vert was the previous one read
				if ( (msh->ifacetocell[igf*2] == ii) && (msh->ifacetovert[igf*2] == msh->icelltovert[ii*MAXADJ+nreadverts-1] ) ){
					igv = msh->ifacetovert[igf*2+1];
					break;
				}
				// or cell was to right and second vert was the previous one read
				if  ( (msh->ifacetocell[igf*2+1] == ii) && (msh->ifacetovert[igf*2+1] == msh->icelltovert[ii*MAXADJ+nreadverts-1]) ){
					igv = msh->ifacetovert[igf*2];
					break;
				}
			}

			//check if a face was found
			if (igv == -1 ) CHK(0);

			//set the next vert
			msh->icelltovert[ii*MAXADJ+nreadverts] = igv;
		}
	}

	/*
	  find cell centres
	*/
	for (ii=0 ; ii < msh->ncell ; ii++){
		for (jj = 0; jj < msh->ncellface[ii] ; jj++){
			int igv = msh->icelltovert[ii*MAXADJ + jj];
			msh->dcellcentre[ii*2 + dirX] += msh->dvertcoord[igv*2 + dirX] / (double)msh->ncellface[ii];
			msh->dcellcentre[ii*2 + dirY] += msh->dvertcoord[igv*2 + dirY] / (double)msh->ncellface[ii];
		}
	}

	/*
	 * find minimum distance
	 */
	msh->dmin = 1e12;
	msh->dmax = 0;
	msh->dave = 0;
	for (ii = 0 ; ii < msh->nface ; ii++){
		
		cleft  = msh->ifacetocell[ii*2];
		cright  = msh->ifacetocell[ii*2+1];

		if( (cleft >=0) && (cright >= 0) ){
			ierr = mesh_dist(msh, cleft, cright, &dist);CHKERRQ(ierr);
			msh->dmin = MIN__(msh->dmin, dist);
			msh->dmax = fmax(msh->dmax, dist);
			msh->dave += dist;
			//printf("%d %d %lf \n", cleft, cright, dist);
		}
		
		
	}
	msh->dave /= msh->ncell;
	printf("min dist: %lf\n" , msh->dmin);
	printf("max dist: %lf\n" , msh->dmax);
	printf("ave dist: %lf\n" , msh->dave);


 	/*
	  close the file
	*/
	fclose(fin);


	/*
	  print to check everything is correct
	*/

	//verts
	/* printf("VERT:\n"); */
	/* for (ii=0 ; ii < msh->nvert ; ii++) printf("%lf %lf\n",msh->dvertcoord[ii*2],msh->dvertcoord[ii*2+1]); */
	/* printf("\n"); */
	
	/* cells */
 	/* printf("\nCELLS:\n"); */
	/* for (ii=0 ; ii < msh->ncell ; ii++){ */

	/* 	//neighbour cells */
	/* 	printf("[%d] cent: %lf,%lf nneigh:%d ", ii, msh->dcellcentre[ii*2+dirX], msh->dcellcentre[ii*2+dirY] */
	/* 		   , (msh->ncellneigh)[ii]); */
	/* 	for (jj = 0; jj < (msh->ncellneigh)[ii] ; jj++) */
	/* 		printf("%d ", (msh->icelltocell)[ii*MAXADJ + jj]); */
	/* 	printf("| \t"); */

	/* 	//neighbour faces */
	/* 	printf("nfaces: %d ", (msh->ncellface)[ii]); */
	/* 	for (jj = 0; jj < (msh->ncellface)[ii] ; jj++) */
	/* 		printf("%d ", (msh->icelltoface)[ii*MAXADJ + jj]); */
	/* 	printf("| \t"); */

	/* 	//vertices */
	/* 	printf("verts: "); */
	/* 	for (jj = 0; jj < (msh->ncellface)[ii] ; jj++) */
	/* 		printf("%d ", (msh->icelltovert)[ii*MAXADJ + jj]); */
	/* 	printf("\n"); */
	/* } */

	//faces
	/* for (ii=0 ; ii < msh->nface ; ii++){ */
	/* 	printf("[%d] cl:%d cr:%d vl:%d vu:%d \n",  ii, */
	/* 		   msh->ifacetocell[ii*2], msh->ifacetocell[ii*2+1], */
	/* 		   msh->ifacetovert[ii*2], msh->ifacetovert[ii*2+1] ); */
	/* } */


	/*
	  delete extra things
	*/
	//free(msh->dvertcoord); msh->dvertcoord = (double*)NULL; need to write the mesh

	//these can be used later
	free(msh->ifacetovert); msh->ifacetovert = (int*)NULL;
	free(msh->ifacetocell); msh->ifacetocell = (int *)NULL;
	
	//free(msh->icelltovert); msh->icelltovert = (int *)NULL; need to write the mesh
	free(msh->icelltoface); msh->icelltoface = (int *)NULL;
	//free(msh->ncellface); msh->ncellface = (int *)NULL; need to write the mesh

	return 0;
}

PetscErrorCode master_metis_test(){
	int ierr;
	int ii;
	idx_t  ncell = 16;
	idx_t ncon = 1;
	idx_t nparts = 2;
	idx_t objval;
	idx_t part[ncell];
	idx_t xadj[] = {0,2,4,6,8,11,14,16,18,20,23,26,28,31,34,37,40};
	idx_t adjncy[] = { 11,9,      //0 - 0
					 4,8,       //1 - 2
					 15,7,      //2 - 4
					 6,12,      //3 - 6
					 1,10,12,   //4 - 8
					 15,11,9,   //5 - 11
					 14,3,      //6 - 14
					 13,2,      //7 - 16
					 10,1,      //8 - 18
					 5,0,14,    //9 - 20
					 8,4,13,    //10 - 23
					 5,0,       //11 - 26
					 4,3,14,    //12 - 28
					 15,7,10,   //13 - 31
					 6,12,9,    //14 - 34 
					 13,2,5     //15 - 37 -> 40
					 
	};
	
	ierr=METIS_PartGraphKway(&ncell, &ncon, xadj, adjncy,
						    NULL, NULL, NULL, &nparts, NULL,
							 NULL, NULL, &objval, part);
	CHK(ierr==METIS_OK);
	printf("objval: %d\n", objval);
	for (ii=0; ii < ncell ; ii++){
		printf("[%d] -> part[%d]\n", ii, part[ii]);
	}

	return 0;
}

PetscErrorCode master_compute_adjacency(struct mesh *mshmast){
//(int *ncelllocal,  int *newcellnumberingbegin,
//int **oldcellnumbering, int **celltocellold, int **nneighlocal){

	//the serial and partitioned meshes
    struct mesh msh; mesh_create(&msh);
	struct mesh mshslav; mesh_create(&mshslav);
	mesh_create(mshmast);
	
	//metis stuff
	int ierr;
	int ii, jj, kk, ww ;
	idx_t ncon = 1;
	idx_t nparts = mpi_size;
	idx_t objval;
	idx_t *part = (idx_t*)NULL;
	idx_t *xadj = (idx_t*)NULL;
	idx_t *adjncy =  (idx_t*)NULL;
	


	/*
	 *
	 * read the mesh and save the adjacency
	 *
	 */
	master_read_mesh(&msh);

	/*
	 *
	 * compute adjacency for metis
	 *
	 */
	//allocate memory
	part = (idx_t*)calloc((size_t)msh.ncell, sizeof(idx_t));
	xadj = (idx_t*)calloc((size_t)(msh.ncell+1), sizeof(idx_t));
	adjncy = (idx_t*)calloc((size_t)(msh.ncell*MAXADJ), sizeof(idx_t));
	

	/* create metis input data */
	xadj[0] = 0;
	for (ii = 0 ; ii < msh.ncell ; ii++ ){
		xadj[ii+1] = xadj[ii];
	    for (jj = 0 ; jj < msh.ncellneigh[ii] ; jj++){
			adjncy[ xadj[ii+1] ] = msh.icelltocell[ii*MAXADJ+jj];
			xadj[ii+1]++;
		}	
	}
	//test what you have created!!!
	for (ii = 0 ; ii < msh.ncell ; ii++ ){
		printf("[%d] ", xadj[ii]);
	    for (jj = 0 ; jj < msh.ncellneigh[ii] ; jj++){
		    printf(" %d " , adjncy[ xadj[ii] + jj ] );
		}
		printf("\n");
	}
	printf("[%d]\n", xadj[msh.ncell]);
		

	/*
	 *
	 * partition using metis
	 *
	 */
	ierr=METIS_PartGraphKway(&msh.ncell, &ncon, xadj, adjncy,
							 NULL, NULL, NULL, &nparts, NULL,
							 NULL, NULL, &objval, part);
	CHK(ierr==METIS_OK);
	/* test partitioning */
	printf("objval: %d\n", objval);
	for (ii=0; ii < msh.ncell ; ii++){
		printf("[%d] -> part[%d]\n", ii, part[ii]);
	}
	
	//delete the data metis wanted
	if(adjncy) free(adjncy);
	if(xadj) free(xadj);

	/* /\* */
	/*  * */
	/*  * Calculate the data for each processor and send it */
	/*  * to him */
	/*  * */
	/* *\/ */
	/* slave_newcellnumberingbegin = 0; */
	/* for (ii = 0 ; ii < mpi_size ; ii++){ */
	/* 	//count the number of cells */
	/* 	slave_ncelllocal = 0; */
	/* 	for ( jj = 0 ; jj < ncell ; jj++) */
	/* 		if (part[jj] == ii) slave_ncelllocal++; */

	/* 	//create an array of oldcellnumbering and adjacency */
	/* 	slave_oldcellnumbering = (int*)malloc((size_t)slave_ncelllocal*sizeof(int)); */
	/* 	slave_celltocellold = (int*)malloc((size_t)MAXADJ*slave_ncelllocal*sizeof(int)); */
	/* 	slave_nneighlocal = (int*)malloc((size_t)slave_ncelllocal*sizeof(int)); */
		
	/* 	//remove valgrind errors */
	/* 	memset(slave_celltocellold, -2, (size_t)(MAXADJ*slave_ncelllocal*sizeof(int))); */

		
	/* 	kk = 0; */
	/* 	for ( jj = 0 ; jj < ncell ; jj++){ */
	/* 		if (part[jj] == ii){ */
	/* 			//save the cell number */
	/* 			slave_oldcellnumbering[kk] = jj; */
	/* 			slave_nneighlocal[kk] = ncellneigh[jj]; */

	/* 			//save the adjacency */
	/* 			for (ww=0 ; ww < ncellneigh[jj] ; ww++) */
	/* 				slave_celltocellold[ww+kk*MAXADJ] = celltocell[ww+jj*MAXADJ]; */
				
	/* 			kk++; */
	/* 		} */
	/* 	} */
	/* 	CHK(kk==slave_ncelllocal); */

		
 	/* 	//printf for testing */
	/* 	/\* printf("====================PROCESS %d====================\n", ii); *\/ */
	/* 	/\* printf("ncelss: %d, newcellnumbegin: %d\n", slave_ncelllocal, slave_newcellnumberingbegin); *\/ */
	/* 	/\* printf("adjacency: \n"); *\/ */
	/* 	/\* for ( kk = 0 ; kk < slave_ncelllocal ; kk++){ *\/ */
	/* 	/\* 	printf("[%d] |%d| " , slave_oldcellnumbering[kk], slave_nneighlocal[kk]); *\/ */
			
	/* 	/\* 	for (ww=0 ; ww < slave_nneighlocal[kk] ; ww++) *\/ */
	/* 	/\* 		printf(" %d ", slave_celltocellold[ww+kk*MAXADJ] ); *\/ */
	/* 	/\* 	printf ("\n"); *\/ */
	/* 	/\* } *\/ */
	/* 	/\* printf("\n\n"); *\/ */


	/* 	//send through mpi or save to our own data */
	/* 	if (ii == 0){ */
	/* 		*ncelllocal = slave_ncelllocal; */
	/* 		*newcellnumberingbegin = slave_newcellnumberingbegin; */
	/* 		*oldcellnumbering = slave_oldcellnumbering; */
	/* 		*nneighlocal = slave_nneighlocal; */
	/* 		*celltocellold = slave_celltocellold;	       */
	/* 	} */
	/* 	else{ */

	/* 		//do some sending */
	/* 	    ierr=MPI_Send(&slave_ncelllocal, 1, MPI_INT, ii, 1 /\*tag*\/,PETSC_COMM_WORLD);CHKERRQ(ierr); */
	/* 		ierr=MPI_Send(&slave_newcellnumberingbegin, 1, MPI_INT, ii, 1 /\*tag*\/,PETSC_COMM_WORLD);CHKERRQ(ierr); */
			
	/* 		ierr=MPI_Send(slave_oldcellnumbering, slave_ncelllocal, MPI_INT, ii, 1 /\*tag*\/,PETSC_COMM_WORLD);CHKERRQ(ierr); */
	/* 		ierr=MPI_Send(slave_celltocellold, slave_ncelllocal*MAXADJ, MPI_INT, ii, 1 /\*tag*\/,PETSC_COMM_WORLD);CHKERRQ(ierr); */
	/* 		ierr=MPI_Send(slave_nneighlocal, slave_ncelllocal, MPI_INT, ii, 1 /\*tag*\/,PETSC_COMM_WORLD);CHKERRQ(ierr); */
			

	/* 		//delete the data used to store */
	/* 		if(slave_oldcellnumbering) free(slave_oldcellnumbering); */
	/* 		if(slave_celltocellold) free(slave_celltocellold); */
	/* 		if(slave_nneighlocal) free(slave_nneighlocal); */
	/* 	} */
		
	/* 	//update the newcellnumberingbegin for next processor */
	/* 	slave_newcellnumberingbegin += slave_ncelllocal; */
	/* } */

	
    // free the serial mesh
	mesh_destroy(&msh);
	// free part used for metis
    if(part) free(part);

	return 0;
}

PetscErrorCode slave_compute_adjacency(struct mesh *msh){
/* (int *ncelllocal,  int *newcellnumberingbegin, */
/* int **oldcellnumbering, int **celltocellold, int**nneigh){ */

    MPI_Status status;
	PetscErrorCode ierr;
	
	mesh_create(msh);
	
	ierr=MPI_Recv(&msh->ncell, 1, MPI_INT, 0, 1, PETSC_COMM_WORLD, &status);CHKERRQ(ierr);
	ierr=MPI_Recv(&msh->icellbeg, 1, MPI_INT, 0, 1, PETSC_COMM_WORLD, &status);CHKERRQ(ierr);

	msh->ioldcell = (int*)malloc((size_t)(msh->ncell)*sizeof(int));
    msh->icelltocell = (int*)malloc((size_t)(msh->ncell)*MAXADJ*sizeof(int));
    msh->ncellneigh = (int*)malloc((size_t)(msh->ncell)*sizeof(int));
	
	ierr=MPI_Recv(msh->ioldcell, msh->ncell, MPI_INT, 0, 1 /*tag*/,PETSC_COMM_WORLD,&status);CHKERRQ(ierr);
	ierr=MPI_Recv(msh->icelltocell, msh->ncell*MAXADJ , MPI_INT, 0, 1, PETSC_COMM_WORLD, &status);CHKERRQ(ierr);
	ierr=MPI_Recv(msh->ncellneigh, msh->ncell, MPI_INT, 0, 1, PETSC_COMM_WORLD, &status);CHKERRQ(ierr);

	return 0;
}

PetscErrorCode print_adj(const int ncell, const int icellbeg, int const * nxx, int const * celltoxx, const int blocksize){

	int ww, kk;
	PetscErrorCode ierr;
	
	for ( kk = 0 ; kk < ncell ; kk++){
		ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "[%d] cell(l:%d g:%d) |%d| " , mpi_rank, kk,kk + icellbeg, nxx[kk]);CHKERRQ(ierr);
	
		for (ww=0 ; ww < nxx[kk] ; ww++)
			ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, " %d ", celltoxx[ww+kk*blocksize] );CHKERRQ(ierr);
		ierr=PetscSynchronizedPrintf (PETSC_COMM_WORLD, "\n"); CHKERRQ(ierr);
	}
	ierr=PetscSynchronizedPrintf(PETSC_COMM_WORLD, "\n\n");CHKERRQ(ierr);

	return 0;
}


PetscErrorCode mesh_partition(struct mesh *msh, idx_t **part, const double weight){


	double dist;
	int ierr;
	int ii, jj, otherc;
	int *cnt = (int*) NULL;

	//core - metis
	idx_t ncon = 1;
	idx_t nparts = mpi_size;
	idx_t objval;
	idx_t *xadj = (idx_t*)NULL;
	idx_t *adjncy =  (idx_t*)NULL;

	//weights
	idx_t *adjwgt;

	//options
	idx_t options[METIS_NOPTIONS];

	

	/*
	 * metis options
	 *
	 */
	ierr=METIS_SetDefaultOptions(options);CHK(ierr==METIS_OK);
	// check this.
	options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;
	options[METIS_OPTION_NUMBERING] = 0;
	options[METIS_OPTION_CONTIG] = 1;
	
	
	/*
	 *
	 * compute adjacency for metis
	 *
	 */
	//allocate memory
	*part = (idx_t*)calloc((size_t)msh->ncell, sizeof(idx_t));
	xadj = (idx_t*)calloc((size_t)(msh->ncell+1), sizeof(idx_t));
	adjncy = (idx_t*)calloc((size_t)(msh->ncell*MAXADJ), sizeof(idx_t));
    adjwgt = (idx_t*)calloc((size_t)(msh->ncell*MAXADJ), sizeof(idx_t));
	cnt = (int*)calloc((size_t)(mpi_size), sizeof(int));
	
	// create metis adjacency and weigts
	xadj[0] = 0;
	for (ii = 0 ; ii < msh->ncell ; ii++ ){
		xadj[ii+1] = xadj[ii];
	    for (jj = 0 ; jj < msh->ncellneigh[ii] ; jj++){
			//set adjncy
			adjncy[ xadj[ii+1] ] = otherc = msh->icelltocell[ii*MAXADJ+jj];
			//set weight
			ierr=mesh_dist(msh, ii, otherc, &dist);CHKERRQ(ierr);
			//adjwgt[ xadj[ii+1] ] = (int) (dist / msh->dmin);
			adjwgt[ xadj[ii+1] ] = ((idx_t) (msh->dave / dist) );
			
			xadj[ii+1]++;
		}	
	}
	
	//test what you have created!!!
	/* for (ii = 0 ; ii < msh->ncell ; ii++ ){ */
	/* 	printf("[%d] ", xadj[ii]); */
	/*     for (jj = 0 ; jj < msh->ncellneigh[ii] ; jj++){ */
	/* 	    printf(" %d " , adjncy[ xadj[ii] + jj ] ); */
	/* 	} */
	/* 	printf("|    "); */
		
	/* 	for (jj = 0 ; jj < msh->ncellneigh[ii] ; jj++){ */
	/* 	    printf(" %d " , adjwgt[ xadj[ii] + jj ] ); */
	/* 	} */
	/* 	printf("\n"); */
	/* } */
	/* printf("[%d]\n", xadj[msh->ncell]); */

		

	/*
	 *
	 * partition using metis
	 *
	 */
	ierr=METIS_PartGraphKway(&msh->ncell, &ncon, xadj, adjncy,
							 NULL, NULL, adjwgt, &nparts, NULL,
							 NULL, options, &objval, *part);
	CHK(ierr==METIS_OK);
	/* test partitioning */
	/* printf("objval: %d\n", objval); */
	/* for (ii=0; ii < msh->ncell ; ii++){ */
	/* 	printf("[%d] -> part[%d]\n", ii, (*part)[ii]); */
	/* } */

	//count in each partition
	memset(cnt, 0, sizeof(int)*mpi_size);
	for (ii = 0; ii < msh->ncell; ii++){
		cnt[ (*part)[ ii ] ] ++;
	}
	for (ii = 0 ; ii < mpi_size ; ii++)
		printf("sizepart[%d]: %d\n", ii, cnt[ii]);
	
	//delete the data metis wanted
	if(adjncy) free(adjncy);
	if(xadj) free(xadj);
	if(adjwgt) free(adjwgt);
	if(cnt) free(cnt);

	return 0;
}


PetscErrorCode mesh_write(struct mesh *msh, idx_t *part){

	int ii,jj;
	
	int useBinary = 0;
	float *pts, *vars;
	int *celltypes;
	int *conn, numconn;
	

	int vardim[] = {1};
	int centering[] = {0}; //cell based
	const char *varname = {"part"};

	//fill the points
	pts = (float*) malloc(3*msh->nvert*sizeof(float));
	for(ii=0; ii<msh->nvert ; ii++){
		pts[ii*3] = (float)msh->dvertcoord[ii*2];
		pts[ii*3+1] = (float)msh->dvertcoord[ii*2+1];
		pts[ii*3+2] = 0;
	}

	//fill cell types and length of connectivity
	numconn = 0;
	celltypes = (int*) malloc(msh->ncell*sizeof(int));
	for(ii=0; ii<msh->ncell ; ii++){
		if (msh->ncellface[ii] == 3 )
			celltypes[ii] = VISIT_TRIANGLE;
		else if (msh->ncellface[ii] == 4 )
			celltypes[ii] = VISIT_QUAD;
		else
			SETERRQ2(PETSC_COMM_WORLD, 1, "cell %d has %d verts!", ii, msh->ncellface[ii]);
		numconn += msh->ncellface[ii];
	}

	//fill connectivity and vars
	conn = (int*) malloc(numconn*sizeof(int));
	vars = (float*) malloc(msh->ncell*sizeof(float));
	numconn = 0;
	for(ii=0; ii<msh->ncell ; ii++){
	    for(jj=0; jj<msh->ncellface[ii]; jj++){
			conn[numconn+jj] = msh->icelltovert[ii*MAXADJ+jj];
		}
	    numconn += msh->ncellface[ii];

		vars[ii] = (float)part[ii];
	}


	//write the mesh
	write_unstructured_mesh(fname, useBinary, msh->nvert, pts, msh->ncell, celltypes, conn, 1, vardim, centering, &varname, &vars);
	

	free(pts);
	free(celltypes);
	free(conn);
	free(vars);
	return 0;
}
