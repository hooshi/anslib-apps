/*
 * merge.hxx
 *
 * Merge triangles on lines to create quads.
 */


#ifndef MERGE_HXX
#define MERGE_HXX

#include "global.hxx"
#include "lines.hxx"
#include "mesh.hxx"


//functions
// Merges the triangles in a line to create quads
// mesh: the original mesh
// lines: the struct containing the lines
// newmesh: the new mesh (output).
void mergeTriInline(Mesh *mesh, LinesMaster *lines, MeshGr *newmesh);

#endif /* MERGE_HXX */
