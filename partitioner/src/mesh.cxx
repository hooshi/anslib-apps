#include "mesh.hxx"
#include "visit_writer.hxx"
#include <cstdio>
#include <vector>
#include <iostream>
#include <cmath>
#include <cstring>
#include <algorithm>

#include "lines.hxx"
#include "part.hxx"

/****************************************************************************
 *                         Input Mesh Formats
 ****************************************************************************/

void MeshGr::nullify(){
	iNbdryface = ncell = nface = nvert = 0;
	dvertcoord = (double*) NULL;
	ifacetovert = ifacetocell = (int*) NULL;

	/* add for fec */
	nfemord = nnode = 0;
	ncellnode = (int*)NULL;
	icelltonode = (int*) NULL;
	dnodecoord = (double*) NULL;
	

}

void MeshGr::destroy(){
	if(dvertcoord)   delete[] dvertcoord;
	if(ifacetovert)  delete[] ifacetovert;
	if(ifacetocell)  delete[] ifacetocell;

	/* add for fec */
	if(ncellnode) delete[] ncellnode;
	if(icelltonode) delete[] icelltonode;
	if(dnodecoord) delete[] dnodecoord;

	// set all to null again
	nullify();
}

void MeshGr::readDotmesh(const char *fname){
	
	FILE *fin;
	int ii, nread;

	/*
	  open the file
	*/
	fin=fopen(fname, "r");
	assert(fin);
	nread=fscanf(fin, "%d %d %*d %d", &ncell, &nface, &nvert);
	assert(nread==3);

	/*
	  create data
	*/
	ifacetovert = new int[nface*2];
	ifacetocell = new int[nface*2];
	dvertcoord = new double[nvert*2];
	
    /*
	  read vertices
	*/
	for (ii=0 ; ii < nvert ; ii++){
		nread=fscanf(fin,"%lf %lf", &dvertcoord[ii*2], &dvertcoord[ii*2+1]);
		assert(nread==2);
	}

	/*
	  read faces
	*/
    for (ii=0; ii < nface ; ii++){
		nread=fscanf(fin,"%d %d %d %d", &ifacetocell[ii*2], &ifacetocell[ii*2+1],
					 &ifacetovert[ii*2] , &ifacetovert[ii*2+1]);
		assert(nread==4);
	}

 	/*
	  close the file
	*/
	fclose(fin);
}

void MeshGr::readFEC(const char *fname){
	
	FILE *fin;
	int nread, nelem;

	/* open the file */
	fin=fopen(fname, "r"); assert(fin);
	nread=fscanf(fin, "%d %d %d", &nnode, &nelem, &nfemord);assert(nread==3);
	if (ncell != 0) assert(nelem == ncell);
	else ncell = nelem;
						   

	/* create data */
	ncellnode = new int[ncell];
	icelltonode = new int[ncell*MAXNODE];
	dnodecoord = new double[2*nnode];
	//this is needed for valgrind warnings when a map is created
	memset(icelltonode, -1, (size_t)(ncell*MAXNODE*sizeof(int))); 
	
    /*  read node coordinates */
	for (int ii=0 ; ii < nnode ; ii++){
		nread=fscanf(fin,"%lf %lf", &dnodecoord[ii*2], &dnodecoord[ii*2+1]);
		assert(nread==2);
	}

	/*  read member nodes 	*/
    for (int ii=0; ii < ncell ; ii++){
	    nread=fscanf(fin, "%d", ncellnode +ii);assert(nread);

		for (int jj=0; jj<ncellnode[ii]; jj++){
			nread=fscanf(fin, "%d", &icelltonode[ii*MAXNODE + jj]);assert(nread);
		}
			
	}

 	/*  close the file 	*/
	fclose(fin);
}


void MeshGr::print() const{

	printf("MESH_GR, iord: %d", nfemord);
	
	//verts
	printf("VERTS: \n");
	for (int ii=0 ; ii < nvert ; ii++)
		printf("[%d] %lf %lf \n", ii, dvertcoord[ii*2], dvertcoord[ii*2+1]);
	printf("\n");
	
	//faces
	for (int ii=0 ; ii < nface ; ii++){
		printf("[%d] cl:%d cr:%d vl:%d vu:%d \n",  ii,
			   ifacetocell[ii*2], ifacetocell[ii*2+1],
			   ifacetovert[ii*2], ifacetovert[ii*2+1] );
	}

	//nodes
	if ( nnode > 0 )
		for (int ii=0 ; ii < ncell ; ii++){
			printf("[%d](nn:%d) ",  ii, ncellnode[ii]);
			for (int jj=0; jj<ncellnode[ii]; jj++)
				printf("%d ", icelltonode[ii*MAXNODE + jj]);
			printf("\n");
		}

}

/****************************************************************************
 *                                   Mesh
 ****************************************************************************/

void Mesh::nullify(){
	//set all to zero
	dmin = dmax = dave = 0;
	ncell = nvert = nface = icellbeg = 0;

	//set int pointers to zero
	icelltoface = icelltovert =	ifacetovert = icelltocell = (int*)NULL;
	ncellneigh = ioldcell =	ifacetocell = ncellface = (int*)NULL;
	ioldvert = ioldface = (int*)NULL;

	//double pointer
	dvertcoord = dcellcentre = (double *) NULL;

	/* add for fec */
	nfemord = nnode = 0;
	ncellnode = (int *) NULL;
	icelltonode = (int *) NULL;
	ioldnode = (int *) NULL;
	dnodecoord = (double *) NULL;

	/* bpatch */
	adbdryface = (double (*)[NBPATCH] )NULL;
	iIbdryface = NULL;
	iNbdryface = 0;

}

void Mesh::destroy(){
	if(ncellneigh) delete[] ncellneigh ;
	if(ncellface) delete[] ncellface ;
	
	if(ioldcell) delete[] ioldcell;
	if(ioldface) delete[] ioldface ;
	if(ioldvert) delete[] ioldvert ;
	
	if(icelltocell) delete[] icelltocell ;
	if(icelltovert) delete[] icelltovert ;
	if(icelltoface) delete[] icelltoface ;
	if(ifacetovert) delete[] ifacetovert ;
	if(ifacetocell) delete[] ifacetocell ;
	
	if(dvertcoord) delete[] dvertcoord ;
	if(dcellcentre) delete[] dcellcentre ;

	/* add for fec */
	if(ncellnode) delete[] ncellnode;
	if(icelltonode) delete[] icelltonode;
	if(ioldnode ) delete[] ioldnode;
	if(dnodecoord) delete[] dnodecoord;

	/* bpatch */
	if(adbdryface) delete[] adbdryface;
	if(iIbdryface) delete[] iIbdryface;
		
	//set all to null
	nullify();
}


double Mesh::dCCDist(const int cleft, const int cright) const{
	if ( !((cleft >= 0) && (cright >= 0) && (cleft < ncell) && (cright < ncell)) )
	{
		printf("%d %d ,max %d \n", cleft, cright, ncell);
		//assert((cleft >= 0) && (cright >= 0) && (cleft < ncell) && (cright < ncell));
	}
	return sqrt( pow(dcellcentre[cleft*2+dirX] - dcellcentre[cright*2+dirX],2) +
				 pow(dcellcentre[cleft*2+dirY] - dcellcentre[cright*2+dirY],2) );
}

double Mesh::dFaceLength(const int ii, double *adcenter) const{

	double distx, disty;
	double dxc, dyc;
	
	int igv1, igv2;

	distx = disty = dxc = dyc = 0;

	igv1 = ifacetovert[ii*2];
	igv2 = ifacetovert[ii*2 + 1];
	
	distx += dvertcoord[igv1*2 + dirX];
	distx -= dvertcoord[igv2*2 + dirX];
	distx *= distx;

   	dxc += dvertcoord[igv1*2 + dirX];
	dxc += dvertcoord[igv2*2 + dirX];
	dxc /= 2.;

	disty += dvertcoord[igv1*2 + dirY];
	disty -= dvertcoord[igv2*2 + dirY];
	disty *= disty;

	dyc += dvertcoord[igv1*2 + dirY];
	dyc += dvertcoord[igv2*2 + dirY];
	dyc /= 2.;


	if (adcenter){
		adcenter[dirX] = dxc;
		adcenter[dirY] = dyc;
	}
	return sqrt(distx + disty);

}

void Mesh::vCellDistInfo(const int ii, double *dmax, double *dmin, double *dsum) const{
	
	double dsum_, dmin_, dmax_, dist;
	int jj;

	//find the average and maximum
	dsum_ = 0;
	dmax_ = 0;
	dmin_ = 1e12;
	for (jj = 0 ; jj < ncellneigh[ii] ; jj++){
		dist = dCCDist(ii, icelltocell[ii*MAXADJ+jj]);
		dmax_ = fmax(dmax_, dist);
		dmin_ = fmin(dmin_, dist);
		dsum_ += dist;
	}
	for (; jj < ncellface[ii] ; jj++){
			 
		double *adc;
		double adf[2];
		int igf = icelltoface[ii*MAXADJ + jj];

		assert(ifacetocell[2*igf +1] < 0);
		dFaceLength(igf, adf);
		adc = dcellcentre + ii*2;
		dist = 2 * sqrt(pow(adc[0] - adf[0] ,2) + pow(adc[1] - adf[1],2));

		dmax_ = fmax(dmax_, dist);
		dmin_ = fmin(dmin_, dist);
		dsum_ += dist; //experiment on using this
	}

	if (dmax) *dmax = dmax_;
	if (dmin) *dmin = dmin_;
	if (dsum) *dsum = dsum_;
}



void Mesh::vCellRefPoint(const int ii, double* adcoord) const{

	adcoord[dirX] = adcoord[dirY] = 0;
	
	// use the average of verts
	if ( g_refpt == 0){
		for (int jj = 0; jj < ncellface[ii] ; jj++){
		
			int igv = icelltovert[ii*MAXADJ + jj];
			adcoord[dirX] += dvertcoord[igv*2 + dirX];
			adcoord[dirY] += dvertcoord[igv*2 + dirY];
		
		}
		adcoord[dirX] /= ncellface[ii];
		adcoord[dirY] /= ncellface[ii];
	}

	//use the perimeter center 
	else if(g_refpt == 1){

		double adfcent[2], dlen, dper;
		int igf;

		dper = 0;
		for (int jj = 0; jj < ncellface[ii] ; jj++){
			igf = icelltoface[ii*MAXADJ + jj];
			dlen = dFaceLength(igf, adfcent);
			adcoord[dirX] += dlen * adfcent[dirX];
			adcoord[dirY] += dlen * adfcent[dirY];
			dper += dlen;
		}
		adcoord[dirX] /= dper;
		adcoord[dirY] /= dper;
	}

	// no good input for method
	else{
		assert(0);
	}

	//printf("(%lf,%lf)\n", adcoord[dirX], adcoord[dirY]);
	
}


void Mesh::createFromGrummp(MeshGr *gru){

	int cleft, cright;
	int ii, itmp;
	double dist;

	/*
	 * set the numbers
	 */
	ncell = gru->ncell;
	nvert = gru->nvert;
	nface = gru->nface;

	/*
	 * create or copy data
	 */
	
	// will keep
    icelltocell = new int[ncell*MAXADJ]; 
	ncellneigh = new int[ncell];
	ncellface = new int[ncell]; 
	dcellcentre = new double[ncell*2];

	memset(ncellface, 0, (size_t)(ncell*sizeof(int)) );
	memset(ncellneigh, 0, (size_t)(ncell*sizeof(int)) );
	memset(dcellcentre, 0, (size_t)(ncell*sizeof(double)*2 ) );
	memset(icelltocell, -1, (size_t)(ncell*sizeof(int)*MAXADJ) );

	//will destroy before end of this func
	icelltoface = new int[ncell*MAXADJ];
	icelltovert = new int[ncell*MAXADJ]; 
	ifacetovert = gru->ifacetovert; gru->ifacetovert = (int *)NULL;
	ifacetocell = gru->ifacetocell; gru->ifacetocell = (int *)NULL;
	dvertcoord = gru->dvertcoord;   gru->dvertcoord = (double *)NULL;
	
	memset(icelltoface, -1, (size_t)(ncell*sizeof(int)*MAXADJ) );
	memset(icelltovert, -1, (size_t)(ncell*sizeof(int)*MAXADJ) );
	
	/*
	  iterate faces
	*/
    for (ii=0; ii < nface ; ii++){

		//put boundary to right
		if( ifacetocell[ii*2] < 0 ){
		    assert(ifacetocell[ii*2+1] >= 0);

			itmp = ifacetovert[ii*2];
			ifacetovert[ii*2] = ifacetovert[ii*2+1];
			ifacetovert[ii*2+1] = itmp;
			
			itmp = ifacetocell[ii*2];
			ifacetocell[ii*2] = ifacetocell[ii*2+1];
			ifacetocell[ii*2+1] = itmp;			
		}
		cleft = ifacetocell[ii*2];
		cright = ifacetocell[ii*2+1];

		// fill the cell to cell data 
		// if (cright >= 0){
		// 	icelltocell[cleft*MAXADJ + ncellneigh[cleft] ] = cright;
		// 	ncellneigh[cleft]++;
		// 	icelltocell[cright*MAXADJ + ncellneigh[cright] ] = cleft;
		// 	ncellneigh[cright]++;
		// }
		
		// fill the cell to face data 
		icelltoface[cleft*MAXADJ + ncellface[cleft] ] = ii;
		ncellface[cleft]++;
		if (cright >= 0){
			icelltoface[cright*MAXADJ + ncellface[cright] ] = ii;
			ncellface[cright]++;
		}		

	}


	//over the cells
	for (ii = 0 ; ii < ncell ; ii++)
	{
		/*
		 * Cell to cell data
		 * correct cell to face to data
		 * TODO: first shuffle, then add cells !!!!
		 */
		{
			int jj;
			double adsrt[MAXADJ];
			int ilf[] = {0,1,2,3,4}, igf, aigfnew[MAXADJ];
			CmpUniso cmp;
	    
			// the non existing cells should be at the end of the sorted array
			for (int jj = 0 ; jj < MAXADJ ; jj++){
			adsrt[jj] = -1;
			}

			// sort the faces
			for(int jj = 0 ; jj < ncellface[ii] ; jj++){
				igf = icelltoface[ii*MAXADJ + jj];
			
				if( ifacetocell[2*igf + 1] < 0 ) adsrt[jj] = 0; //bdry is always on right
				else adsrt[jj] = 1;                             //non bdry
			}
			CmpUniso::setVals(adsrt, MAXADJ);
			std::sort(ilf, ilf+MAXADJ, cmp);

			//fill the celltocell
			for(jj = 0 ; jj < ncellface[ii] ; jj++){
				assert( ilf[jj] < ncellface[ii] );
				igf = icelltoface[ii*MAXADJ + ilf[jj]];
				
				if( ifacetocell[2*igf + 1] < 0 ) break;

				//find the other cell
				if ( ifacetocell[igf*2] == ii )					
					icelltocell[ii*MAXADJ + ncellneigh[ii] ] = ifacetocell[igf*2+1];
				else if ( ifacetocell[igf*2+1] == ii )
					icelltocell[ii*MAXADJ + ncellneigh[ii] ] = ifacetocell[igf*2];
				else assert(0);

				//replace the new cell
				aigfnew[jj] = igf;
				
				ncellneigh[ii]++;

			}
			for(; jj < ncellface[ii] ; jj++){
				assert( ilf[jj] < ncellface[ii] );	      
				igf = icelltoface[ii*MAXADJ + ilf[jj]];
				icelltocell[ii*MAXADJ + jj ] = ifacetocell[igf*2+1];
				//replace the new cell
				aigfnew[jj] = igf;
				assert(ifacetocell[2*igf + 1] < 0);
			}

			//replace the new cells
			for (int jj = 0 ; jj < ncellface[ii] ; jj++){
				icelltoface[ii*MAXADJ + jj ] = aigfnew[jj]; 
			}
		}


		/*
		 * fill the cell to vert data
		 */	
		// for the first face
		int nreadverts = 0;
		int igf = icelltoface[ii*MAXADJ+0], ilf, igv;
		
	    // if the cell is on the left
		if (ifacetocell[igf*2] == ii ){
			icelltovert[ii*MAXADJ] = ifacetovert[igf*2];
			icelltovert[ii*MAXADJ+1] = ifacetovert[igf*2+1];
		}
		//cell on the right
		else if(ifacetocell[igf*2+1] == ii ){
			icelltovert[ii*MAXADJ] = ifacetovert[igf*2+1];
			icelltovert[ii*MAXADJ+1] = ifacetovert[igf*2];
		}else{
			printf("cell:%d , face: %d , cl: %d, cr:%d ",
					 ii, igf, ifacetocell[igf*2], ifacetocell[igf*2+1]);
			assert(0);
		}
		nreadverts+=2;

		// for to find all the other verts
		for (;nreadverts < ncellface[ii] ; nreadverts++){
			
			//loop over all faces to find the one containing the next vert
			igv = -1;
			for (ilf = 0 ; ilf < ncellface[ii] ; ilf++){
				igf = icelltoface[ii*MAXADJ+ilf];
				
				// if cell was to left and first vert was the previous one read
				if ( (ifacetocell[igf*2] == ii) &&
					 (ifacetovert[igf*2] == icelltovert[ii*MAXADJ+nreadverts-1] ) ){
					igv = ifacetovert[igf*2+1];
					break;
				}
				
				// or cell was to right and second vert was the previous one read
				if  ( (ifacetocell[igf*2+1] == ii) &&
					  (ifacetovert[igf*2+1] == icelltovert[ii*MAXADJ+nreadverts-1]) ){
					igv = ifacetovert[igf*2];
					break;
				}
			}

			//check if a face was found
			if (igv == -1 ) assert(0);

			//set the next vert
			icelltovert[ii*MAXADJ+nreadverts] = igv;
		}
	}

	/*
	  find cell centres
	*/
	for (ii=0 ; ii < ncell ; ii++){
		vCellRefPoint(ii, dcellcentre + ii*2);
	}

	/*
	 * find minimum distance
	 */
	dmin = 1e12;
	dmax = 0;
	dave = 0;
	for (ii = 0 ; ii < nface ; ii++){
		
		cleft  = ifacetocell[ii*2];
		cright  = ifacetocell[ii*2+1];

		if( (cleft >=0) && (cright >= 0) ){
			dist = dCCDist(cleft, cright);
			//printf("%lf\n",qdist);
			dmin = fmin(dmin, dist);
			dmax = fmax(dmax, dist);
			dave += dist;
		}				
	}
	dave /= ncell;

	/*
	  find boundary faces
	*/
	iNbdryface = 0;
	for (int ii = 0 ; ii < nface ; ii++){
	    if (ifacetocell[ii*2 + 1] < 0)	iNbdryface++;
	}

	//printf("INBDRYFACE: %d", iNbdryface);

	iIbdryface = new int[iNbdryface];
	mapGF2GB.clear();
	
	{
		int tmp = 0;
		for (int ii = 0 ; ii < nface ; ii++){
			if (ifacetocell[ii*2 + 1] < 0){
				iIbdryface[tmp] = ii;
				mapGF2GB[ii] = tmp;
				tmp++;
			}
		}

		assert(tmp == iNbdryface);
	}
	

}

void Mesh::addNodeFromGrummp(MeshGr *gru){

	assert(ncell == gru->ncell);
	nnode = gru->nnode;
	nfemord = gru->nfemord; assert((nfemord==3)||(nfemord==4));

	dnodecoord = gru->dnodecoord; gru->dnodecoord = (double*) NULL;
	ncellnode = gru->ncellnode; gru->ncellnode = (int *) NULL;
	icelltonode = gru->icelltonode; gru->icelltonode = (int *) NULL;
}

void Mesh::readBPatch(const char *fname){

	FILE *fl;

	fl = fopen(fname, "r");assert(fl);
		
	// read the bdry face file
	adbdryface = new double[iNbdryface][NBPATCH];
	
	for (int ii = 0 ; ii < iNbdryface ; ii++){
		int tmp;
		fscanf(fl, "%d", &tmp);
		if(tmp != iIbdryface[ii] ){
			printf("%d, %d\n", tmp, iIbdryface[ii]);
			assert(tmp == iIbdryface[ii]);
		}
		
		for (int jj = 0 ; jj < NBPATCH ; jj++){
			tmp = fscanf(fl, "%lf", &adbdryface[ii][jj]);
			assert(tmp);
		}
	}

	fclose(fl);
	
}

void Mesh::print() const{

	int ii, jj;
	
	//verts
	printf("VERT:\n");
	for (ii=0 ; ii < nvert ; ii++) printf("%lf %lf\n",dvertcoord[ii*2],dvertcoord[ii*2+1]);
	printf("\n");

	//nodes
	printf("NODE:\n");
	for (ii=0 ; ii < nnode ; ii++) printf("%lf %lf\n",dnodecoord[ii*2],dnodecoord[ii*2+1]);
	printf("\n");
	
	//cells
 	printf("\nCELLS:\n");
	for (ii=0 ; ii < ncell ; ii++){

		//neighbour cells
		printf("[%d] cent: %lf,%lf nneigh:%d ", ii, dcellcentre[ii*2+dirX], dcellcentre[ii*2+dirY]	   , (ncellneigh)[ii]);
		for (jj = 0; jj < (ncellneigh)[ii] ; jj++)
			printf("%d ", (icelltocell)[ii*MAXADJ + jj]);
		printf("| \t");

		//neighbour faces
		printf("nfaces: %d ", (ncellface)[ii]);
		for (jj = 0; jj < (ncellface)[ii] ; jj++)
			printf("%d ", (icelltoface)[ii*MAXADJ + jj]);
		printf("| \t");

		//vertices
		printf("verts: ");
		for (jj = 0; jj < (ncellface)[ii] ; jj++)
			printf("%d ", (icelltovert)[ii*MAXADJ + jj]);
		printf("| \t");

		//nodes
		printf("nodes: ");
		for (jj = 0; jj < ncellnode[ii] ; jj++)
			printf("%d ", (icelltonode)[ii*MAXNODE + jj]);
		printf("\n");
	}

	//faces
	for (ii=0 ; ii < nface ; ii++){
		printf("[%d] cl:%d cr:%d vl:%d vu:%d \n",  ii,
			   ifacetocell[ii*2], ifacetocell[ii*2+1],
			   ifacetovert[ii*2], ifacetovert[ii*2+1] );
	}

	

}


void Mesh::writeVtk(const char *fname,
					int nvars, int *vardim ,
					int *centering , double **vars){

	int ii,jj;
	
	int useBinary = 0;
	float *pts ;
	int *celltypes;
	int *conn, numconn;

	const char *name1 = "var1";
	const char *name2 = "var2";
	const char *name3 = "var3";
	const char *name4 = "var4";
	char const* varnames[] = {name1, name2, name3, name4};

	//fill the points
	pts = (float*) malloc(3*nvert*sizeof(float));
	for(ii=0; ii<nvert ; ii++){
		pts[ii*3] = (float)dvertcoord[ii*2];
		pts[ii*3+1] = (float)dvertcoord[ii*2+1];
		pts[ii*3+2] = 0;
	}

	//fill cell types and length of connectivity
	numconn = 0;
	celltypes = (int*) malloc(ncell*sizeof(int));
	for(ii=0; ii<ncell ; ii++){
		if (ncellface[ii] == 3 )
			celltypes[ii] = VISIT_TRIANGLE;
		else if (ncellface[ii] == 4 )
			celltypes[ii] = VISIT_QUAD;
		else
		    assert(0);

		numconn += ncellface[ii];
	}

	//fill connectivity and vars
	conn = (int*) malloc(numconn*sizeof(int));
	numconn = 0;
	for(ii=0; ii<ncell ; ii++){
	    for(jj=0; jj<ncellface[ii]; jj++){
			conn[numconn+jj] = icelltovert[ii*MAXADJ+jj];
		}
	    numconn += ncellface[ii];
	}


	//write the mesh
	write_unstructured_mesh(fname, useBinary, nvert, pts, ncell, celltypes, conn, nvars, vardim, centering, varnames, vars);
	

	free(pts);
	free(celltypes);
	free(conn);
}

static int cell2vtk(const int inp)
{
	switch(inp){
	case 3:
		return 5;
		break;
	case 4:
		return 9;
		break;
	default:
		assert(0);
		return 0;
	}
}

void Mesh::writeVtkLinesMaster(const char *fname, LinesMaster &lns)
{

	FILE *fl;
	int ii, jj, icelllength, nlinenonempty;
	CellNode *it;
	LineNode *inf;

	fl = fopen(fname, "w");
	assert(fl);

	// write the header
	fprintf(fl, "# vtk DataFile Version 2.0\n");
	fprintf(fl, "Test for lines\n");
	fprintf(fl, "ASCII\n");
	fprintf(fl, "DATASET UNSTRUCTURED_GRID\n\n");

	// write the points
	fprintf(fl, "POINTS %d double\n", ncell+nvert);
	for(ii=0; ii<nvert ; ii++){
		fprintf(fl, "%-25.20lf ", dvertcoord[ii*2+dirX]);
		fprintf(fl, "%-25.20lf ", dvertcoord[ii*2+dirY]);
		fprintf(fl, "0.00\n");
	}
	for(ii=0; ii<ncell ; ii++){
		fprintf(fl, "%-25.20lf ", dcellcentre[ii*2+dirX]);
		fprintf(fl, "%-25.20lf ", dcellcentre[ii*2+dirY]);
		fprintf(fl, "0.00\n");
	}
	fprintf(fl, "\n");

	//find the total cell size list
	icelllength = 0;
	nlinenonempty = 0;
	for(ii=0; ii<ncell ; ii++){
		icelllength++;
		icelllength += ncellface[ii];
	}
	for(inf=lns.infohead; inf != NULL ; inf=inf->next){
  		if (inf->nmems > 1){
  			nlinenonempty ++;
  			icelllength++;
  			icelllength += inf->nmems;
  		}
  	}

	//write the cell connectivity
	fprintf(fl, "CELLS %d %d\n", ncell+nlinenonempty, icelllength);
	for(ii=0; ii<ncell ; ii++){
		fprintf(fl, "%d ", ncellface[ii] );
		for (jj=0; jj<ncellface[ii] ; jj++)
			fprintf(fl, "%d ", icelltovert[ii*MAXADJ + jj]);
		fprintf(fl, "\n");
	}
	for(inf=lns.infohead; inf != NULL ; inf=inf->next){
		if (inf->nmems > 1){
			fprintf(fl, "%d ", inf->nmems );
			for (it = inf->nodebeg ; it != NULL ; it = it->next)
				fprintf(fl, "%d ", nvert+it->icell);
			fprintf(fl, "\n");
		}		
	}
	fprintf(fl, "\n");
	
	//write cell types
	fprintf(fl, "CELL_TYPES %d \n", ncell+nlinenonempty);
	for(ii=0; ii<ncell ; ii++){
		fprintf(fl, "%d \n", cell2vtk(ncellface[ii]) );
	}
    for(inf=lns.infohead; inf != NULL ; inf=inf->next){
		if (inf->nmems > 1)
			fprintf(fl, "4\n");
	}
	fprintf(fl, "\n");

	//write line numbers
	fprintf(fl, "CELL_DATA %d\n", ncell+nlinenonempty);
	fprintf(fl, "SCALARS line_no int\n");
	fprintf(fl, "LOOKUP_TABLE default\n");
	for(ii=0; ii<ncell ; ii++){
		fprintf(fl, "-%d\n", lns.nodes[ii].father->inum);
	}
    for(inf=lns.infohead; inf != NULL ; inf=inf->next)
		if (inf->nmems > 1)	fprintf(fl, "%d\n", inf->inum);
    fprintf(fl, "\n");

    // write the line colors

	std::vector<int> color; color.resize(ncell);
	vMeshPartitionLinesMaster(this, &lns, 0, &color[0], COLOR_LINES);

	fprintf(fl, "SCALARS line_color int\n");
	fprintf(fl, "LOOKUP_TABLE default\n");

	for(ii=0; ii<ncell ; ii++)
	{
		fprintf(fl, "%d\n", color[lns.nodes[ii].icell]);
	}
    for(inf=lns.infohead; inf != NULL ; inf=inf->next)
		if (inf->nmems > 1)
			fprintf(fl, "%d\n", color[inf->nodebeg->icell]);


	
	fclose(fl);

}

void Mesh::writeMSH(const char *fname)
{
	FILE *fl;

	fl = fopen(fname,"w"); assert(fl);

	/*
	  Write the header
	*/
	fprintf(fl, "%d %d %d %d\n\n", ncell, nface, iNbdryface, nvert);

	/*
	  Write vertex coordinates
	*/
	for (int i = 0 ; i < nvert ; i++)
	{
		fprintf(fl, "%.16lf %.16lf \n", dvertcoord[i*2], dvertcoord[i*2+1]);
	}
	fprintf(fl, "\n");

	/*
	  Write faces adjacent verts and cells
	*/
	for (int i = 0 ; i < nface ; i++)
	{
		fprintf(fl, "%d %d %d %d \n",
				ifacetocell[i*2], ifacetocell[i*2+1],
				ifacetovert[i*2], ifacetovert[i*2+1]);
	}
	fprintf(fl, "\n");


	/*
	  Write boundary face data
	*/
	int ibdry = 0;
	for (int i = 0 ; i < nface ; i++)
		if (ifacetocell[i*2+1] < 0) 
		{
			fprintf(fl, "%d %d %d %d \n",
					i, -ifacetocell[i*2+1],
					ifacetovert[i*2], ifacetovert[i*2+1]);

			ibdry++;
		}
	fprintf(fl, "\n");

	/*
	  Write region #
	*/
	for (int i = 0 ; i < ncell ; i++)
	{
		fprintf(fl, "1\n");

	}
	

	
	fclose(fl);
	
}
