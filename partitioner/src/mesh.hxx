/*
 * mesh.hxx
 *
 * Functions and data structures related to a mesh.
 */


#ifndef MESH_HXX
#define MESH_HXX

#include "global.hxx"
#include <map>

class LinesMaster;

/*****************************************************************************
 *                                Input meshes
 *****************************************************************************/
/* Meshtri
 *
 * Element base mesh. Like Triangle or Gmsh.
 */
struct MeshTri{
	//
};


/* meshgrummp
 *
 * This is a struct that stores the data for
 * a mesh which is read using grummp.
 */
struct MeshGr{
	int nnode;
	int ncell;
	int nface;
	int nvert;
	int nfemord;

	double *dvertcoord;
	double *dnodecoord;
	int *ifacetovert;
	int *ifacetocell;
	int *ncellnode;
	int *icelltonode;

	int iNbdryface;


	MeshGr() {nullify();}
	void nullify();
	void destroy();

	void readDotmesh(const char *fname);
	void readFEC(const char *fname);
	void print() const;
};


/****************************************************************************
 *                                Internal mesh
 ****************************************************************************/

/* mesh
 *
 * Data structure used by my program for a mesh
 */
struct Mesh{
	int nfemord;

	int ncell;
	int nvert;
	int nface;
	int nnode;

	int icellbeg;

	int *ncellface;
	int *ncellneigh;
	int *ncellnode;
	int *ioldcell;
	int *ioldface;
	int *ioldvert;
	int *ioldnode;
	
	int *icelltocell;
	int *icelltovert;
	int *icelltoface;
	int *icelltonode;
	
	int *ifacetovert;
	int *ifacetocell;

	double *dvertcoord;
	double *dnodecoord;
	double *dcellcentre;
	double dmin, dmax, dave;

	// BPatch file
	int *iIbdryface;
	int iNbdryface;
	double (*adbdryface)[NBPATCH];
	std::map<int, int> mapGF2GB;
	

	// constructor and stuff
	Mesh() {nullify();}
	~Mesh() {destroy();}
	
	void nullify();
	void destroy();

	//cell related functions
	double dCCDist(const int cleft, const int cright) const;
    double dFaceLength(const int ii, double* adcenter) const;
	void vCellDistInfo(const int ii,
					   double *dmax, double *dmin, double *dsum) const;
	void vCellRefPoint(const int icell, double* adcoord) const;

	//initialize
	void createFromGrummp(MeshGr *gru);
	void addNodeFromGrummp(MeshGr *gru);
	void readBPatch(const char *fname);

	void print() const;

	void writeVtk(const char *fname){
		writeVtk(fname, 0, NULL, NULL, NULL);
	}
	void writeVtk(const char *fname, int nvars , int *vardim,
				  int *centering, double **vars);
	void writeVtkLinesMaster(const char *fname, LinesMaster& lns);
	
  // write grummp files to be used by solver
  void writeMSH(const char *fname);
  void writeBPATCH(const char *fname);
};




#endif /* MESH_HXX */
