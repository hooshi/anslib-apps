/*
 * merge.cxx
 *
 * Functions to merge triangles located on lines.
 */

#include "merge.hxx"
#include <math.h>
#include <algorithm>
#include <cstdio>
#include <vector>

using namespace std;

void replaceCell(Mesh *mesh, MeshGr *newmesh, const int icell, const int icellnew)
{
	// go through adjacent faces and rename them
	for (int ilf = 0 ; ilf < mesh->ncellface[icell] ; ilf++)
	{
		int igf = mesh->icelltoface[icell*MAXADJ + ilf];

		// check the neighbours of the face
		if (mesh->ifacetocell[igf*2] == icell)
		{
			newmesh->ifacetocell[igf*2] = icellnew;
		}
		else if (mesh->ifacetocell[igf*2+1] == icell)
		{
			newmesh->ifacetocell[igf*2+1] = icellnew;
		}
		else
		{
			assert(0);
		}
	}

}

void mergeTriInline(Mesh *mesh, LinesMaster *lines, MeshGr *newmesh)
{
	vector<int> newbeg;
	vector<LineNode *> linevec;
	int ncellnew;
	
	/*
	 * Create linevec and newbeg
	 */
	{
	    linevec.resize(lines->nlines);
		newbeg.resize(lines->nlines);

		// create a vector containing pointer to lines
		for (LineNode *it=lines->infohead ; it != NULL ; it=it->next)
			linevec.at(it->inum) = it;

		// find the new cell number beginning for each line
		int ncellnewctr = 0;
		for (int ii = 0 ; ii<lines->nlines ; ii++)
		{
			newbeg.at(ii) = ncellnewctr;
			ncellnewctr += (linevec[ii]->nmems / 2) + (linevec[ii]->nmems % 2);
		}

		// new number of cells is now found
		ncellnew = ncellnewctr ;
	}
	// debug
	// printf("Line new # mems: \n");
	// for (int ii = 0 ; ii<lines->nlines ; ii++)
	// {
	// 	printf("%d %d -> %d \n", ii, linevec[ii]->nmems ,newbeg[ii]);
	// }

	/*
	 * Create newmesh and modify its ifacetocell
	 */
	{
		// assign memory and copy old data to be replaced
		newmesh->ifacetocell = new int[mesh->nface * 2];
		memcpy(newmesh->ifacetocell, mesh->ifacetocell, sizeof(int)*2*mesh->nface);

		// for over all lines to merge their members
		int currnewcell;
		for (int iline = 0 ; iline<lines->nlines ; iline++)
		{
			// this variable stores the index of the new cell number
			currnewcell = newbeg.at(iline);

			// for over line members
			for (CellNode *it = linevec[iline]->nodebeg ; it != NULL ; it=it->next)
			{
				// if the cell does not have any next just renumber it
				if (it->next == NULL)
				{
					replaceCell(mesh, newmesh, it->icell, currnewcell);
				}

				// if the cell has a next merge them
				else
				{
					replaceCell(mesh, newmesh, it->icell, currnewcell);
					it = it->next;
					replaceCell(mesh, newmesh, it->icell, currnewcell);					
				}

				// increment the iterator and the new number
				currnewcell++;
			}
		}

		// make sure the numbering is consistent
		assert(currnewcell == ncellnew);

	}
	//debug
	// printf("New Adj: \n");
	// for (int ii = 0 ; ii<mesh->nface ; ii++)
	// {
	// 	printf("%d: %d %d -> %d %d \n", ii,
	// 		   mesh->ifacetocell[ii*2], mesh->ifacetocell[ii*2+1],
	// 		   newmesh->ifacetocell[ii*2], newmesh->ifacetocell[ii*2+1]);
	// }

	/*
	 * delete the unused faces 
	 */
	{
		// back up 
		int *ifacetocellold = newmesh->ifacetocell;

		// count the new faces
		newmesh->nface = 0;
		for (int iof = 0 ; iof < mesh->nface ; iof++)
		{
			if (newmesh->ifacetocell[iof*2] != newmesh->ifacetocell[iof*2+1])
				newmesh->nface++;
		}
		printf("%d %d \n", mesh->nface, newmesh->nface);

		// create data for new faces
		newmesh->ifacetocell = new int [newmesh->nface*2];
		newmesh->ifacetovert = new int [newmesh->nface*2];
		

		// fill the data for new faces
		int inf = 0;
		for (int iof = 0 ; iof < mesh->nface ; iof++)
		{
			if (ifacetocellold[iof*2] != ifacetocellold[iof*2+1])
			{
				// copy face to cell data
				newmesh->ifacetocell[inf*2]= ifacetocellold[iof*2]; 
				newmesh->ifacetocell[inf*2+1] = ifacetocellold[iof*2+1];
				
				// copy face to vert data
				newmesh->ifacetovert[inf*2] = mesh->ifacetovert[iof*2];
				newmesh->ifacetovert[inf*2+1] = mesh->ifacetovert[iof*2+1];
				
				// increment new face counter
				inf++;
			}
		}

		// delete the interim face to cell (which contained the faces to be omitted)
		delete[] ifacetocellold;

	}
	
	//copy remaining data
	newmesh->nvert = mesh->nvert;
	newmesh->ncell = ncellnew;
	newmesh->iNbdryface = mesh->iNbdryface;
	newmesh->dvertcoord = new double[newmesh->nvert*2];
	memcpy(newmesh->dvertcoord, mesh->dvertcoord, 2*newmesh->nvert*sizeof(double));
}
