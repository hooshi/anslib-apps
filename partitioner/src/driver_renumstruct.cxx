#include "mesh.hxx"
#include "lines.hxx"

#include <cmath>
#include <cstdio>
#include <cstdarg>
#include <string>
#include <sstream>

using std::string;
using std::stringstream;

const char help[] = "YELLOW!";

// This function is the actuall map between the
// old and the new numbering.
int new_number(const int M, const int N, const int P, const int k)
{

	int knew;
	
	// Find the row and column
	int i, j;
	i = k / N;
	j = k % N;

	// Find the new number
	if (j < P) knew = i*P+j;
	else knew = j*M+i;

	return knew;
}

// method of renumbering
enum RenumMethod
{
	STRUCTURED = 1,
	FROM_FILE = 2
};

// Renumbers a structured mesh
// Creates lines perpendicular to surface for the first P cells
// Other lines will be in the direction of the flow
// Assumes all the cells are numbered in the y direction
int main(int argc, char *argv[]){

	PetscErrorCode ierr;
	
	// Grummp mesh to be read and renumbered
	MeshGr gr;
	// Mesh to be written
	Mesh msh;

	// Arrays holding the renumbers
	int *aold, *anew;
	
	// Method to repartition
	int met;

	// initialize
	ierr=PetscInitialize(&argc, &argv, NULL, help);CHKERRQ(ierr);
	ierr=g_vInitialize();CHKERRQ(ierr);

	
	// read the method
	{
		PetscBool flg;
		ierr=PetscOptionsGetInt("", "-method", &met, &flg); assert(flg==PETSC_TRUE);
	}

	// read the mesh
	{
		string name = g_fname;
		name += ".mesh";
		gr.readDotmesh(name.c_str());	    
	}

	// create the renumbering arrays
	aold = new int[gr.ncell];
	anew = new int[gr.ncell];

	/*
	  create the renumbering details
	*/
	if (met == (RenumMethod)STRUCTURED )
	{
		// Structured mesh details
		int M, // # cells in x direction
			N, // # cells in y direction
			P; // New length of the lines

		// set M, N and P
		{
			PetscBool flg;
			ierr=PetscOptionsGetInt("", "-M", &M, &flg); assert(flg==PETSC_TRUE);
			ierr=PetscOptionsGetInt("", "-N", &N, &flg); assert(flg==PETSC_TRUE);
			ierr=PetscOptionsGetInt("", "-P", &P, &flg); assert(flg==PETSC_TRUE);	
		}

		for (int ico = 0 ; ico < gr.ncell ; ico++)
		{
			int icn = new_number(M, N, P, ico); 
			anew[ico] = icn;
			aold[icn] = ico;
		}
		
	}
	else if ( (RenumMethod)FROM_FILE )
	{
		LinesMaster lm;

		// read the lines from a file
		PetscBool flg;
		char fname[200];
		ierr=PetscOptionsGetString(NULL, "-l", fname, 200, &flg); assert(flg==PETSC_TRUE);
		
		string sname(fname);
		sname +=  ".l";
		ierr=read_lines_from_file(&lm, gr.ncell, sname.c_str());

		// start reading cells
		int icn=0, ico;

		//printLinesMaster(lm);

		for (LineNode *line = lm.infohead ; line != NULL ; line = line->next)
		{
			for (CellNode *cell = line->nodebeg ; cell != NULL ; cell = cell->next )
			{
				ico = cell->icell;
				anew[ico] = icn;
				aold[icn] = ico;
				icn++;
			}
		}
		
		assert(icn == gr.ncell);
	}
	else
	{
		SETERRQ(PETSC_COMM_WORLD, 1, "Method not supported");
	}
		

	// for over ifacetocell and renumber all
	for (int iface = 0 ; iface < gr.nface ; iface++)
	{
		if (gr.ifacetocell[iface*2] >= 0) gr.ifacetocell[iface*2] = anew[ gr.ifacetocell[iface*2] ];
		if (gr.ifacetocell[iface*2+1] >= 0) gr.ifacetocell[iface*2+1] = anew[ gr.ifacetocell[iface*2+1] ];
	}

	// create the mesh and write it
	msh.createFromGrummp(&gr);
	{
		PetscBool flg;
		char fname[200];
		char fnamevtk[200];
		ierr=PetscOptionsGetString(NULL, "-o", fname, 200, &flg); assert(flg==PETSC_TRUE);

		sprintf(fnamevtk, "%s.mesh", fname);
		msh.writeMSH(fnamevtk);
		
		sprintf(fnamevtk, "%s.rmesh.vtk", fname);
		msh.writeVtk(fnamevtk);
	}
	
	//  destroy and finalize
	delete[] anew;
	delete[] aold;
	msh.destroy();
	gr.destroy();
	ierr=PetscFinalize();

	return 0;
}

