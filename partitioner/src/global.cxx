#include "global.hxx"

#include <cstdio>
#include <cstring>

/****************************************************************************
 *                               Variables
 ****************************************************************************/
int g_bverbos;
int g_refpt;
double g_alpha, g_beta;
int g_npart;
int g_wgt;
int g_fec;
char g_fname[300];
char g_fecname[300];
int g_bpatch;
PartMethod g_partmethod;
char g_flw_fname[300];

char g_out_prefix[300];
char g_part_fname[300];
char *g_fname_fonly;

/****************************************************************************
 *                               Functions
 ****************************************************************************/
PetscErrorCode g_vInitialize(){
	
    PetscBool flg;
    PetscErrorCode ierr;

    //Check for petsc to be initialized
    ierr=PetscInitialized(&flg);CHKERRQ(ierr);
    if(!flg){
        fprintf(stderr, "PETSC MUST BE INITIALIZED\n");
        assert(0);
    }
	
    //level of verbosity
    ierr=PetscOptionsHasName(NULL, NULL,"-v", &flg);CHKERRQ(ierr);
    g_bverbos = (flg ? 1 : 0);
    //method of finding reference point
    ierr=PetscOptionsGetInt(NULL, NULL, "-ref", &g_refpt, &flg);CHKERRQ(ierr);
    if (!flg) g_refpt = 0;
    //line finding parameters
    ierr=PetscOptionsGetScalar(NULL, NULL, "-alpha", &g_alpha, &flg);CHKERRQ(ierr);
    if (!flg) g_alpha = 1;
    ierr=PetscOptionsGetScalar(NULL, NULL, "-beta", &g_beta, &flg);CHKERRQ(ierr);
    if (!flg) g_beta = 4;
    //number of partitions
    ierr=PetscOptionsGetInt(NULL, NULL, "-npart", &g_npart, &flg);CHKERRQ(ierr);
    if (!flg) g_npart = 0;
    //weigt assigning method
    ierr=PetscOptionsGetInt(NULL, NULL, "-wgt", &g_wgt, &flg);CHKERRQ(ierr);
    if (!flg) g_wgt = 0;
    if (g_wgt == -1) g_partmethod = PART_LINES;
    else g_partmethod = PART_CELLS;

    // should we read fec
    ierr=PetscOptionsGetString(NULL,NULL,"-fec",g_fecname,300,&flg);CHKERRQ(ierr);  
    g_fec = (flg ? 1 : 0);
    //should we read bpatch
    ierr=PetscOptionsHasName(NULL, NULL,"-bpatch", &flg);CHKERRQ(ierr);
    g_bpatch = (flg ? 1 : 0);
	
    // file name
    ierr=PetscOptionsGetString(NULL, NULL,"-f",g_fname,300,&flg);CHKERRQ(ierr);
    if(!flg) SETERRQ(PETSC_COMM_SELF,1,"File name not given!!");
    g_fname_fonly =  strrchr ( g_fname, '/' );
    if(g_fname_fonly) g_fname_fonly++;
    else g_fname_fonly = g_fname;

    // Output file name prefix
    ierr=PetscOptionsGetString(NULL, NULL,"-oprefix",g_out_prefix,300,&flg);CHKERRQ(ierr);
    if(!flg) g_out_prefix[0] = '\0' ;

    // parition file name
    ierr=PetscOptionsGetString(NULL, NULL,"-ipart",g_part_fname,300,&flg);CHKERRQ(ierr);
    if(!flg) g_part_fname[0] = '\0' ;

    // face line weight file name
    PetscOptionsGetString(NULL, PETSC_NULL, "-flw", g_flw_fname, 200, &flg);CHKERRQ(ierr);
	


    //print stuff
    fprintf(stderr, "Globals Initialized\n");
    fprintf(stderr, "Level of verbosity: %d\n", g_bverbos);
    fprintf(stderr, "Reference point: %d\n", g_refpt);
    fprintf(stderr, "alpha, beta: %lf, %lf\n", g_alpha, g_beta);
    fprintf(stderr, "npart: %d\n", g_npart);
    fprintf(stderr, "wgt: %d\n", g_wgt);
    fprintf(stderr, "read fec: %d\n", g_fec);
    fprintf(stderr, "file name: %s\n", g_fname);


    return 0;
}
