#include "mesh.hxx"
#include "lines.hxx"
#include "part.hxx"

#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdarg>
#include <vector>

const char help[] = "YELLOW!";

int main(int argc, char *argv[]){

    PetscErrorCode ierr;
    MeshGr gr;
    Mesh msh;

    //writing vtk files
    int vardim[] = {1,1,1,1,1};
    int centering[] = {0,0,0,0,0};

    /*
      initialize
    */
    ierr=PetscInitialize(&argc, &argv, NULL, help);CHKERRQ(ierr);
    ierr=g_vInitialize();CHKERRQ(ierr);
	
    /*
      read the mesh
    */
    {
        char fname[200];

        if(g_fec) sprintf(fname, "%s.mod.mesh", g_fname);
        else sprintf(fname, "%s.mesh", g_fname);
        gr.readDotmesh(fname);
        msh.createFromGrummp(&gr);
        gr.destroy();
	    
    }
    //msh.print();
	
    /*
      partition
    */
    if(g_npart > 0)
    {
        Mesh *submesh = new Mesh[g_npart];
        idx_t *aicelltopart = new idx_t[msh.ncell];
        std::map<int,int> ao;

        // Read the aicelltopart
        FILE *pinfile = fopen(g_part_fname, "r");
        if(!pinfile) SETERRQ1(PETSC_COMM_SELF, 1, "Could not find \'%s\'", g_part_fname);
        for(int i = 0 ; i < msh.ncell ; i++)
        {
            fscanf(pinfile, "%d", aicelltopart+i);
        }
        fclose(pinfile);
            
        // extract submeshs
        vExtractPartitions(&msh, g_npart, aicelltopart, submesh);
	
        // create the ao and move to the new numbering
        vCreateAO(msh.ncell, g_npart, submesh, ao);

        /* delete these data */
        {
            delete[] msh.dcellcentre; msh.dcellcentre = (double *) NULL;
            delete[] msh.icelltovert; msh.icelltovert = (int *) NULL;
        }
	
        /* read the finite element if needed */
        if (g_fec){
            char fname[300];
            sprintf(fname, "%s.fec", g_fecname);
            gr.readFEC(fname);		
            msh.addNodeFromGrummp(&gr);		
            gr.destroy();
        }

        /* read bpatch if needed */
        if (g_bpatch){
            char fname[300];
            sprintf(fname, "%s.bpatch", g_fname);
            msh.readBPatch(fname);
        }
	
        /* migrate a mesh and then write it  */
        for (int ii= 0; ii < g_npart ; ii++)
        {
            char strfname[200];

            /* find the migrated mesh and write it */
            ierr=vMigrateMesh(&msh, ao, submesh+ii);CHKERRQ(ierr);
            if (g_fec) sprintf(strfname, "%s%s_%d.mod.mesh", g_out_prefix, g_fname_fonly, ii);
            else sprintf(strfname, "%s%s_%d.mesh", g_out_prefix, g_fname_fonly, ii);
            vWriteMigrated_DotMesh(&msh, submesh+ii, strfname);

            sprintf(strfname, "%s%s_%d.map", g_out_prefix, g_fname_fonly, ii);
            vWriteMigrated_Map(&msh, submesh+ii, strfname);
            

            /* write the bpatch */
            if(g_bpatch) {
                sprintf(strfname, "%s%s_%d.bpatch", g_out_prefix, g_fname_fonly, ii);
                vWriteMigrated_BPATCH(&msh, submesh+ii, strfname);
            }

            /* write the map */
            
		

            delete[] submesh[ii].ifacetovert; submesh[ii].ifacetovert = (int *) NULL;
            delete[] submesh[ii].ioldvert; submesh[ii].ioldvert = (int *) NULL;
            delete[] submesh[ii].ioldface; submesh[ii].ioldface = (int *) NULL;
            delete[] submesh[ii].ifacetocell; submesh[ii].ifacetocell = (int *) NULL;

            /* find the migrated nodes and write it*/
            if(g_fec) {
                ierr=vMigrateFECData(&msh, submesh+ii);CHKERRQ(ierr);
                sprintf(strfname, "%s%s_%d.fec", g_out_prefix, g_fname_fonly, ii);
                vWriteMigrated_FEC(&msh, submesh+ii, strfname);
            }
		
            /* destroy the mesh */
            submesh[ii].destroy();
        }

        // delete the partitioning stuff
        {
            delete[] aicelltopart;
            delete[] submesh;
        }
    } // end of partitioning 
	
    /*
      destroy the mesh
    */
    msh.destroy();
    ierr=PetscFinalize();

    return 0;
}
