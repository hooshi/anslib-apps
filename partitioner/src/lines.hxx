/*
 * lines.hxx
 *
 * Functions and data structures related to the lines.
 */

#ifndef LINES_HXX
#define LINES_HXX

#include "global.hxx"

class Mesh;
/****************************************************************************
 *                                Comparing
 ****************************************************************************/
class CmpUniso{
private:
	static double const * wgts;
	static int ncells;	

public:
	static void setVals(double const *w_, const int n_){
		ncells=n_;
		wgts=w_;
	}
	bool operator() (int ii, int jj) const {
		assert ( (ii < ncells) && (jj < ncells) );
		return wgts[ii] > wgts[jj];
	}
};


/****************************************************************************
 *                               CellNode
 ****************************************************************************/
struct LineNode;

struct CellNode{

	int qline;
	CellNode *next, *prev;
	int icell;
	LineNode *father;
	
    CellNode():
		qline(0),
		next( (CellNode*)NULL ),		
		prev( (CellNode*)NULL ),
		icell(-1),
		father(NULL)
		{};		
};

/****************************************************************************
 *                              LineNode
 ****************************************************************************/
struct LineNode{
	int inum;
	int nmems;
	CellNode *nodebeg;
	CellNode *nodeker;
	CellNode *nodeend;

	LineNode *next;
	LineNode *prev;

	LineNode()
		{
			next = NULL;
			prev = NULL;
			nodeker = NULL;
			nodebeg = NULL;
			nodeend = NULL;
			nmems = 0;
			inum = -1;
		}

	LineNode(CellNode *ker, const int inum_):
		inum(inum_),
		nmems(1),
		nodebeg(ker),
		nodeker(ker),
		nodeend(ker),
		next(NULL),
		prev(NULL)
		{}

	// revert the line
	PetscErrorCode revert();

	// print the line
	PetscErrorCode print();
	

};

template<typename T>
void vDestroyLinkedList(T *head){
	if (head->next) vDestroyLinkedList(head->next);
	if (head) delete head;
}

/****************************************************************************
 *                                  LinesMaster
 ****************************************************************************/
struct LinesMaster
{
	
	int nlines;
	LineNode *infohead;
	CellNode *nodes;
	Mesh *mesh;

	LinesMaster():
		mesh(NULL)
		{
		nullify();
		}
	
	void nullify()
		{
		infohead = NULL;
		nodes = NULL;
		}
	
	void destroy()
		{
		vDestroyLinkedList(infohead);
		if(nodes) delete[] nodes;
		}

	// merge two lines
	// merges the end of line_keep to beginning of line_delete
	// deletes line_delete completely
	PetscErrorCode merge_two_lines(LineNode *line_keep, LineNode *line_delete);


	// the old line creation code
	// does not fill all the LineNode members
	// such as nodeend, prev
	void create(Mesh& msh);
};


/****************************************************************************
 *                              weight for lines
 ****************************************************************************/

// assign weights according to distance
PetscErrorCode face_weight_distance(Mesh *mesh, double global_face_wgts[]);

// read weights from a file
PetscErrorCode face_weight_file(Mesh *mesh, double global_face_wgts[], const char file_name[]);


/****************************************************************************
 *                           Create the lines - initial phase
 ****************************************************************************/
enum Direction{BACKWARD, FORWARD};

// create the lines ( The new fancier code)
PetscErrorCode create_all_lines(LinesMaster *list_lines, Mesh *mesh, double global_face_wgts[]);

// create a line in either forward or backward direction
// input :
//   mesh the mesh obviously!
//   cell_nodes array containing information about cells
//   face_wgts array containing weights of the faces
//   line pointer to the LineNode containing information about the current line
//   direction direction towards which the line is created
PetscErrorCode create_one_line(const Mesh* mesh,
							   CellNode cell_nodes[],
							   const double face_wgts[],
							   LineNode* line,
							   const Direction direction);


/****************************************************************************
 *                          Merge the lines - second phase
 ****************************************************************************/

// merge all lines
// also renumbers them
PetscErrorCode merge_all_lines
(LinesMaster *lm, Mesh *mesh, double global_face_wgts[]);


// find the faces of a cell that are either boundary or endlines of a
// different line.
//
// n_target_local_faces: number of those faces
// target_local_faces: the local number of those faces
PetscErrorCode find_boundary_or_endline_faces
(LinesMaster *lm, Mesh *mesh, double global_face_wgts[],
 const int cell, 
 int &n_target_local_faces, int target_local_faces[]);

// check if a node can me merged to neighbours and then merge
//
// Note: this function reverts the neighbour line but the user is
//       responsible for reverting the line that the cell is a member
//       of (asserts that cell to be nodeend of its line)
//
// cell: cell to be checked
// merge_needed: returns true if merge was successful
PetscErrorCode merge_end_cell
(LinesMaster *lm, Mesh *mesh, double global_face_wgts[],
 const int cell,
 bool &merge_needed);



/****************************************************************************
 *                               IO
 ****************************************************************************/

// print the lines
void printLinesMaster(LinesMaster &lines);

// write the face weights as vtk
PetscErrorCode face_weight_write_vtk(Mesh *mesh, double global_face_wgts[], const char file_name[]);

// write the lines in line format
PetscErrorCode read_lines_from_file(LinesMaster *lm,  const int ncell, const char file_name[]);

// read the lines from the dat format
PetscErrorCode write_lines_to_file(LinesMaster *lm, Mesh *ms, const char file_name[]);


#endif /* LINES_HXX */
