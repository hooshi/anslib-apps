/*
 * part.hxx
 *
 * Functions and data structures related to partitioning.
 */

#ifndef PART_HXX
#define PART_HXX

#include <metis.h>
#include <petscao.h>
#include <petscviewer.h>
#include <map>

#include "global.hxx"
#include "mesh.hxx"
#include "lines.hxx"
#include <algorithm>

// a map for migrating verts and faces
// key: the global value
// mapped: the local value
// usually the local to global is done using iold[face,vert]
// but global to local is done using this type
typedef  std::map<int,int> Migrator;

/****************************************************************************
 *                             Metis Stuff
 ****************************************************************************/

int iEdgeWeight(Mesh *msh, const int ic1, const int ic2);

void vCell2MetisEdge(idx_t xadj[], idx_t adjncy[], const int igc1,
					 const int igc2, int &ilc1, int &ilc2);

// Convert a mesh to metis graph
// The cell numbering in the mesh should be [global, partitioned]
// and iglobbeg must be set accordingly.
void vMesh2MetisGraph(Mesh *msh, idx_t xadj[], idx_t adjncy[], idx_t adjwgt[]);

// Prints a metis graph on fout
void vPrintMetisGraph(const int nnode,idx_t xadj[], idx_t adjncy[], idx_t adjwgt[], FILE *fout);


/****************************************************************************
 *                             Partition Stuff
 ****************************************************************************/
// Partition the mesh
PetscErrorCode vMeshPartitionCells
(Mesh *msh, LinesMaster *lns,
const int npart, idx_t* aicelltopart);

PetscErrorCode vMeshPartitionLinesMaster
(Mesh *msh, LinesMaster *lns,
const int npart, idx_t* aicelltopart,
const PartMethod met = PART_LINES);

inline PetscErrorCode vMeshPartition(Mesh *msh, LinesMaster *lns, const int npart,	idx_t* aicelltopart)
{
	PetscErrorCode ierr;
	
	// if only on partition, just fill aicelltopart with ones
	if (npart == 1)
	{
		std::fill(aicelltopart, aicelltopart+msh->ncell, 0);
	}
	else
	{
		switch(g_partmethod)
		{
		case PART_LINES:
			ierr=vMeshPartitionLinesMaster(msh, lns, npart, aicelltopart);CHKERRQ(ierr);
			break;
		case PART_CELLS:
			ierr=vMeshPartitionCells(msh, lns, npart, aicelltopart);CHKERRQ(ierr);
			break;
		default:
			assert(0);
		}
	}

	return 0;
}

void vExtractPartitions(Mesh *msh, const int npart, idx_t aicelltopart[], Mesh submesh[]);
void vReorderSubmesh(Mesh *msh, const int npart, idx_t* aicelltopart, LinesMaster *lns, Mesh *submesh);

// myapp -> old numbering
// petsc -> new numbering
void vCreateAO(const int ntotcell ,const int npart, Mesh *submesh, std::map<int,int>& ao);

// After call these will have values
PetscErrorCode vMigrateMesh(Mesh *msh, std::map<int,int>& ao, Mesh *submesh);
PetscErrorCode vMigrateFECData(Mesh *msh, Mesh *submesh);

// you can clear the migrator after you are done with it
PetscErrorCode vLocalizeIndices(const int nonunisize, const int* nonuniidx, int &unisize, int*& unidx, Migrator& mig);


/****************************************************************************
 *                    Writing the partitioned mesh
 ****************************************************************************/

void vWriteMigrated_DotMesh(Mesh *msh, Mesh *submesh, const char fname[]);
void vWriteMigrated_FEC(Mesh *msh, Mesh *submesh, const char fname[]);
void vWriteMigrated_BPATCH(Mesh *msh, Mesh *submesh, const char fname[]);
void vWriteMigrated_Vtk();
void vWriteMigrated_Map(Mesh *msh, Mesh *submesh, const char fname[]);

#endif /*PART_HXX*/
