#include "mesh.hxx"
#include "lines.hxx"
#include "part.hxx"
#include "merge.hxx"

#include <cmath>
#include <cstdio>
#include <cstdarg>

const char help[] = "YELLOW!";

int main(int argc, char *argv[]){

	PetscErrorCode ierr;
	MeshGr gr, nmsh;
	Mesh msh;
	LinesMaster lines;


	/*
	  initialize
	*/
	ierr=PetscInitialize(&argc, &argv, NULL, help);CHKERRQ(ierr);
	ierr=g_vInitialize();CHKERRQ(ierr);
	
	/*
	  read the mesh
	*/
	{
		char fname[200];

		if(g_fec) sprintf(fname, "%s.mod.mesh", g_fname);
                else sprintf(fname, "%s.mesh", g_fname);
		gr.readDotmesh(fname);
		msh.createFromGrummp(&gr);
		gr.destroy();
	    
	}
	//msh.print();

	
	/*
	  create the lines
	*/
	lines.create(msh);
	VERB printLinesMaster(lines);
	msh.writeVtkLinesMaster("lines.vtk", lines);
	
	/* 
           read bpatch if needed 
        */
	if (g_bpatch){
		char fname[300];
		sprintf(fname, "%s.bpatch", g_fname);
		msh.readBPatch(fname);
	}

	/*
	  merge
	 */
	mergeTriInline(&msh, &lines, &nmsh);
	msh.destroy();
	msh.createFromGrummp(&nmsh);
	msh.writeVtk("merged.vtk");
	msh.writeMSH("merged.mesh");
	
	/*
	  destroy the mesh
	*/
	msh.destroy();
	nmsh.destroy();
	lines.destroy();
	ierr=PetscFinalize();

	return 0;
}
