// The libMesh Finite Element Library.
// Copyright (C) 2002-2016 Benjamin S. Kirk, John W. Peterson, Roy H. Stogner

// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA



#ifndef UGRID_IO_HXX
#define UGRID_IO_HXX

// Local includes
#include "libmesh/libmesh_common.h"
#include "libmesh/mesh_input.h"
#include "libmesh/mesh_output.h"

// C++ includes
#include <cstddef>

namespace libMesh
{

// Forward declarations
class MeshBase;



/**
 * Let's try to read the UGRID format similar to GMSH
 */
class UGRID_IO : public MeshInput<MeshBase>,
               public MeshOutput<MeshBase>
{
public:

  /**
   * Constructor.  Takes a non-const Mesh reference which it
   * will fill up with elements via the read() command.
   */
  explicit
  UGRID_IO (MeshBase & mesh);

  /**
   * Constructor.  Takes a reference to a constant mesh object.
   * This constructor will only allow us to write the mesh.
   */
  explicit
  UGRID_IO (const MeshBase & mesh);

  /**
   * Reads in a mesh in the Gmsh *.msh format from the ASCII file
   * given by name.
   *
   * The user is responsible for calling Mesh::prepare_for_use()
   * after reading the mesh and before using it.
   */
  virtual void read (const std::string & name) libmesh_override;

  /**
   * This method implements writing a mesh to a specified file
   * in the Gmsh *.msh format.
   */
  virtual void write (const std::string & name) libmesh_override;

  /**
   * Bring in base class functionality for name resolution and to
   * avoid warnings about hidden overloaded virtual functions.
   */
  using MeshOutput<MeshBase>::write_nodal_data;

  /**
   * This method implements writing a mesh with nodal data to a
   * specified file where the nodal data and variable names are provided.
   */
  virtual void write_nodal_data (const std::string &,
                                 const std::vector<Number> &,
                                 const std::vector<std::string> &) libmesh_override;

  int & binary ();

private:
  /**
   * Implementation of the read() function.  This function
   * is called by the public interface function and implements
   * reading the file.
   */
  void read_mesh (std::istream & in);



  /**
   * Flag to write binary data.
   * 0 -> ascii
   * 1 -> lower endian
   * 2 -> big endian
   */
  int _binary;

  static void add_element_to_mesh(MeshBase &mesh, std::istream & in, ElemType type, int id);
};


} // namespace libMesh

#endif // UGRID_IO_HXX
