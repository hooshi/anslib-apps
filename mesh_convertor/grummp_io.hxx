#include "semantics.hxx"

#include "libmesh/enum_elem_type.h"

// Bring in everything from the libMesh namespace

struct Face
{
    uint id;
    libMesh::ElemType type;
    int cv1;
    int cv2;
    std::vector<uint> nodes;

    Face(const uint _id, const libMesh::ElemType type_in, const int _cv1, const int _cv2, std::vector<uint>& _nodes);
    uint bdry_id() const {if(cv2 < 0) return uint(-cv2); else libmesh_error(); }
};

class GRUMMP_IO
{
private:
    libMesh::ReplicatedMesh &_mesh;
    typedef std::vector<Face*>::iterator face_iterator;
    uint _precision;
    bool _obeys_right_hand_rule;

    std::vector<Face*> _faces;
    std::vector<Face*> _faces_bdry;

public:
    GRUMMP_IO(libMesh::ReplicatedMesh &mesh, const int precision = 16, const bool obeys_right_hand_rule = true);
    ~GRUMMP_IO();

    static double random(const uint seed);
    void perturb_pyramids(const double delta);
    
#ifdef WITH_PETSC
    void reorder_petsc(std::string type);
#endif /* WITH_PETSC */

    void create_face_info();
    void write_mesh(const std::string& file_name);
    libMesh::ReplicatedMesh* surface_wall_mesh(std::string file_name, const std::set<uint> &walltags);
};
