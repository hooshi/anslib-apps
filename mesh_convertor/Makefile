######################################################################
#
# Template libMesh application Makefile
export LIBMESH_DIR ?= /home/hooshi/code/libmesh/parallel-everything


# include the library options determined by configure
include $(LIBMESH_DIR)/Make.common

target1     := gmsh_to_mesh.exe.$(METHOD)
target2     := ugrid_to_xxx.exe.$(METHOD)
target3     := distance_from_wall.exe.$(METHOD)


###############################################################################
# File management.  This is where the source, header, and object files are
# defined

SRCFILES_ALL    := $(wildcard *.C)
SRCFILES_EXEC   := $(wildcard *.exe.C)
SRCFILES        := $(filter-out $(SRCFILES_EXEC), $(SRCFILES_ALL))
OBJFILES_EXEC   := $(patsubst %.C, %.$(obj-suffix), $(SRCFILES_EXEC))
OBJFILES        := $(patsubst %.C, %.$(obj-suffix), $(SRCFILES))
INCFILES        := $(wildcard *.hxx)
EXEFILES        := $(patsubst %.exe.C, %.exe.$(METHOD), $(SRCFILES_EXEC))

###############################################################################



.PHONY: dust clean distclean

###############################################################################
# Target:
#
all: $(target1)

t1: $(target1)
t2: $(target2)
t3: $(target3)

%.exe.$(METHOD): $(OBJFILES) %.exe.$(obj-suffix)
	@echo "Linking "$@"..."
	@$(libmesh_LIBTOOL) --tag=CXX $(LIBTOOLFLAGS) --mode=link \
	  $(libmesh_CXX) $(libmesh_CXXFLAGS) $^ -o $@ $(libmesh_LIBS) $(libmesh_LDFLAGS) $(EXTERNAL_FLAGS)
	rm -rf $^


# Useful rules.
dust:
	@echo "Deleting old output and runtime files"
	@rm -f out*.m job_output.txt output.txt* *.gmv.* *.plt.* out*.xdr* out*.xda* PI* complete 

clean: dust
	@rm -f $(OBJFILES) *.$(obj-suffix) *.lo

clobber: clean 
	@rm -f $(EXEFILES)

distclean: clean
	@rm -rf *.o .libs .depend

echo:
	@echo srcfiles_exe = $(SRCFILES_EXEC)		
	@echo srcfiles = $(SRCFILES)
	@echo objects_exe = $(OBJFILES_EXEC)
	@echo objects = $(OBJFILES)
	@echo target_candidates = $(EXEFILES)
	@echo target1 = $(target1)
	@echo libmeshrun = ${LIBMESH_RUN}

# include the dependency list
-include .depend

#
# Dependencies
#
.depend: $(srcfiles) $(LIBMESH_DIR)/include/libmesh/*.h
	@$(perl) $(LIBMESH_DIR)/contrib/bin/make_dependencies.pl -I. $(foreach i, $(LIBMESH_DIR)/include $(wildcard $(LIBMESH_DIR)/include/*), -I$(i)) "-S\$$(obj-suffix)" $(srcfiles) > .depend

###############################################################################
