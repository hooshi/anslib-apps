#include "grummp_io.hxx"
#include "libmesh/mesh_modification.h"

#ifdef WITH_PETSC
#include "petscmat.h"
#endif /* WITH_PETSC */

// Bring in everything from the libMesh namespace
using namespace libMesh;

Face::Face(const uint _id, const ElemType type_in, const int _cv1, const int _cv2, std::vector<uint>& _nodes):
    id(_id),
	type(type_in),
    cv1(_cv1),
    cv2(_cv2),
	nodes()
{
    nodes.swap(_nodes);
}

	
GRUMMP_IO::GRUMMP_IO(ReplicatedMesh &mesh, const int precision, const bool obeys_right_hand_rule):
    _mesh(mesh),
    _precision(precision),
    _obeys_right_hand_rule(obeys_right_hand_rule)
{}

GRUMMP_IO::~GRUMMP_IO()
{
	for(face_iterator fc = _faces.begin() ; fc != _faces.end();	++fc)
	{
		delete *fc;
	}
}

double GRUMMP_IO::random(const uint seed)
{
    srand(seed);
    const double sign = ((rand() % 2) - 0.5) * 2;
    const double magnitude = (double)rand() / (double)RAND_MAX * 0.5 + 0.5;
    return sign * magnitude;

}

void GRUMMP_IO::perturb_pyramids(const double delta)
{

    std::vector<bool> is_perturbed(_mesh.n_nodes(), false);
			
    MeshBase::const_element_iterator el = _mesh.active_elements_begin();
    const MeshBase::const_element_iterator el_end = _mesh.active_elements_end();
    for(; el != el_end ; ++el)
    {
        Elem *elem = *el;

        if( elem->type() == PYRAMID5 )
        {
            Node &node4 = *elem->get_node(4);
            if( !is_perturbed[node4.id()] )
            {
                const double ls = cbrt( elem->volume());
                const uint s = elem->id();
						
                node4(0) += ls * delta * random(s);
                node4(1) += ls * delta * random(s);
                node4(2) += ls * delta * random(s);
                is_perturbed[node4.id()] = true;
            } // end of if(perturbed)
						
        } // end of if(pyramid)
    } // end of for(el)
} // All done

#ifdef WITH_PETSC
void GRUMMP_IO::reorder_petsc(std::string type)
{
    PetscErrorCode ierr;
    const int max_n_stencil = 7;

    Mat A;
    ierr=MatCreateSeqAIJ(PETSC_COMM_SELF, _mesh.n_elem(), _mesh.n_elem(), max_n_stencil, NULL, &A);libmesh_assert(ierr==0);

    MeshBase::const_element_iterator el = _mesh.active_elements_begin();
    const MeshBase::const_element_iterator el_end = _mesh.active_elements_end();

    // ------------------------------ Assemble the matrix
    for(; el != el_end ; ++el)
    {
        // Find the stencil
        const Elem* elem = *el;
        const int elem_id = elem->id();
        std::vector<int> stencil_ids(max_n_stencil);
        stencil_ids[0] = elem_id;
        int n_neigh = 1;

			    
        libmesh_assert_less_msg(elem->n_sides(), max_n_stencil, elem->n_sides() << " must be < " << max_n_stencil);				
        for (uint side=0; side<elem->n_sides(); side++)
        {
            if (elem->neighbor(side) == NULL) continue;

            const Elem* neighbor = elem->neighbor(side);
            const unsigned int neighbor_id = neighbor->id();
            stencil_ids[n_neigh] = neighbor_id;
            n_neigh++;
        }

        // Assemble the stencil
        const PetscScalar val[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
        ierr=MatSetValues(A, 1, &elem_id, n_neigh, &stencil_ids.front(), val, INSERT_VALUES);libmesh_assert(ierr==0);
    }
    ierr=MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);libmesh_assert(ierr==0);
    ierr=MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);libmesh_assert(ierr==0);

    // get the matrix reordering and destroy it
    IS rperm, cperm;
    const int* new_to_old;
    ierr=MatGetOrdering(A, type.c_str(), &rperm, &cperm);libmesh_assert(ierr==0);
    ierr=MatDestroy(&A);libmesh_assert(ierr==0); A=NULL;

    ierr=ISGetIndices(rperm, &new_to_old);libmesh_assert(ierr==0);

    // ------------------------------ renumber the mesh

    // add an element to increase the size
    const int n_elem = _mesh.n_elem();
    Elem *e = new Edge2;
    e->set_id( n_elem*2);
    _mesh.add_elem(e);

    // shift all elements
    for(int ie = 0 ; ie < n_elem ; ie++)
    {
        _mesh.renumber_elem(ie, ie+n_elem);
    }

    // move elements back to their new numbering
    for(int inew = 0 ; inew < n_elem ; inew++)
    {
        const int iold = new_to_old[inew]+n_elem;
        _mesh.renumber_elem(iold, inew);
    }

    // delete the dummy element again again
    _mesh.delete_elem(e);

    // -------------------------- destroy the remaining stuff
    ierr=ISRestoreIndices(rperm, &new_to_old);libmesh_assert(ierr==0);
    ierr=ISDestroy(&rperm);libmesh_assert(ierr==0);
    ierr=ISDestroy(&cperm);libmesh_assert(ierr==0);
} 
#endif /* WITH_PETSC */

void GRUMMP_IO::create_face_info()
{

	for(auto el = _mesh.active_elements_begin(); el !=  _mesh.active_elements_end() ; ++el)
	{
		const Elem* elem = *el;
		const unsigned int elem_id = elem->id();

		// check mesh: check the element volume to
		// be positive
		libmesh_assert(elem->volume() > 0);

		// debug
		// find element numbering
		// for (uint node_id=0; node_id < elem->n_nodes() ; node_id++)
		// {
		// 	const Node &node = *elem->get_node(node_id);
		// 	std::cout << node.id() << " ";
		// }
		//std::cout << std::endl;

		for (uint side=0; side<elem->n_sides(); side++)
		{
			// get side and its nodes
			std::vector<uint> nodes_side;
			UniquePtr<Elem> elem_side = elem->side(side);
			std::vector<boundary_id_type> bdry_ids;

			// if the side is on boundary
			if (elem->neighbor(side) == NULL)
			{

				 _mesh.boundary_info->boundary_ids(elem, side, bdry_ids);
				int bdry_id;

				// Default boundary id = 500
				if(bdry_ids.size()>0)  bdry_id=bdry_ids.front();
				else bdry_id = 500;

				for (uint node_id=0 ; node_id < elem_side->n_nodes() ; node_id++)
				{
					//const int z = elem_side->n_nodes() - node_id - 1;
					const Node &node = *elem_side->get_node(node_id);
					nodes_side.push_back(node.id());
				}
				Face *fc = new Face(_faces.size(), elem_side->type(), elem_id, -std::abs(bdry_id),  nodes_side);
				_faces_bdry.push_back(fc);
				_faces.push_back(fc);

				// debug
				// print the cell + side
				// print the face vertices
				// std::cout << "(" << elem->id() << "," << side << "): ";
				// for (uint node_id=0; node_id < fc->face->n_nodes() ; node_id++)
				// {
				// 	const Node &node = *fc->face->get_node(node_id);
				// 	std::cout << node.id() << " ";
				// }
				// std::cout << std::endl;
			} // End of boundary face
			else
			{
				const Elem* neighbor = elem->neighbor(side);
				const unsigned int neighbor_id = neighbor->id();

				for (uint node_id=0 ; node_id < elem_side->n_nodes() ; node_id++)
				{
					const Node &node = *elem_side->get_node(node_id);
					nodes_side.push_back(node.id());
				}

				if (elem_id < neighbor_id)
				{
					// Pointer to the element side
					Face *fc = new Face(_faces.size(), elem_side->type(), elem_id, neighbor_id, nodes_side);
					_faces.push_back(fc);
				}

				// debug
				// print the cell + side
				// print the face vertices
				// UniquePtr<Elem> elem_side (elem->side(side));
				// std::cout << "(" << elem->id() << "," << side << "): ";
				// for (uint node_id=0; node_id < elem_side->n_nodes() ; node_id++)
				// {
				//  	const Node &node = *elem_side->get_node(node_id);
				//  	std::cout << node.id() << " ";
				//  }
				// std::cout << std::endl;
			} // End of internal face

		}// end loop over side
	}// end loop over elements
} // All done

void GRUMMP_IO::write_mesh(const std::string& file_name)
{
	// open the file
    std::fstream fs;
    fs << std::scientific ;
    fs << std::setprecision(_precision) ;
    fs.open(file_name, std::fstream::out); assert(fs.is_open());
			
    // Make sure faces are there
    if (_faces.size() == 0) this->create_face_info();

    /*
      Write the mesh file
    */

    // header
    fs << _mesh.n_elem() << " "
       << _faces.size() << " "
       << _faces_bdry.size() << " "
       << _mesh.n_nodes() << std::endl;
    fs << std::endl;

    // write the vertices
    {
        MeshBase::const_node_iterator nd = _mesh.nodes_begin();
        const MeshBase::const_node_iterator nd_end = _mesh.nodes_end();

        for(; nd != nd_end ; ++nd)
        {
            const Node &node = **nd;
            if (_mesh.mesh_dimension() == 2)
                fs  << node(0) << " " << node(1) << std::endl;
            else if (_mesh.mesh_dimension() == 3)
                fs << node(0) << " " << node(1) << " " << node(2) << std::endl;
            else
                assert(0);
        }
        fs << std::endl;
    }

    // write the faces
    {
        face_iterator fc = _faces.begin();
        const face_iterator fc_end = _faces.end();

        for(; fc != fc_end;	++fc)
        {
            const Face *face = *fc;

            // write left and rigth CV's
            fs <<  face->cv1 << " " << face->cv2  << " ";

            // write the nodes
            if (_obeys_right_hand_rule)
            {
                for (uint node_id=0 ; node_id < face->nodes.size(); node_id++)
                    fs << face->nodes[node_id] << " ";
            }
            else 
            {
                for (int node_id=face->nodes.size()-1 ; node_id >= 0 ; node_id--)
                    fs << face->nodes[node_id] << " ";
            }
            fs << std::endl;
					
        }
    }
    fs << std::endl;

    // write the boundary faces
    {
        face_iterator fc = _faces_bdry.begin();
        const face_iterator fc_end = _faces_bdry.end();

        for(; fc != fc_end;	++fc)
        {
            const Face *face = *fc;

            fs << face->id << " " << face->bdry_id() << " ";
					
            //write the nodes
            for (auto it = face->nodes.begin(); it != face->nodes.end() ; ++it)
            {
                fs << *it << " ";
            }
            fs << std::endl ;
        }
    }
    fs << std::endl;

    // write region numbers
    {
        MeshBase::const_element_iterator el = _mesh.elements_begin();
        const MeshBase::const_element_iterator el_end = _mesh.elements_end();

        for(; el != el_end ; ++el)
        {
            const Elem *elem = *el;
            fs << elem->subdomain_id() << std::endl ;
        }
    }

    // close the file
    fs.close();
}

ReplicatedMesh* GRUMMP_IO::surface_wall_mesh(std::string file_name, const std::set<uint> &walltags)
{
	// NOTE: The normal of boundary faces points towards inside, which is what we actually want
	// for the surface mesh.
	// First we create a surface mesh, then we call all_tri on it, an finally we write the mesh.


	/*
	 * Find vertices on the boundary
	 */
	std::set<uint> bdry_verts;
	std::map<uint, uint> map_vert_old_to_new;
	std::vector<uint>   map_vert_new_to_old;

	for(face_iterator fc = _faces_bdry.begin() ; fc != _faces_bdry.end();	++fc)
	{
		const Face *face = *fc;
		if (!walltags.count(face->bdry_id())) continue;

		//w rite the nodes
		for (auto it = face->nodes.begin(); it != face->nodes.end() ; ++it)
		{
			bdry_verts.insert(*it);
		}
	} // Found all verts on the boundary

	/*
	 * Give index on the vertices on the boundary
	 */
	{
		std::set<uint>::iterator it = bdry_verts.begin();
		const std::set<uint>::iterator it_end = bdry_verts.end();
		uint inew = 0, iold;
		for(; it != it_end ; ++it, ++inew)
		{
			iold = *it;
			map_vert_old_to_new.insert( std::make_pair(iold, inew) );
			map_vert_new_to_old.push_back( iold );
		}
	}

	/*
	 * Create a mesh using the old mesh
	 */
	uint n_points = map_vert_new_to_old.size();
	ReplicatedMesh *ms = new ReplicatedMesh(_mesh.comm());

	// Set mesh dimensions
	ms->set_mesh_dimension(2);

	// Add the vertices to the mesh
	ms->reserve_nodes ( n_points );
	for (uint pn=0 ; pn < n_points ; pn++)
	{
		const uint po = map_vert_new_to_old[pn];
		const Point &pointn = _mesh.point(po);
		ms->add_point (pointn, pn);
	}

	// Add the elements to the mesh
	uint face_id = 0;
	for(face_iterator fc = _faces_bdry.begin() ; fc != _faces_bdry.end();	++fc, ++face_id)
	{
		const Face *face = *fc;
		if (!walltags.count(face->bdry_id())) continue;

		Elem * elem = Elem::build(face->type).release();
		elem->set_id(face_id);
		elem = ms->add_elem(elem);
		elem->subdomain_id() = 1;

		libmesh_assert( elem->n_nodes() == face->nodes.size() );
		for (uint i=0; i<elem->n_nodes(); i++)
		{
			const uint node_id = map_vert_old_to_new[ face->nodes[i] ];
			elem->set_node(i) = ms->node_ptr(node_id);
		}
	}

	/*
	 * Convert mesh to triangles
	 */
	ms->prepare_for_use();
	MeshTools::Modification::all_tri(*ms);

	/*
	 * Write the mesh in Off format
	 */
	try
	{
		std::cout <<"WRITING to  " << file_name << std::endl;

		std::fstream fs(file_name, std::fstream::out);

		fs << "OFF" << std::endl;

		fs << ms->n_nodes() << " " << ms->n_elem() << " " << 0 << std::endl;

		for(auto nd = ms->nodes_begin(); nd !=  ms->nodes_end() ; ++nd)
		{
			const Node& node = **nd;
			fs << node(0) << " " << node(1) << " " << node(2) << std::endl;
		}
		fs << std::endl;

		for(auto el = ms->active_elements_begin(); el !=  ms->active_elements_end() ; ++el)
		{
			const Elem* elem = *el;
			fs << 3 << " ";
			for (uint node_id=0 ; node_id < elem->n_nodes() ; node_id++)
			{
				fs << elem->node(node_id) << " ";
			}
			fs << std::endl;
		}
		fs << std::endl;
	}
	catch(...)
	{
		libmesh_error_msg("Could not open " << file_name);
	}

	return ms;

} // All done
