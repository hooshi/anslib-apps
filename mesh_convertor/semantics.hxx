// C++ include files that we need
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <assert.h>
#include <time.h>

// Basic include files needed for the mesh functionality.
#include "libmesh/libmesh.h"
#include "libmesh/mesh.h"
#include "libmesh/replicated_mesh.h"

#include "libmesh/gmsh_io.h"
#include "libmesh/vtk_io.h"
#include "libmesh/exodusII_io.h"


// Define useful datatypes for finite element
// matrix and vector components.
#include "libmesh/elem.h"
#include "libmesh/edge_edge2.h"

// Get pot
#include "libmesh/getpot.h"


// Petsc
//#define WITH_PETSC

