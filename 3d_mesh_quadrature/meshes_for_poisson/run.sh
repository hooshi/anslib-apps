#!/bin/bash


echo "tet_5"
../generator_opt n=6 ctype=0 out=tet_5
echo "tet_10"
../generator_opt n=11 ctype=0 out=tet_10
echo "tet_20"
../generator_opt n=21 ctype=0 out=tet_20
echo "tet_40"
../generator_opt n=41 ctype=0 out=tet_40

echo "hex_5"
../generator_opt n=6 ctype=1 out=hex_5
echo "hex_10"
../generator_opt n=11 ctype=1 out=hex_10
echo "hex_20"
../generator_opt n=21 ctype=1 out=hex_20
echo "hex_40"
../generator_opt n=41 ctype=1 out=hex_40

echo "wedge_5"
../generator_opt n=6 ctype=2 out=wedge_5
echo "wedge_10"
../generator_opt n=11 ctype=2 out=wedge_10
echo "wedge_20"
../generator_opt n=21 ctype=2 out=wedge_20
echo "wedge_40"
../generator_opt n=41 ctype=2 out=wedge_40

echo "pyramid_5"
../generator_opt n=6 ctype=3 out=pyramid_5
echo "pyramid_10"
../generator_opt n=11 ctype=3 out=pyramid_10
echo "pyramid_20"
../generator_opt n=21 ctype=3 out=pyramid_20
echo "pyramid_40"
../generator_opt n=41 ctype=3 out=pyramid_40









