#include "mesh.hxx"
#include <cmath>
#include <string>
#include <cstring>
#include <vector>

const double Fx = 1;
const double Fy = 1;
const double Fz = 1;

void test_func(const double r[], double ans[])
{
	const double x = r[0];
	const double y = r[1];
	const double z = r[2];

	// a trigonometric function
	ans[0]= Fx * cos(M_PI * x)    * cos(M_PI * y)   * sin(2*M_PI * z) ;
	ans[1]= Fy * sin(M_PI * x)    * sin(2*M_PI * y) * cos(M_PI * z);
	ans[2]= Fz * sin(2* M_PI * x) * sin(M_PI * y)   * cos(2*M_PI * z);

	// a polynomial
	// ans[0]=x*x*x*x;
	// ans[1]=y*y*y*y;
	// ans[2]=z*z*z*z;
}

double div_test_func(const double r[])
{
	const double x = r[0];
	const double y = r[1];
	const double z = r[2];

	// a trigonometric function
	return (
		-M_PI   * Fx * sin(M_PI * x)    * cos(M_PI * y)   * sin(2*M_PI * z)  +
		2*M_PI  * Fy * sin(M_PI * x)    * cos(2*M_PI * y) * cos(M_PI * z)    +
		-2*M_PI * Fz * sin(2* M_PI * x) * sin(M_PI * y)   * sin(2*M_PI * z) 
		);

	// a polynomial
	// return (x*x*x + y*y*y + z*z*z) * 4.;
}

const double div_over_all_domain = 0;
const double f_over_all_domain = 0;


void integrate(const Mesh &mesh, const uint vol_ord, const uint face_ord, std::vector<double>& errors)
{
	assert(mesh.get_n_elem() > 0);

	double l2_norm = 0;
	double total_face_integ= 0;
	double total_vol_integ = 0;
	
	for (uint el=0 ; el < mesh.get_n_elem() ; el++)
	{

		double vol_integ = 0;
		double face_integ = 0;

		/*
		  Integrate the divergence over element volume
		 */
		const CellType tp = mesh.get_elem_type(el);
		const int dummy_ids[] = {0,1,2,3,4,5,6,7};
		CellInfo cell(tp, dummy_ids);
		double r[N_CELL2VERT_3D][3]; mesh.get_elem_coords(el, r);

		QGenerator q;
		q.init(tp, vol_ord);
		q.init_3d_gauss();
		
		FEMap fe_map(tp, r);

		for(uint qp=0 ; qp < q.n_points() ; qp++)
		{
			double jac;
			double xyz[3];

			fe_map.map_to_physical(q.qp(qp).array(), xyz, NULL, jac);
		    vol_integ += jac * q.w(qp) * div_test_func(xyz);
		} // end of volume integration

		/*
		  Integrate the function over element surface
		*/
		std::vector<CellInfo> sides;
		cell.sides(sides);

		for (uint sd=0 ; sd < sides.size() ; sd++)
		{
			const CellInfo &side_cell = sides[sd];
			double r_side[N_FACE2VERT_3D][3];
			std::vector<int> side_verts = side_cell.points();
			

			//debug
			//fprintf(stderr,"side coords: \n");
			for (uint vside=0 ; vside < side_cell.n_verts() ; vside++)
			{
				const uint v = side_verts[vside];
				r_side[vside][0] = r[v][0];
				r_side[vside][1] = r[v][1];
				r_side[vside][2] = r[v][2];

				//debug
				//fprintf(stderr,"%lf, %lf, %lf \n",  r[v][0],  r[v][1],  r[v][2]);
			}
			//debug
			//fprintf(stderr,"side type: %s \n", side_cell.name().c_str());


			FEMap fe_side(side_cell.type(), r_side);
			QGenerator q_side;
			q_side.init(side_cell.type(), face_ord);
			q_side.init_2d_gauss();

			for(uint qp=0 ; qp < q_side.n_points() ; qp++)
			{
				double jac;
				double xyz[3];
				double normal[3];
				double func[3];

				fe_side.map_to_physical(q_side.qp(qp).array(), xyz, normal, jac);
				test_func(xyz, func);
				face_integ += jac * q_side.w(qp) *
					(func[0]*normal[0] + func[1]*normal[1] + func[2]*normal[2]);
				//debug
				//fprintf(stderr,"qp(%d): normal: %lf, %lf, %lf  \n     xyz: %lf, %lf, %lf, JxW: %lf\n",
				//qp, normal[0], normal[1], normal[2], 
				 //xyz[0], xyz[1], xyz[2], jac * q_side.w(qp));
			}
		} //end of face integration

		const double error = fabs(face_integ - vol_integ);
		l2_norm += error * error;
		total_vol_integ += vol_integ;
		total_face_integ += face_integ;

		//debug
		//fprintf(stderr,"el(%d): %e %e %e \n\n", el, vol_integ, face_integ, error);
	
	}

	//debug
	// printf("VI: %e FI: %e ",
	// 	  fabs(total_vol_integ-3) ,
	// 	   fabs(total_face_integ-3));

	l2_norm /= mesh.get_n_elem();
	l2_norm = sqrt(l2_norm);

	errors.resize(0);
	errors.push_back(total_vol_integ-div_over_all_domain);
	errors.push_back(total_face_integ-f_over_all_domain);
	errors.push_back(l2_norm);
   	
}

int main()
{
	// test 3d integration
	{		
		const CellType cell_type = HEX;
		const double delta = 0.7;
		const uint vol_ord = 7;
		const uint surf_ord = 1;
		const uint r_levels= 3;
		
		double pre=-1;
		uint n = 5;
		CubeMesh mesh;
		const double l[] = {1,1,1};

		for (uint i = 0 ; i < r_levels ; i++)
		{
			const uint sizes[] ={n,n,n};
			mesh.build_mesh(sizes, l, cell_type);
			mesh.perturb_mesh(delta);

			char name[200];
			sprintf(name, "mesh_%d.vtk", i);
			mesh.write_vtk(name );

			std::vector<double> errors;
 			integrate(mesh, vol_ord, surf_ord, errors);

			if(pre < 0) pre=errors[2];
			printf("v:%e f:%e l2:%e %.2lf\n", errors[0], errors[1], errors[2], log(pre/errors[2]));

			// printf("%e ", error );
			// if(pre>-1) printf("%.2lf \n", log(pre/error) );
			// else printf("\n");
			pre=errors[2];

		
			n *= 2;
		}
	}	
	
	return 0;
}
