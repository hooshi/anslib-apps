#include "mesh.hxx"
#include <cmath>
#include <string>
#include <cstring>
#include <vector>

#include "GetPot"


int box_bdry_tag(const double[])
{
    // Dirichlet boundary condition 
    return 1;
}
    
// TET=0, HEX, WEDGE, PYRAMID

int main(int argc, char *argv[])
{

	GetPot getpot(argc, argv);
	
	CubeMesh mesh;
	const uint cell_type_uint = getpot.operator()("ctype",1);
	const uint n = getpot("n", 3);
	const double l = getpot("l", 1);
	const double delta = getpot("delta", 0.4);
	std::string out = getpot("out", "mesh");

	const CellType cell_type = (CellType)cell_type_uint;
	const uint sizes[] ={n,n,n};
	const double lengths[] = {l,l,l};
			
	mesh.build_mesh(sizes, lengths, cell_type);
	mesh.perturb_mesh(delta);
	
	mesh.write_grummp((out+".vmesh").c_str(), box_bdry_tag);
	mesh.write_vtk((out+".vtk").c_str());

	return 0;
}
