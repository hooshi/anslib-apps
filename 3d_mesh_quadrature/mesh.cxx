#include "mesh.hxx"

#include <cstring>
#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <memory>
#include <algorithm>

#include <boost/assign/list_of.hpp>
#include <boost/assign/std/vector.hpp>

/******************************************************\
                          Mesh
\******************************************************/

int cell2vtk(const CellType inp)
{
    switch(inp)
    {
    case TRI:
        return 5;
    case QUAD:
        return 9;
    case TET:
        return 10;
    case HEX:
        return 12;
    case WEDGE:
        return 13;
    case PYRAMID:
        return 14;		
    default:
        assert(0);
        return 0;
    }
}


void Mesh::write_vtk(const char fname[])
{
    FILE *fl;

    fl = fopen(fname, "w");
    assert(fl);

    // write the header
    fprintf(fl, "# vtk DataFile Version 3.0\n");
    fprintf(fl, "Mixed element cube mesh\n");
    fprintf(fl, "ASCII\n");
    fprintf(fl, "DATASET UNSTRUCTURED_GRID\n\n");

    // write the points
    fprintf(fl, "POINTS %d float\n", _n_pts);
    for(uint i=0; i<_n_pts ; i++)
    {
        double pts[3];
        get_point_coords(i, pts);
        fprintf(fl, "%lf %lf %lf \n", pts[0], pts[1], pts[2]);
    }
    fprintf(fl, "\n");

    //find the total cell size list
    uint icelllength = 0;
    for(uint i=0; i<_n_elem ; i++)
    {
        icelllength++;
        icelllength += CellInfo(_elem_type[i]).n_verts();
    }
	
    //write the cell connectivity
    fprintf(fl, "CELLS %d %d\n", _n_elem, icelllength);
    for(uint i=0; i<_n_elem ; i++)
    {
        const uint n_elem_verts = CellInfo(_elem_type[i]).n_verts();
		
        fprintf(fl, "%d ", n_elem_verts );

        // vertex ordering for wedge is different in VTK 
        if (_elem_type[i] == WEDGE)
        {
            fprintf(fl, "%d %d %d ",  _conn[i][3], _conn[i][4], _conn[i][5]);
            fprintf(fl, "%d %d %d \n", _conn[i][0], _conn[i][1], _conn[i][2]);			
        }
        else
        {
            for (uint v=0; v<n_elem_verts ; v++)
                fprintf(fl, "%d ", _conn[i][v]);
        }
	    
        fprintf(fl, "\n");
    }

    //write cell types
    fprintf(fl, "CELL_TYPES %d \n", _n_elem);
    for(uint i=0; i<_n_elem ; i++)
    {
        fprintf(fl, "%d \n", cell2vtk(_elem_type[i]) );
    }

    // write element id's
    fprintf(fl, "CELL_DATA %d\n", _n_elem);
	
    fprintf(fl, "SCALARS elem_id int\n");
    fprintf(fl, "LOOKUP_TABLE default\n");
    for(uint i=0; i<_n_elem ; i++)
    {
        fprintf(fl, "%d\n", i);
    }

    if(_is_bdry.size() == (uint)_n_elem)
    {
        fprintf(fl, "SCALARS is_bdry int\n");
        fprintf(fl, "LOOKUP_TABLE default\n");
        for(uint i=0; i<_n_elem ; i++)
        {
            fprintf(fl, "%d\n", (int)_is_bdry[i]);
        }
    }
	
    fclose(fl);
}

void Mesh::check_orientation()
{
    for (uint elem=0 ; elem < _n_elem ; elem++)
    {
        FELagrange fe(_elem_type[elem]);

        double x[N_CELL2VERT_3D][3];
        get_elem_coords(elem, x);
        FEMap fe_map(_elem_type[elem], x);

        double xi[3]; fe.ref_centroid(xi);
        double p[3];
        double jac;
        double norm[3];
		
        fe_map.map_to_physical(xi, p, norm, jac);
        //printf("%lf \n ", jac);
        assert(jac>0);
    }
}

/******************************************************\
                          Cube
\******************************************************/

void CubeMesh::point_str2un(const uint ijk[], uint &num)
{
    const uint i = ijk[0];
    const uint j = ijk[1];
    const uint k = ijk[2];
	
    num = k*(_nx*_ny) + j * _nx +  i;
}

void CubeMesh::point_un2str(const uint num, uint ijk[])
{
    ijk[0]= num % _nx;
    ijk[1]= (num % (_nx*_ny)) / _nx;
    ijk[2]= num / (_nx*_ny);
}


void CubeMesh::build_mesh(const uint n_verts[], const double lengths[], const uint cell_type, double **coords)
{
    _lx = lengths[0];
    _ly = lengths[1];
    _lz = lengths[2];

    _nx = n_verts[0];
    _ny = n_verts[1];
    _nz = n_verts[2];

    // find number of points and elements
    _n_pts = _nx*_ny*_nz;
    const uint n_hexx = _nx-1;
    const uint n_hexy = _ny-1;
    const uint n_hexz = _nz-1;
    const uint n_hex = n_hexx * n_hexy * n_hexz;
    const uint max_n_elem = n_hex * 6;
    const double hx = _lx / n_hexx;
    const double hy = _ly / n_hexy;
    const double hz = _lz / n_hexz;


    // allocate memory
    if(_pts) delete[] _pts;
    if(_elem_type) delete[] _elem_type;
    if(_conn) delete[] _conn;
	
    _pts = new double[_n_pts][3];
    _elem_type = new CellType[max_n_elem];
    _conn = new uint[max_n_elem][N_CELL2VERT_3D];

    // create the mesh
    CellType ctype = (CellType) cell_type;
    _n_elem = 0;
	
    for(uint k=0 ; k < n_hexz ; k++)
        for(uint i=0 ; i < n_hexx ; i++)
            for(uint j=0 ; j < n_hexy ; j++)
            {
                const uint corner[]={i,j,k};
                build_elem_conn(corner, ctype);
            }

    // create the point locations
    for(uint k=0 ; k < _nz ; k++)
        for(uint j=0 ; j < _ny ; j++)
            for(uint i=0 ; i < _nx ; i++)
            {
                const uint corner[]={i,j,k};
                uint i_vert;
                point_str2un(corner, i_vert);

                if(coords && coords[0])
                    _pts[i_vert][0] = coords[0][i];
                else
                    _pts[i_vert][0] = hx*i;

                if(coords && coords[1])
                    _pts[i_vert][1] = coords[1][j];
                else
                    _pts[i_vert][1] = hy*j;

                if(coords && coords[2])
                    _pts[i_vert][2] = coords[2][k];
                else
                    _pts[i_vert][2] = hz*k;
					
				   
            }
}

void CubeMesh::perturb_mesh(const double delta)
{

    const uint n_hexx = _nx-1;
    const uint n_hexy = _ny-1;
    const uint n_hexz = _nz-1;
    const double hx = _lx / n_hexx / 2.;
    const double hy = _ly / n_hexy / 2.;
    const double hz = _lz / n_hexz / 2.;

    for(uint k=0 ; k < _nz ; k++)
        for(uint j=0 ; j < _ny ; j++)
            for(uint i=0 ; i < _nx ; i++)
            {
                const uint corner[]={i,j,k};
                uint v;
                point_str2un(corner, v);
                double dx,dy,dz;

                const int sgn = (rand() % 2)*2 - 1 ;
				
                if( (i==0) || (i==_nx-1)) dx = 0;
                else dx = ((double)rand()/(double)RAND_MAX) * delta * hx ;

                if( (j==0) || (j==_ny-1)) dy = 0;
                else dy = ((double)rand()/(double)RAND_MAX) * delta * hy;

                if( (k==0) || (k==_nz-1)) dz = 0;
                else dz = ((double)rand()/(double)RAND_MAX) * delta * hz;

                _pts[v][0] += sgn*dx;
                _pts[v][1] += sgn*dy;
                _pts[v][2] += sgn*dz;

            }

}

void CubeMesh::build_elem_conn(const uint as[], const CellType type)
{
    using namespace boost::assign;

    const uint bs[] = {as[0]+1 , as[1]   , as[2]};
    const uint cs[] = {as[0]+1 , as[1]+1 , as[2]};
    const uint ds[] = {as[0]   , as[1]+1 , as[2]};
    const uint es[] = {as[0]   , as[1]   , as[2]+1};
    const uint fs[] = {as[0]+1 , as[1]   , as[2]+1};
    const uint gs[] = {as[0]+1 , as[1]+1 , as[2]+1};
    const uint hs[] = {as[0]   , as[1]+1 , as[2]+1};

    uint a,b,c,d,e,f,g,h;
    point_str2un(as, a);
    point_str2un(bs, b);
    point_str2un(cs, c);
    point_str2un(ds, d);
    point_str2un(es, e);
    point_str2un(fs, f);
    point_str2un(gs, g);
    point_str2un(hs, h);



    std::vector<std::vector<uint> > conn;
    std::vector<CellType> sub_types;
	
    switch(type)
    {
    case HEX:
    {

        conn.resize(1);
        sub_types += HEX;
        conn[0] += a,b,c,d,e,f,g,h;

        break;
    }
	
    case WEDGE:
    {
		
        conn.resize(2);
        sub_types += WEDGE, WEDGE;
        conn[0] += a,b,d,e,f,h;
        conn[1] += d,b,c,h,f,g;

        break;
				
    }
	
    case PYRAMID:
    {
        conn.resize(4);
        sub_types += PYRAMID, PYRAMID, TET, TET;
        conn[0] += a, b, c, d, f;
        conn[1] += f, e, h, g, d;
        conn[2] += c, g, d, f;
        conn[3] += a, d, e, f;

        break;
    }

    case TET:
    {
        conn.resize(6);
        sub_types += TET, TET, TET, TET, TET, TET;
        conn[0] +=f, e, h ,a;
        conn[1] +=b, f, h, a;
        conn[2] +=b, h, d, a;
        conn[3] +=f, h, g, b;
        conn[4] +=d, g, h, b;
        conn[5] +=d, c, g, b;
		
        break;
    }

    default:
        assert("Not supported" && 0);
	
    }

    // create the elements
    for(uint i = 0 ; i < conn.size() ; i++)
    {
        for(uint j=0 ; j < conn[i].size() ; j++)
            _conn[_n_elem][j] = conn[i][j];
		
        _elem_type[_n_elem] = sub_types[i];
        _n_elem++;
    }
}


class FaceCompare
{

    std::vector<int> _points;
    const uint _cv;

public:

    const std::vector<int>& points() const {return _points;}
    uint n_verts() const {return _points.size();}
    uint cv() const {return _cv;}
	
    FaceCompare(std::vector<int>& points_in, const uint cv_in):
        _cv(cv_in)
        {
            _points.swap(points_in);
        }

    class CMP
    {
    public:

        // Note must return if c1 < c2
        bool operator()(const std::unique_ptr<FaceCompare>& c1,
                        const std::unique_ptr<FaceCompare>& c2) const 
            {
			
                // first compare their size
                if(c1->n_verts() < c2->n_verts()) return true;
                else if(c2->n_verts() < c1->n_verts()) return false;

                // compare verts one by one
                else
                {
                    std::vector<int> verts1 = c1->points();
                    std::vector<int> verts2 = c2->points();

                    std::sort(verts1.begin(), verts1.end());
                    std::sort(verts2.begin(), verts2.end());

                    for(uint i = 0 ; i < verts1.size() ; i++)
                    {
                        if(verts1[i] < verts2[i]) return true;
                        else if(verts1[i] > verts2[i]) return false;
                    }
                    return false;
                }
            }
    };
};


void CubeMesh::write_grummp(const char file_name[], int (*bdry_tag)(const double[])) const
{
    /*
     * Go over all elements and store their
     * faces inside a vector
     */
    std::vector<std::unique_ptr<FaceCompare> > all_faces;
    for(uint cell=0; cell < _n_elem; cell++)
    {
        std::vector<CellInfo> cell_faces;
        CellInfo cell_info(_elem_type[cell], _conn[cell]);
        cell_info.sides(cell_faces);

        for (auto it = cell_faces.begin(); it != cell_faces.end() ; ++it)
            all_faces.push_back(std::unique_ptr<FaceCompare>(new FaceCompare(it->points(), cell) ));
    }

    /*
     * Now sort the faces by comparing their verts.
     */
    FaceCompare::CMP cmp;
    std::sort(all_faces.begin(), all_faces.end(), cmp);

    // debug
    // print the faces
    // for (auto it = all_faces.begin() ; it != all_faces.end() ; ++it)
    // {
    // 	const std::unique_ptr<FaceCompare>&  fc = *it;
    // 	printf("%d ", fc->cv());

    // 	for(auto v= fc->points().begin() ; v!=fc->points().end() ; ++v)
    // 		printf("%d ", *v);

    // 	printf("\n ");
    // }
    // make sure everything is correct

    /*
     * Extract the face lists
     */
    std::vector<uint> iface_bdry;
    std::vector<uint> iface_internal;
    std::vector<std::pair<uint, uint> > cvs;
	

    int iface = 0;
    for (auto it = all_faces.begin() ; it != all_faces.end() ; ++it, ++iface)
    {
		
        auto it2 = it; it2++;
        if(it2 == all_faces.end())
        {
            iface_bdry.push_back(iface);
            break;
        }

        const bool g = cmp(*it, *it2);
        const bool s = cmp(*it2, *it);
        const bool is_bdry =  (g != s);
        const bool is_internal = ((!g) && (!s));
        const bool is_invalid = (g && s);

        assert(!is_invalid);
        if(is_internal)
        {
            iface_internal.push_back(iface);
            cvs.push_back(std::make_pair((*it)->cv(), (*it2)->cv()));
            ++it; ++iface;			
        }
        else
        {
            assert(is_bdry);
            iface_bdry.push_back(iface);
        }
    }

    /*
     * open the file and write the data
     */
    FILE *fl = fopen(file_name, "w");
    assert(fl);

    // write the number of verts + ...
    const uint n_faces = iface_bdry.size() + iface_internal.size();
    fprintf(fl, "%d %d %d %d \n",
            _n_elem, n_faces, (uint)iface_bdry.size(), _n_pts);
    fprintf(fl, "\n");

    // write the vert coordinates
    for (uint v = 0 ; v < _n_pts ; v++)
    {
        fprintf(fl, "%.16lf %.16lf %.16lf \n",
                _pts[v][0], _pts[v][1], _pts[v][2]);
    }
    fprintf(fl, "\n");
	
    // write the faces
    for(uint i = 0 ; i < iface_internal.size() ; i++)
    {
        const uint face = iface_internal[i];
			
        fprintf(fl, "%d %d ", cvs[i].first, cvs[i].second);
        for(auto v= all_faces[face]->points().begin() ; v!=all_faces[face]->points().end() ; ++v)
        {
            fprintf(fl, "%d ", *v);
        }
        fprintf(fl, "\n");
    }


    _is_bdry.resize(_n_elem, false);
    for(uint i = 0 ; i < iface_bdry.size() ; i++)
    {
        const uint face = iface_bdry[i];

        // find the center of the face
        double coords[] = {0,0,0};
        for(uint j = 0 ; j < all_faces[face]->points().size() ; j++)
        {
            const int ipoint = all_faces[face]->points()[j];
            coords[0] += _pts[ipoint][0];
            coords[1] += _pts[ipoint][1];
            coords[2] += _pts[ipoint][2];
        }
        coords[0] /= all_faces[face]->points().size();
        coords[1] /= all_faces[face]->points().size();
        coords[2] /= all_faces[face]->points().size();

        fprintf(fl, "%d %d ", all_faces[face]->cv(), -bdry_tag(coords));
        for(auto v= all_faces[face]->points().begin() ; v!=all_faces[face]->points().end() ; ++v)
        {
            fprintf(fl, "%d ", *v);
        }
        fprintf(fl, "\n");

        // record that the cv is on boundary
        _is_bdry[all_faces[face]->cv()] = true;
    }


    // close the file
    fclose(fl);
}


CellType gmsh2cell(const uint in)
{
    switch(in)
    {
    case 1: return EDGE;
    case 2: return TRI;
    case 3: return QUAD;
    case 4: return TET;
    case 5: return HEX;
    case 6: return WEDGE;
    case 7: return PYRAMID;
    default: assert(0);
    }
}

void CubeMesh::read_gmsh(const char file_name[])
{

    // clear the mesh
    if(_pts) delete[] _pts;
    if(_elem_type) delete[] _elem_type;
    if(_conn) delete[] _conn;

    /*
     * open the file and write the data
     */
    FILE *fl = fopen(file_name, "r");
    assert(fl);

    // read the nodes
    char str[300];

    // header
    int n_read=0;
    n_read=fscanf(fl, "%s", str);assert(n_read!=0);
    assert(strcmp(str, "$NOD") == 0 );

    // node coordinates
    n_read=fscanf(fl, "%d", &_n_pts);assert(n_read!=0);
    _pts = new double[_n_pts][3];
    for (uint v=0 ; v < _n_pts ; v++)
    {
        uint gmsh_v;
		
        n_read=fscanf(fl, "%d %lf %lf %lf", &gmsh_v, _pts[v], _pts[v]+1, _pts[v]+2);assert(n_read!=0);
        assert(gmsh_v-1 == v);
    }

    // header
    n_read=fscanf(fl, "%s", str);assert(n_read!=0);
    assert(strcmp(str, "$ENDNOD") == 0 );
    n_read=fscanf(fl, "%s", str);assert(n_read!=0);
    assert(strcmp(str, "$ELM") == 0 );

    // elements 
    n_read=fscanf(fl, "%d", &_n_elem);assert(n_read!=0);
    const uint max_n_elem = _n_elem;
    _elem_type = new CellType[_n_elem];
    _conn = new uint[_n_elem][N_CELL2VERT_3D];

    uint index = 0;
    for (uint e=0 ; e < max_n_elem ; e++)
    {
        uint gmsh_e;
        uint gmsh_type;
        uint gmsh_n_vert;
        n_read=fscanf(fl, "%d %d %*d %*d %d", &gmsh_e, &gmsh_type, &gmsh_n_vert);assert(n_read!=0);
		
        CellType type = gmsh2cell(gmsh_type);
        CellInfo info(type);
		
        assert(gmsh_e - 1 == e);
        assert(info.n_verts() == gmsh_n_vert);

        _elem_type[index] = type;

        for(uint v = 0 ; v < info.n_verts() ; v++)
        {
            n_read=fscanf(fl, "%d", _conn[index]+v);assert(n_read!=0);
            _conn[index][v]--;
        }
		
        if( (type == TRI) || (type==QUAD) )
        {
            _n_elem = _n_elem - 1;
            continue;
        }
        if( (type != TET) )
        {
            printf("WARNING: NON TET \n");
            continue;
        }

	    
        index++;
    }
    assert(_n_elem == index);

    printf("n_ignored_bdry_elems: %d \n", max_n_elem - _n_elem);
	
    // close the file
    fclose(fl);
	
}


/******************************************************\
                           SHell
\******************************************************/

void ShellMesh::point_str2un(const uint ij[], uint &num)
{
    const uint i = ij[0];
    const uint j = ij[1];
	
    num = i + j * _n_tet;
}

void ShellMesh::point_un2str(const uint num, uint ij[])
{
    ij[0]= num % _n_tet;
    ij[1]= num / _n_tet;
}



void ShellMesh::build_mesh(const uint n_verts[], const double lengths[], const uint cell_type, double**)
{
    _r = lengths[0];
    _tet = lengths[1];
    if(fabs(_tet-2*M_PI) < 1e-10) _is_periodic = true;
    _phi = lengths[2];
    assert(_phi < M_PI/2.);

    _n_tet = n_verts[0];
    _n_phi = n_verts[1];

    // find number of points and elements
    _n_pts = _n_tet * _n_phi;
    const uint n_hex_tet = (_is_periodic ? _n_tet : _n_tet-1);
    const uint n_hex_phi = _n_phi-1;
    const uint n_hex = n_hex_tet * n_hex_phi;
    const uint max_n_elem = n_hex * 2;
    const double h_phi = _phi / n_hex_phi ;
    const double h_tet = _tet / n_hex_tet ;

    // debug
    //printf("%lf, %lf \n", h_tet, h_phi);


    // allocate memory
    if(_pts) delete[] _pts;
    if(_elem_type) delete[] _elem_type;
    if(_conn) delete[] _conn;
	
    _pts = new double[_n_pts][3];
    _elem_type = new CellType[max_n_elem];
    _conn = new uint[max_n_elem][N_CELL2VERT_3D];

    // create the mesh
    CellType ctype = (CellType) cell_type;
    _n_elem = 0;
	
    for(uint j=0 ; j < n_hex_phi ; j++)
        for(uint i=0 ; i < n_hex_tet ; i++)
        {
            const uint corner[]={i,j};
            build_elem_conn(corner, ctype);
        }

    // create the point locations
    for(uint j=0 ; j < _n_phi ; j++)
        for(uint i=0 ; i < _n_tet ; i++)
        {
            const uint corner[]={i,j};
            uint v;
            point_str2un(corner, v);
            _pts[v][0] = h_tet*i;
            _pts[v][1] = h_phi*j;
            //debug
            //printf("point(%d): %lf, %lf \n", v, _pts[v][0], _pts[v][1]);
        }
}

void ShellMesh::perturb_mesh(const double delta)
{

    const uint n_hex_tet = (_is_periodic ? _n_tet : _n_tet-1);
    const uint n_hex_phi = _n_phi-1;
    const double h_phi = _phi / n_hex_phi / 2.;
    const double h_tet = _tet / n_hex_tet / 2.;

	
    for(uint i=0 ; i < _n_tet ; i++)
        for(uint j=0 ; j < _n_phi ; j++)
        {
				
            const uint corner[]={i,j};
            uint v;
            point_str2un(corner, v);
			    
            const int sgn = (rand() % 2)*2 - 1 ;

            double dtet;
            if( !_is_periodic && ( (i==0) || (i==_n_tet-1) ) ) dtet=0;
            else dtet= ((double)rand()/(double)RAND_MAX) * delta * h_tet;

            double dphi;
            if( (j==0) || (j==_n_phi-1)) dphi = 0;
            else dphi = ((double)rand()/(double)RAND_MAX) * delta * h_phi;

            _pts[v][0] += sgn*dtet;
            _pts[v][1] += sgn*dphi;
        }

}


void ShellMesh::build_elem_conn(const uint as[], const CellType type)
{
    using namespace boost::assign;

    const uint bs[] = {(as[0]+1) % _n_tet , as[1]   };
    const uint cs[] = {(as[0]+1) % _n_tet , as[1]+1 };
    const uint ds[] = {as[0]              , as[1]+1 };

    uint a,b,c,d;
    point_str2un(as, a);
    point_str2un(bs, b);
    point_str2un(cs, c);
    point_str2un(ds, d);

    std::vector<std::vector<uint> > conn;
    std::vector<CellType> sub_types;
	
    switch(type)
    {
    case TRI:
    {

        conn.resize(2);
        sub_types += TRI, TRI;
        conn[0] += a,b,c;
        conn[1] += a,c,d;
        break;
    }
	
    case QUAD:
    {
		
        conn.resize(1);
        sub_types += QUAD;
        conn[0] += a,b,c,d;
        break;
				
    }
	
    default:
        assert("Not supported" && 0);
	
    }

    // create the elements
    for(uint i = 0 ; i < conn.size() ; i++)
    {
        for(uint j=0 ; j < conn[i].size() ; j++)
            _conn[_n_elem][j] = conn[i][j];
		
        _elem_type[_n_elem] = sub_types[i];
        _n_elem++;
    }
}

