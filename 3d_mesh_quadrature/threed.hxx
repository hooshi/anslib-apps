#ifndef THREED_HXX
#define THREED_HXX

#include <set>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <string>
#include <cstring>

typedef unsigned int uint;

#define N_FACE2VERT_3D 4
#define N_CELL2VERT_3D 8
#define N_CELL2FACE_3D 6

enum CellType{TET=0, HEX, WEDGE, PYRAMID, TRI, QUAD, EDGE};

/* -----------------------------------------------------------------------
 *  Utility Functions
 * ----------------------------------------------------------------------- */

void vec_union(const std::vector<std::vector<int> >& x, std::vector<int>& ans_v);
int vec_find(const std::vector<int>& x, const int val);

/* -----------------------------------------------------------------------
 *  Point
 * ----------------------------------------------------------------------- */

struct Point
{
private:
	double val[3];

public:

	Point(const double x=0, const double y=0, const double z=0)
		{
			val[0] = x;
			val[1] = y;
			val[2] = z;			
		}

	Point(const Point& p){set_equal(p);}

	void set_equal(const Point &x) 
		{
			val[0]=x.val[0];
			val[1]=x.val[1];
			val[2]=x.val[2];
		}

	Point& operator= (const Point& p)
		{
		set_equal(p);
		return *this;
		}

	Point operator-() const
		{
			Point p(*this);
			p.val[0]*=-1;
			p.val[1]*=-1;
			p.val[2]*=-1;
			return p;
		}
	
	double& operator()(const unsigned int i)
		{
			return val[i];
		}

	const double& operator()(const unsigned int i) const
		{
			return val[i];
		}

	const double* array() const
		{
			return val;
		}

};

/**
 * \brief Information about a Geometric Entity.
 * \note: Assumptions are:
 *        Each face is numbered so that right CV is pointed to by the thumb.
 *        Cells are numbered the same way as libMesh library.
 */
// TODO: fix wedge
class CellInfo
{
private:
	CellType _type;
	std::vector<int> _pts;


public:


	/* -----------------------------------------------------------------------
	 *  Constructor
	 * ----------------------------------------------------------------------- */

	// Create only for accessing the sizes
	CellInfo():_type(EDGE) {}
	CellInfo(CellType type_val);
	CellInfo(CellType type_in, const int points[]):
		_type(type_in),
		_pts(points, points+n_verts())
		{}
	CellInfo(CellType type_in, const uint points[]):
		_type(type_in),
		_pts(points, points+n_verts())
		{}


	// default copy assignment must be available
	// ...
	
	// Create by giving faces
	// This is used to create the three-dimensional objects
	CellInfo(const uint nfaces, const bool leftcv[], const uint nfverts[], const uint verts[][N_FACE2VERT_3D]);
	    
	/* -----------------------------------------------------------------------
	 *  Innate properties
	 * ----------------------------------------------------------------------- */

	static CellType type_from_n_vertex_and_dim(const uint, const uint);
	
	CellType type() const;
  	uint n_verts() const;
	uint n_faces() const;
	uint n_edges() const;
	uint dim() const;
	std::string name() const;
	const std::vector<int>& points() const {return _pts;}
	std::vector<int>& points() {return _pts;}

	void sides(std::vector<CellInfo>&) const;

	/* -----------------------------------------------------------------------
	 *  Debug
	 * ----------------------------------------------------------------------- */

	void print(bool char_names = false) const;
	
	// Output must be
	// CellInfo type: TET
	// Verts: b c a d
	// CellInfo type: WEDGE
	// Verts: a e f c b d
	// CellInfo type: HEX
	// Verts: g b d f h c a e
	// CellInfo type: PYRAMID
	// Verts: d a c b e
	static void test();
};


/**
 * Lagrange shape functions
 */
class FELagrange
{

	CellType _type;

public:

	FELagrange(CellType type_in):_type(type_in){}
	CellType type() const {return _type;}

	template<typename T>
	void basis (const T pxi[], T phi[]) const
		{

			const T& xi = pxi[0];
			const T& eta = pxi[1];
			const T& zeta = pxi[2];
			const double eps = 1e-35;

			switch(type() )
			{
			case TRI:
				phi[0] = 1 - xi - eta;
				phi[1] = xi;
				phi[2] = eta;
				break;
			case QUAD:
				phi[0] = .25*(1-xi)*(1-eta);
				phi[1] = .25*(1+xi)*(1-eta);
				phi[2] = .25*(1+xi)*(1+eta);
				phi[3] = .25*(1-xi)*(1+eta);
				break;
			case HEX:
				phi[0] = .125*(1-xi)*(1-eta)*(1-zeta);
				phi[1] = .125*(1+xi)*(1-eta)*(1-zeta);
				phi[2] = .125*(1+xi)*(1+eta)*(1-zeta);
				phi[3] = .125*(1-xi)*(1+eta)*(1-zeta);
				phi[4] = .125*(1-xi)*(1-eta)*(1+zeta);
				phi[5] = .125*(1+xi)*(1-eta)*(1+zeta);
				phi[6] = .125*(1+xi)*(1+eta)*(1+zeta);
				phi[7] = .125*(1-xi)*(1+eta)*(1+zeta);
				break;
			case TET:
				phi[0]=(1-xi-zeta-eta);
				phi[1] = xi;
				phi[2] = eta;
				phi[3] = zeta;
				break;
			case WEDGE:
				phi[0]=0.5*(1-zeta)*(1-xi-eta);
				phi[1]=0.5*(1-zeta)*(xi);
				phi[2]=0.5*(1-zeta)*(eta);
				phi[3]=0.5*(1+zeta)*(1-xi-eta);
				phi[4]=0.5*(1+zeta)*(xi);
				phi[5]=0.5*(1+zeta)*(eta);
				break;
			case PYRAMID:
				phi[0]=.25*(zeta + xi - 1.)*(zeta + eta - 1.)/((1. - zeta) + eps);
				phi[1]=.25*(zeta - xi - 1.)*(zeta + eta - 1.)/((1. - zeta) + eps);
				phi[2]=.25*(zeta - xi - 1.)*(zeta - eta - 1.)/((1. - zeta) + eps);
				phi[3]=.25*(zeta + xi - 1.)*(zeta - eta - 1.)/((1. - zeta) + eps);
				phi[4]=zeta;
				break;
			default:
				assert("phi() not supported for this type!" && 0);
			}
		}

	void basis_deriv(const double pxi[], double dphi[][3]) const;

	void ref_pts(double pxi[][3]) const;
	void ref_centroid(double pxi[]) const;

	double ref_gen_volume() const;
	static void test_lagrange_property();
};

class QGenerator
{
	std::vector<Point> _points;
	std::vector<double> _weights;
	
	CellType _type;
	unsigned int _order;

	// scale in the 1D case
	// assumes old range is [-1 1]
	void scale(const double new_range[]);

	// quadrature rules for triangle
	void dunavant_rule2(const double * wts,
						const double * a,
						const double * b,
						const unsigned int * permutation_ids,
						const unsigned int n_wts);

	// quadrature rule for quad
	void tensor_product_quad();
	
	// quadrature rule for tet
	void conical_rule_tet();

	// quadrature rule for prism
	void tensor_product_prism();

	// quadrature rule for pyramid
	void conical_rule_pyramid();	

	// quadrature rule for hex
	void tensor_product_hex();



public:

	uint n_points();
	const Point&  qp(const int i) const;
	double w(const int i) const;


	void init(CellType type, const unsigned int order);
	void init_1d_jacobi(const int a);	
	void init_1d_gauss();
	void init_2d_gauss();
	void init_3d_gauss();
};

/** Maps a reference element to the physical space.
 */
class FEMap
{
	CellType _type;
	double  _ref_pts[N_CELL2VERT_3D][3];

	double _jac;
	double _normal[3];
public:

	FEMap(CellType type,  const double ref_pts[][3]):
		_type(type),
		_jac(-1)
		{
			_normal[0]=_normal[1]=_normal[2]=0;
			memcpy(_ref_pts, ref_pts, sizeof(double)*3*CellInfo(_type).n_verts());
		}

	void reinit(CellType type,  const double ref_pts[][3])
		{
			_type = type;
			_jac = -1;
			_normal[0]=_normal[1]=_normal[2]=0;
			memcpy(_ref_pts, ref_pts, sizeof(double)*3*CellInfo(_type).n_verts());
		}

	void map_to_physical(const double xi[], double p[], double normal[], double &jac);
};

#endif /* THREED_HXX */
