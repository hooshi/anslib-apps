#include "threed.hxx"
#include "mesh.hxx"

int main()
{
	
	CellInfo::test();

	FELagrange::test_lagrange_property();
	
	{
		FELagrange tri(PYRAMID);
		double pxi[]={0,0,0};
		double dphi[8][3];
	
		tri.basis_deriv(pxi, dphi);

		printf("%5lf %5lf %5lf \n", dphi[0][0], dphi[0][1], dphi[0][2]);
		printf("%5lf %5lf %5lf \n", dphi[1][0], dphi[1][1], dphi[1][2]);
		printf("%5lf %5lf %5lf \n", dphi[2][0], dphi[2][1], dphi[2][2]);
		printf("%5lf %5lf %5lf \n", dphi[3][0], dphi[3][1], dphi[3][2]);
	}

	CubeMesh mesh;
	uint n_vert[] = {2,2,2};
	double lengths[] = {1,1,1};
	mesh.build_mesh(n_vert, lengths, (int)TET); mesh.perturb_mesh(0.6); mesh.write_vtk("tet.vtk");
	mesh.build_mesh(n_vert, lengths, (int)HEX); mesh.perturb_mesh(0.6);  mesh.write_vtk("hex.vtk");
	mesh.build_mesh(n_vert, lengths, (int)PYRAMID); mesh.perturb_mesh(0.6);  mesh.write_vtk("pyramid.vtk");
	mesh.build_mesh(n_vert, lengths, (int)WEDGE); mesh.perturb_mesh(0.6);  mesh.write_vtk("wedge.vtk");
	
	return 0;
}
