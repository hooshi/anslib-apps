#include "mesh.hxx"
#include <cmath>
#include <string>
#include <cstring>
#include <vector>
#include <cassert>

#include "GetPot"

// TET=0, HEX, WEDGE, PYRAMID


int bdry_tag1(const double p[])
{
	return 1;
}

int bdry_tag_unitsphere(const double p[])
{
    const double len2 = p[0]*p[0]+p[1]*p[1]+p[2]*p[2];
    const double len = sqrt(len2);
    if (len < 1.5) return 1;
    else if(len > 95) return 5;
    else
    {
        static int i = 0;
        printf("Error Face Alone: %d %lf %lf %lf \n", i, p[0], p[1], p[2]);
        i++;
        return -1000000;
    }
        
}

int main(int argc, char *argv[])
{

	GetPot getpot(argc, argv);
	
	CubeMesh mesh;
	std::string in = getpot("in", "mesh");
	std::string out = getpot("out", "mesh");

			
	mesh.read_gmsh((in+".msh").c_str());
	mesh.write_grummp((out+".vmesh").c_str(), bdry_tag_unitsphere);
	mesh.write_vtk((out+".vtk").c_str());
	mesh.check_orientation();
	
	return 0;
}
