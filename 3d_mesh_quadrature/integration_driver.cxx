#include "mesh.hxx"
#include <cmath>
#include <string>
#include <cstring>

double f(const double r[], const double n[] = NULL)
{
	const double x = r[0];
	const double y = r[1];
	const double z = r[2];

	//return exp(x)*exp(y)*exp(z);
	//return sinh(x)*cos(y)*sin(z);

	return 1;
}

double exact()
{
	//return pow(exp(1)-1,3);
	//return (cosh(1)-1)*sin(1)*(1-cos(1));
	return 1;
}

double integrate(const Mesh &mesh, const int order = 2)
{
	assert(mesh.get_n_elem() > 0);
	
	double ans = 0;
	
	for (uint el=0 ; el < mesh.get_n_elem() ; el++)
	{
		const CellType tp = mesh.get_elem_type(el);
		CellInfo cell(tp);
		double r[CELL2VERT][3]; mesh.get_elem_coords(el, r);

		QGenerator q;
		q.init(tp, order);
		if(cell.dim() == 3)
			q.init_3d_gauss();
		else if(cell.dim() == 2)
			q.init_2d_gauss();
		else
			assert(0);
			
		FEMap fe_map(tp, r);

		for(uint qp=0 ; qp < q.n_points() ; qp++)
		{
			double jac;
			double xyz[3];
			double normal[3];

			
			fe_map.map_to_physical(q.qp(qp).array(), xyz, normal, jac);
			ans += jac * q.w(qp) * f(xyz, normal);


			if(el == 20 )
			{
				FELagrange fe(tp);
				// in case of 2d mesh check the normal is correct
				if( cell.dim() == 2)
				{
					printf("n: %e, %e , %e  j: %lf\n",  normal[0], normal[1], normal[2], jac*fe.ref_gen_volume());
					//printf("%lf, %lf , %lf \n", xyz[0], xyz[1], xyz[2]);
					//printf("%lf, %lf , %lf \n\n",
					//	   fabs(xyz[0]-normal[0]),
					//	   fabs(xyz[1]-normal[1]),
					//	   fabs(xyz[2]-normal[2]));
				}
				//printf("%lf %lf %lf -> %lf \n", q.qp(qp)(0), q.qp(qp)(1), q.qp(qp)(2), q.w(qp));
				  //printf("%lf %lf %lf -> %.20lf  \n", fabs(xyz[0]-z[qp][0]), fabs(xyz[1]-z[qp][1]), fabs(xyz[2]-z[qp][2]), fabs(jac-dSize));

				  //ans += dSize/24. * f(z[qp]);

			 	// double dphi_dxi[8][3];
				// FELagrange fe(tp);
				// fe.basis_deriv(q.qp(qp).array(), dphi_dxi);

				// CellInfo cell(tp);
				// for(uint v=0 ; v<cell.n_verts() ; v++)
				// 	 printf("%lf %lf %lf\n", dphi_dxi[v][0], dphi_dxi[v][1], dphi_dxi[v][2]);
				// printf("\n");
			}
		}

	}

	return ans;
	
}

int main()
{
	// test 3d integration
	if(0)
	{
		double pre=-1;
		uint n = 2;
		CubeMesh mesh;
		const double l[] = {1,1,1};

		for (uint i = 0 ; i < 4 ; i++)
		{
			const uint sizes[] ={n,n,n};
			mesh.build_mesh(sizes, l, HEX);
			mesh.perturb_mesh(0.5);


			char name[200];
			sprintf(name, "mesh_%d.vtk", i);
			mesh.write_vtk(name );
		
			const double int_e = exact();
			const double int_a = integrate(mesh,4);
			printf("%e ", fabs(int_e - int_a) );
			if(pre>-1) printf("%.2lf \n", log(pre/fabs(int_e - int_a)) );
			else printf("\n");
			pre=fabs(int_e - int_a);
		
			n *= 2;
		}
	}

	// test 2d mesh creation
	{
		ShellMesh mesh;
		const uint n[] = {20,20};
		const double l[]= {1, 1 , 1};
		const uint ord=2;
	
		mesh.build_mesh(n, l, TRI);
		mesh.perturb_mesh(0.7);
		mesh.write_vtk("shell_0.vtk");
		
		integrate(mesh,ord);
		printf("-------------------------- \n");
	
		mesh.build_mesh(n, l, QUAD);
		mesh.perturb_mesh(0.7);
		mesh.write_vtk("shell_1.vtk");

		// for(uint v=0 ; v < mesh.get_n_vert() ; v++)
		// {
		// 	double pt[3];
		// 	mesh.get_point_coords(v,pt);
		// 	const double r = pt[0]*pt[0]+pt[ord]*pt[1]+pt[2]*pt[2];
		// 	assert(fabs(r-1)<1e-10);
		// }
		
		integrate(mesh,ord);
	}

	
	
	return 0;
}
