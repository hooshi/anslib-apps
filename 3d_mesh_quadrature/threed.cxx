#include "threed.hxx"
#include "ANS_dual.h"
#include <boost/assign/std/vector.hpp>

#include <cmath>



/******************************************************************\
                       UTILITY FUNCTIONS
\******************************************************************/

void vec_union(const std::vector<std::vector<int> >& x, std::vector<int>& ans_v)
{
	std::set<int> ans;
	ans_v.clear();

	for (std::vector<std::vector<int> >::const_iterator itv = x.begin() ; itv != x.end() ; ++itv)
	{
		for(std::vector<int>::const_iterator it = itv->begin() ; it != itv->end() ; ++it)
		{
			if (ans.count(*it) == 0)
			{
				ans.insert(*it);
				ans_v.push_back(*it);
			}
		}
	}
}

int vec_find(const std::vector<int>& x, const int val)
{
	unsigned int i;

	for(i=0 ; i<x.size(); i++)
	{
		if (x[i] == val)
			return i;
	}
	return -1;
}

/******************************************************************\
                       CellInfo
\******************************************************************/

/* ---------------------------------------*\
 *  Constructors
 * ---------------------------------------*/

// Create only for accessing the sizes
CellInfo::CellInfo(CellType type_in):_type(type_in)
{
	// for(uint i = 0 ; i < n_verts() ; i++)
	// {
	// 	_pts.push_back(i);
	// }
}

// Create by giving faces
// This is used to create the three-dimensional objects
// currently only works for 3D cells
CellInfo::CellInfo
(const uint nfaces, const bool leftcv[], const uint nfverts[], const uint verts[][N_FACE2VERT_3D])
{
	/*
	  create face2vert connectivity
	*/
	std::vector<std::vector<int> > face2vert;
	face2vert.resize(nfaces);
	for (uint jf = 0 ; jf < nfaces ; jf++)
	{
		face2vert[jf].resize(nfverts[jf]);
		for (uint jv = 0 ; jv < nfverts[jf] ; jv++)
		{
			const uint address = (leftcv[jf] ? jv : nfverts[jf]-jv-1);
			face2vert[jf][address] = verts[jf][jv];
		}
	}


	/*
	  find the verts
	*/
	std::vector<int> pts_unorder;
	vec_union(face2vert, pts_unorder);
	const unsigned int nverts = pts_unorder.size();


	/*
	  find the cell type
	*/
	_type = type_from_n_vertex_and_dim(nverts, 3);

	/*
	  put the vertices in order
	*/
	_pts.resize(0);

	switch(_type)
	{

	case TET:
	{
		// choose an arbitrary face and put it into the answer
		// in the reverse order
		_pts.push_back(face2vert[0][2]);
		_pts.push_back(face2vert[0][1]);
		_pts.push_back(face2vert[0][0]);

		// find the other remaining point
		for (uint jv = 0 ; jv < nverts ; jv++)
		{
			if (std::find(_pts.begin(), _pts.end(), pts_unorder[jv]) == _pts.end() )
			{
				_pts.push_back(pts_unorder[jv]);
			}
		}
		break;
	}

	// it is possible to merge the code for WEDGE and HEX into one
	// block of code
	case WEDGE:
	{
		// add the first triangular face in reverse
		uint ftri = 0;
		{
			for(; ftri < face2vert.size() ; ftri++)
			{
				if (face2vert[ftri].size() == 3) break;
			}
			assert(ftri < face2vert.size());

			const std::vector<int> &ftri_v = face2vert[ftri];
			_pts.push_back(ftri_v[2]);
			_pts.push_back(ftri_v[1]);
			_pts.push_back(ftri_v[0]);
		}

		// find vertices located at the top of the first ones
		for(int basev = 0 ; basev<3 ; basev++)
		{
			const int v0 = _pts[basev];
			const int v1 = _pts[(basev+1) % 3];
			// debug
			//fprintf(stderr, "v0: %c, v1: %c \n", v0 , v1);

			// find the face that has v0 and v1
			for (uint f2=0 ; f2<nfaces ; f2++)
			{
				// do not process the ftri face
				if(f2 == ftri) continue;

				const std::vector<int>& f2v = face2vert[f2];

				// std::find v0 and v1 in f2
				const int v0f = vec_find(f2v, v0);
				const int v1f = vec_find(f2v, v1);

				// find the next vertex
				if( (v0f >= 0) && (v1f >= 0) )
				{
					assert( (v0f + 1) % 4 == v1f );
					assert( f2v.size() == 4 );
					//debug
					//fprintf(stderr, "f2: %d v0f: %d, v1f: %d \n", f2, v0f , v1f);

					const int vnext = f2v[(v0f+3) % 4];
					_pts.push_back( vnext );
				}
				// end of for over side face
			}
			// end of for over base verts
		}
		// end of case wedge
		break;
	}

	case HEX:
	{

		// add the first face as the base
		// must add points in reverse order
		_pts.push_back(face2vert[0][3]);
		_pts.push_back(face2vert[0][2]);
		_pts.push_back(face2vert[0][1]);
		_pts.push_back(face2vert[0][0]);

		// find vertices located at the top of the first ones
		for(int basev = 0 ; basev<4 ; basev++)
		{
			const int v0 = _pts[basev];
			const int v1 = _pts[(basev+1) % 4];
			// debug
			//fprintf(stderr, "v0: %c, v1: %c \n", v0 , v1);

			// find the face that has v0 and v1
			// do not process the base face
			for (uint f2=1 ; f2<nfaces ; f2++)
			{
				const std::vector<int>& f2v = face2vert[f2];

				// find v0 and v1 in f2
				const int v0f = vec_find(f2v, v0);
				const int v1f = vec_find(f2v, v1);

				// find the next vertex
				if( (v0f>=0) && (v1f >= 0) )
				{
					assert( (v0f + 1) % 4 == v1f );
					//debug
					//fprintf(stderr, "f2: %d v0f: %d, v1f: %d \n", f2, v0f , v1f);

					const int vnext = f2v[(v0f+3) % 4];
					_pts.push_back( vnext );
				}
				// end of for over side face
			}
			// end of for over base verts
		}
		//end of case hex
		break;
	}

	case PYRAMID:
	{
		// add the first quad face in reverse order
		uint fquad = 0;
		{
			for(; fquad < face2vert.size() ; fquad++)
			{
				if (face2vert[fquad].size() == 4) break;
			}
			assert(fquad < face2vert.size());

			const std::vector<int> &fquad_v = face2vert[fquad];
			_pts.push_back(fquad_v[3]);
			_pts.push_back(fquad_v[2]);
			_pts.push_back(fquad_v[1]);
			_pts.push_back(fquad_v[0]);
		}

		// find the other remaining point
		for (uint jv = 0 ; jv < nverts ; jv++)
		{
			if (std::find(_pts.begin(), _pts.end(), pts_unorder[jv]) == _pts.end() )
			{
				_pts.push_back(pts_unorder[jv]);
			}
		}

		
		break;
	} //end of case pyramid

	default:
		assert(0);
	} 	// end of switch to find verts



	// make sure all points are found
	assert( (_pts.size() == n_verts()) && "-> Could not find all verts");

	// all done
}

/* ---------------------------------------*\
 *  Innate properties
 * ---------------------------------------*/

CellType CellInfo::type_from_n_vertex_and_dim(const uint nvert_in, const uint dim_in)
{
	switch(dim_in)
	{
		
	case 2:
		switch(nvert_in)
		{
		case 3:
			return TRI;
		case 4:
			return QUAD;
		default:
			assert(0);
		}
		
	case 3:		
		switch(nvert_in)
		{
		case 4:
			return TET;
		case 5:
			return PYRAMID;
		case 6:
			return WEDGE;
		case 8:
			return HEX;
		default:
			assert(0);
		}

	default:
		assert(0);
	}
	
}

CellType CellInfo::type() const {return _type;}

uint CellInfo::n_verts() const
{
	switch(type())
	{
	case EDGE:
		return 2;
	case TRI:
		return 3;
	case QUAD:
		return 4;
	case TET:
		return 4;
	case HEX:
		return 8;
	case WEDGE:
		return 6;
	case PYRAMID:
		return 5;
	default:
		fprintf(stderr, "bad_type: %d \n", type());
		assert("n_verts() not supported for this type" && 0);
	}
}

uint CellInfo::n_faces() const
{
	switch(type())
	{
	case TET:
		return 4;
	case HEX:
		return 6;
	case WEDGE:
		return 5;
	case PYRAMID:
		return 5;
	default:
		assert("n_faces() not supported for this type" && 0);

	}
}

uint CellInfo::n_edges() const
{
	return n_verts()+n_faces()-2;
}

uint CellInfo::dim() const
{
	switch(type())
	{
	case EDGE:
		return 1;
	case TRI:
	case QUAD:
		return 2;
	case TET:
	case HEX:
	case PYRAMID:
	case WEDGE:
		return 3;
	default:
		assert(0);
	}

	return 0;
}
std::string CellInfo::name() const
{
	switch(type())
	{
	case TRI:
		return "TRI";
	case QUAD:
		return "QUAD";
	case TET:
		return "TET";
	case HEX:
		return "HEX";
	case WEDGE:
		return "WEDGE";
	case PYRAMID:
		return "PYRAMID";
	default:
		assert("name() not supported for this type" && 0);
	}
}

void CellInfo::sides(std::vector<CellInfo>& sd) const
{
	
	using namespace boost::assign;

#define a _pts[0]
#define b _pts[1]
#define c _pts[2]
#define d _pts[3]
#define e _pts[4]
#define f _pts[5]
#define g _pts[6]
#define h _pts[7]

	
	switch(type())
	{
		
	case TET:
		
		sd.resize(4);
		sd[0]._type = TRI; sd[0]._pts += b,a,c;
		sd[1]._type = TRI; sd[1]._pts += a,d,c;
		sd[2]._type = TRI; sd[2]._pts += b,c,d;
		sd[3]._type = TRI; sd[3]._pts += b,d,a;
		return;
		
	case HEX:
		
		sd.resize(6);
		sd[0]._type = QUAD; sd[0]._pts += b,a,d,c;
		sd[1]._type = QUAD; sd[1]._pts += b,c,g,f;
		sd[2]._type = QUAD; sd[2]._pts += b,f,e,a;
		sd[3]._type = QUAD; sd[3]._pts += a,e,h,d;
		sd[4]._type = QUAD; sd[4]._pts += c,d,h,g;
		sd[5]._type = QUAD; sd[5]._pts += e,f,g,h;
		return ;
		
	case WEDGE:
		
		sd.resize(5);
		sd[0]._type = TRI; sd[0]._pts += b,a,c;
		sd[1]._type = TRI; sd[1]._pts += d,e,f;
		sd[2]._type = QUAD; sd[2]._pts += a,b,e,d;
		sd[3]._type = QUAD; sd[3]._pts += b,c,f,e;
		sd[4]._type = QUAD; sd[4]._pts += a,d,f,c;
		return;
		
	case PYRAMID:
		
		sd.resize(5);
		sd[0]._type = QUAD; sd[0]._pts += b,a,d,c;
		sd[1]._type = TRI; sd[1]._pts +=  b,c,e;
		sd[2]._type = TRI; sd[2]._pts +=  a,b,e;
		sd[3]._type = TRI; sd[3]._pts +=  a,e,d;
		sd[4]._type = TRI; sd[4]._pts +=  c,d,e;
		return;
		
	case TRI:
	case QUAD:
	default:
		
		assert("name() not supported for this type" && 0);
	}

#undef a
#undef b
#undef c
#undef d
#undef e
#undef f
#undef g
#undef h

}



/* ---------------------------------------*\
 *  Debug
 * ---------------------------------------*/

void CellInfo::print(bool char_names) const
{
	printf("CellInfo type: %s \n", name().c_str());
	printf("Verts: ");
	for (std::vector<int>::const_iterator it = _pts.begin() ; it != _pts.end() ; ++it)
	{
		if(char_names) printf("%c ", *it);
		else printf("%d ", *it);
	}
	printf("\n");
}

// Output must be
// CellInfo type: TET
// Verts: b c a d
// CellInfo type: WEDGE
// Verts: f e a d b c
// CellInfo type: HEX
// Verts: g b d f h c a e
// CellInfo type: PYRAMID
// Verts: d a c b e
void CellInfo::test()
{
	const int a=(int)'a';
	const int b=(int)'b';
	const int c=(int)'c';
	const int d=(int)'d';
	const int e=(int)'e';
	const int f=(int)'f';
	const int g=(int)'g';
	const int h=(int)'h';


	// Test a tet
	{
		const uint nfaces=4;
		const bool leftcv[]={true, true, false, false};
		const uint nfverts[]={3,3,3,3};
		const uint verts[][N_FACE2VERT_3D] =
			{
				{a,c,b,1},
				{c,d,b,1},
				{a,d,b,1},
				{a,c,d,1}
			};

		CellInfo gent(nfaces,  leftcv,  nfverts, verts);
		gent.print(true);
	}

	// Test a wedge
	{
		const uint nfaces=5;
		const bool leftcv[]={false, true, true, true, false};
		const uint nfverts[]={4,4,3,4,3};
		const uint verts[][N_FACE2VERT_3D] =
			{
				{f,d,b,e},
				{f,d,c,a},
				{a,e,f,1},
				{e,a,c,b},
				{c,b,d,1}
			};

		CellInfo gent(nfaces,  leftcv,  nfverts, verts);
		gent.print(true);
	}


	// Test a hex
	{
		const uint nfaces=6;
		const bool leftcv[]={true, false, false, false, true, false};
		const uint nfverts[]={4,4,4,4,4,4};
		const uint verts[][N_FACE2VERT_3D] =
			{
				{f,d,b,g},
				{f,d,a,e},
				{h,g,f,e},
				{c,h,e,a},
				{c,b,d,a},
				{g,h,c,b}
			};

		CellInfo gent(nfaces,  leftcv,  nfverts, verts);
		gent.print(true);
	}

	//Test a pyramid
	{
		const uint nfaces=5;
		const bool leftcv[]={true, true, false, false, true};
		const uint nfverts[] = {3,4,3,3,3};
		const uint verts[][N_FACE2VERT_3D] =
			{
				{d,a,e,1},
				{b,c,a,d},
				{b,c,e,1},
				{e,c,a,1},
				{b,d,e,1},
			};

		CellInfo gent(nfaces,  leftcv,  nfverts, verts);
		gent.print(true);
	}
	// end of test
}


/******************************************************************\
                       LAGRANGE 3D
\******************************************************************/

void FELagrange::basis_deriv(const double pxi[], double dphi[][3]) const
{
	DualNumber<3> pxi_dual[3];
	pxi_dual[0].set(0, 1);
	pxi_dual[1].set(1, 1);
	pxi_dual[2].set(2, 1);
	pxi_dual[0]() = pxi[0];
	pxi_dual[1]() = pxi[1];
	pxi_dual[2]() = pxi[2];


	
	DualNumber<3> phi_dual[N_CELL2VERT_3D];
	basis(pxi_dual, phi_dual);

	CellInfo cell(type());
	for(uint v=0 ; v < cell.n_verts() ; v++)
	{
		dphi[v][0] = phi_dual[v].get(0);
		dphi[v][1] = phi_dual[v].get(1);
		dphi[v][2] = phi_dual[v].get(2);
	}
}

void FELagrange::ref_pts(double pxi[][3]) const
{
	switch(type())
	{
	case TRI:
		pxi[0][0]=0; pxi[0][1]=0; pxi[0][2]=0;
		pxi[1][0]=1; pxi[1][1]=0; pxi[1][2]=0;
		pxi[2][0]=0; pxi[2][1]=1; pxi[2][2]=0;
		break;
	case QUAD:
		pxi[0][0]=-1; pxi[0][1]=-1; pxi[0][2]=0;
		pxi[1][0]=1; pxi[1][1]=-1; pxi[1][2]=0;
		pxi[2][0]=1; pxi[2][1]=1; pxi[2][2]=0;
		pxi[3][0]=-1; pxi[3][1]=1; pxi[3][2]=0;
		break;
	case TET:
		pxi[0][0]=0; pxi[0][1]=0; pxi[0][2]=0;
		pxi[1][0]=1; pxi[1][1]=0; pxi[1][2]=0;
		pxi[2][0]=0; pxi[2][1]=1; pxi[2][2]=0;
		pxi[3][0]=0; pxi[3][1]=0; pxi[3][2]=1;
	break;
	case HEX:
		pxi[0][0]=-1; pxi[0][1]=-1; pxi[0][2]=-1;
		pxi[1][0]=1; pxi[1][1]=-1; pxi[1][2]=-1;
		pxi[2][0]=1; pxi[2][1]=1; pxi[2][2]=-1;
		pxi[3][0]=-1; pxi[3][1]=1; pxi[3][2]=-1;

		pxi[4][0]=-1; pxi[4][1]=-1; pxi[4][2]=1;
		pxi[5][0]=1; pxi[5][1]=-1; pxi[5][2]=1;
		pxi[6][0]=1; pxi[6][1]=1; pxi[6][2]=1;
		pxi[7][0]=-1; pxi[7][1]=1; pxi[7][2]=1;
		break;
	case WEDGE:
		pxi[0][0]=0; pxi[0][1]=0; pxi[0][2]=-1;
		pxi[1][0]=1; pxi[1][1]=0; pxi[1][2]=-1;
		pxi[2][0]=0; pxi[2][1]=1; pxi[2][2]=-1;

		pxi[3][0]=0; pxi[3][1]=0; pxi[3][2]=1;
		pxi[4][0]=1; pxi[4][1]=0; pxi[4][2]=1;
		pxi[5][0]=0; pxi[5][1]=1; pxi[5][2]=1;
		break;
	case PYRAMID:
		pxi[0][0]=-1; pxi[0][1]=-1; pxi[0][2]=0;
		pxi[1][0]=1; pxi[1][1]=-1; pxi[1][2]=0;
		pxi[2][0]=1; pxi[2][1]=1; pxi[2][2]=0;
		pxi[3][0]=-1; pxi[3][1]=1; pxi[3][2]=0;
		pxi[4][0]=0; pxi[4][1]=0; pxi[4][2]=1;
		break;
	default:
		assert("ref_pts() not supported for this type" && 0);
	}
}

void FELagrange::ref_centroid(double pxi[]) const 
{
	switch(_type)
	{
	case TRI:
	case WEDGE:
		pxi[0]= 1/3.; pxi[1]= 1/3.; pxi[2]= 0;
		return;
	case QUAD:
	case HEX:
		pxi[0]= 0; pxi[1]= 0; pxi[2]= 0;
		return;
	case TET:
		pxi[0]= 1/4.; pxi[1]= 1/4.; pxi[2]=1/4.;
		return;
	case PYRAMID:
		pxi[0]= 0; pxi[1]= 0; pxi[2]=1/4.;
		return;
	default:
		assert("ref_center() not supported for this type" && 0);
	}

}

double FELagrange::ref_gen_volume() const
{
	switch(_type)
	{
	case EDGE:
		return 2;
	case TRI:
		return 1/2.;
	case QUAD:
		return 4;
	case HEX:
		return 8;
	case TET:
		return 1/6.;
	case PYRAMID:
		return 4/3.;
	case WEDGE:
		return 1;
	default:
		assert("ref_gen_volume() not supported for this type" && 0);
	}
	
	return 0;
}

void FELagrange::test_lagrange_property()
{
	const double tol = 1e-35;
	
	for(int i=(int)TET ; i <= (int)QUAD ; i++)
	{

		CellInfo cell( (CellType)i );
		FELagrange fe( (CellType)i );		
		double pxi[8][3];
		
		printf("testing %s \n", cell.name().c_str());
		
		fe.ref_pts(pxi);		
   		for(uint v= 0 ; v < cell.n_verts() ; v++)
		{
			double phi[8];
			fe.basis(pxi[v], phi);
			
			for(uint iphi= 0 ; iphi < cell.n_verts() ; iphi++)
			{
				assert( fabs( phi[iphi] - (v==iphi ? 1 : 0) ) < tol);
			}
		}
		
	}	
}

/******************************************************************\
                            FEMAP
\******************************************************************/

void FEMap::map_to_physical(const double xi[], double p[], double normal[], double &jac)
{
	
	CellInfo cell(_type);
	FELagrange fe(_type);

	double phi[N_CELL2VERT_3D];
	fe.basis(xi, phi);

	// find the physical point
	p[0]=p[1]=p[2]=0;
	for (uint s = 0 ; s < cell.n_verts() ; s++)
	{
		p[0]+= _ref_pts[s][0] * phi[s];
		p[1]+= _ref_pts[s][1] * phi[s];
		p[2]+= _ref_pts[s][2] * phi[s];
	}

	// find the jacobian
	// for a T
	if(!( ((_type==TRI) || (_type==TET)) && (_jac > 0) ) )
	{
	    double dphi_dxi[N_CELL2VERT_3D][3];
		fe.basis_deriv(xi, dphi_dxi);

		

		double dx_dxi = 0, dx_deta = 0 , dx_dzeta=0,
			dy_dxi = 0, dy_deta = 0 , dy_dzeta=0,
			dz_dxi = 0, dz_deta = 0 , dz_dzeta=0;
		
		for (uint s = 0 ; s < cell.n_verts() ; s++)
		{
			enum physical_space{X=0,Y,Z};
			enum ref_space{XI=0,ETA,ZETA};
			
			dx_dxi += _ref_pts[s][X] * dphi_dxi[s][XI];
			dx_deta += _ref_pts[s][X] * dphi_dxi[s][ETA];
			dx_dzeta += _ref_pts[s][X] * dphi_dxi[s][ZETA];

			dy_dxi += _ref_pts[s][Y] * dphi_dxi[s][XI];
			dy_deta += _ref_pts[s][Y] * dphi_dxi[s][ETA];
			dy_dzeta += _ref_pts[s][Y] * dphi_dxi[s][ZETA];

			dz_dxi += _ref_pts[s][Z] * dphi_dxi[s][XI];
			dz_deta += _ref_pts[s][Z] * dphi_dxi[s][ETA];
			dz_dzeta += _ref_pts[s][Z] * dphi_dxi[s][ZETA];
		}

		if(cell.dim() == 2)
		{
			const double g11 = (dx_dxi*dx_dxi +
							  dy_dxi*dy_dxi +
							  dz_dxi*dz_dxi);

			const double g12 = (dx_dxi*dx_deta +
							  dy_dxi*dy_deta +
							  dz_dxi*dz_deta);

			const double g21 = g12;

			const double g22 = (dx_deta*dx_deta +
							  dy_deta*dy_deta +
							  dz_deta*dz_deta);

			const double det = (g11*g22 - g12*g21);
			_jac = sqrt( det );
			_normal[0]= (dy_dxi*dz_deta - dz_dxi*dy_deta) /_jac;
			_normal[1]= (dz_dxi*dx_deta - dx_dxi*dz_deta) /_jac;
			_normal[2]= (dx_dxi*dy_deta - dy_dxi*dx_deta) /_jac;

		}
		else if(cell.dim() == 3)
		{
			_jac = (dx_dxi*(dy_deta*dz_dzeta - dz_deta*dy_dzeta)  +
					dy_dxi*(dz_deta*dx_dzeta - dx_deta*dz_dzeta)  +
					dz_dxi*(dx_deta*dy_dzeta - dy_deta*dx_dzeta));
		}
		else
		{
			assert(0);
		}

		// debug
		// printf("%lf  %lf  %lf \n", dx_dxi, dx_deta , dx_dzeta);
		// printf("%lf  %lf  %lf \n", dy_dxi, dy_deta , dy_dzeta);
		// printf("%lf  %lf  %lf \n\n", dz_dxi, dz_deta , dz_dzeta);

		// for(uint v=0 ; v<cell.n_verts() ; v++)
		// 	printf("%lf %lf %lf\n", dphi_dxi[v][0], dphi_dxi[v][1], dphi_dxi[v][2]);

		// for(uint v=0 ; v<cell.n_verts() ; v++)
		// 	 printf("%lf  %lf  %lf \n", _ref_pts[v][0], _ref_pts[v][1], _ref_pts[v][2]);
		

	}
	
	jac=_jac;
	if(normal)
	{
		normal[0]=_normal[0];
		normal[1]=_normal[1];
		normal[2]=_normal[2];
	}
}
