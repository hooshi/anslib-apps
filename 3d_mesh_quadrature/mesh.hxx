#ifndef MESH_HXX
#define MESH_HXX

#include "threed.hxx"

class Mesh
{

protected:
    // coordinates of the points
    double (*_pts)[3];

    // number of elements and points
    unsigned int _n_pts;
    unsigned int _n_elem;

    // type of elements
    CellType *_elem_type;

    // connectivity
    unsigned int (*_conn)[N_CELL2VERT_3D];

    mutable std::vector<bool> _is_bdry;
	
public:

    // Constructor
    Mesh():
        _pts(NULL),
        _n_elem(0),
        _elem_type(NULL),
        _conn(NULL)
        {}

    virtual ~Mesh()
        {
            if(_pts) delete[] _pts;
            if(_elem_type) delete[] _elem_type;
            if(_conn) delete[] _conn;
        }
	
    // virtual functions
    virtual void point_str2un(const uint[], uint&) = 0;
    virtual void point_un2str(const uint, uint[]) = 0;

    virtual void build_mesh(const uint[], const double[], const uint, double** =NULL) = 0;
    virtual void perturb_mesh(const double delta = 0.25) = 0;

    // write the mesh
    void write_vtk(const char[] = "mesh.vtk");


    // query functions
    virtual void get_point_coords(const uint v, double p[]) const
        {
            p[0] = _pts[v][0];
            p[1] = _pts[v][1];
            p[2] = _pts[v][2];			
        }
	
    void get_elem_coords(const uint e, double coords[][3]) const
        {
            for(uint i = 0 ; i < CellInfo(_elem_type[e]).n_verts() ; i++)
            {
                const uint v = _conn[e][i];
                get_point_coords(v, coords[i]);
            }
        }

    CellType get_elem_type(const int i) const
        {
            return _elem_type[i];
        }
	
    uint get_n_elem() const
        {return _n_elem;}

    uint get_n_vert() const
        {return _n_pts;}

    // check the orientation to be correct
    void check_orientation();
};

class CubeMesh: public Mesh
{
    // number of verts in each direction
    unsigned int _nx, _ny, _nz;

    // length of each edge
    double _lx, _ly, _lz;

public:

    CubeMesh():
        Mesh(),
        _nx(0), _ny(0), _nz(0),
        _lx(0), _ly(0), _lz(0)
        {}

    ~CubeMesh(){}

    void point_str2un(const uint[], uint&);
    void point_un2str(const uint, uint[]);

    void build_mesh(const uint[], const double[], const uint, double** =NULL);
    void build_elem_conn(const uint[], const CellType);

    void perturb_mesh(const double delta = 0.25);

	
    // write the CubeMesh in the grummp .mesh format
    // only supports hard coded boundary tag based on
    // center of the face.
    void write_grummp(const char[] , int (*)(const double[]) ) const;

    // read a mesh from gmsh
    void read_gmsh(const char[] = "mesh.vmesh");

};

class ShellMesh: public Mesh
{
    bool _is_periodic;
    uint _n_tet, _n_phi;
    double _r, _phi, _tet;

public:
    ShellMesh():
        _is_periodic(false),
        _n_tet(0), _n_phi(0),
        _r(0)
        {}

    ~ShellMesh(){}

    void point_str2un(const uint[], uint&);
    void point_un2str(const uint, uint[]);
	
    void build_mesh(const uint[], const double[], const uint, double** =NULL);
    void build_elem_conn(const uint[], const CellType);
	
    void perturb_mesh(const double delta = 0.25);

// querry functions

    void get_point_coords(const uint v, double p[]) const
        {
			
            const double tet = _pts[v][0];
            const double phi = _pts[v][1];

            const double x = _r * cos(phi) * cos(tet);
            const double y = _r * cos(phi) * sin(tet);
            const double z = _r * sin(phi);

            p[0] = x;
            p[1] = y;
            p[2] = z;

            // debug - plane surface
            //const double a = 1./sqrt(3.);
            //const double b = 1./sqrt(2.);
            //const double n0[] = {a, -a, a};
            //const double n1[] = {a*b, a/b, a*b};
            //const double n3[] = {-b, 0 , b};
            //p[0] = tet*n0[0] + phi*n1[0];
            //p[1] = tet*n0[1] + phi*n1[1];
            //p[2] = tet*n0[2] + phi*n1[2];
        }

	
};

#endif /* MESH_HXX*/
