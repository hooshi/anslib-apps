#include "mesh.hxx"
#include <cmath>
#include <string>
#include <cstring>
#include <vector>

#include "GetPot"


const int n_wx = 209;
const int	n_vx = 481;
const int	n_vy = 273;
const double s_xl = 1.04368;
const double s_xr = 1.03970;
const double s_y =  1.03970;
const double d0 = 1e-6;

int  flateplate_bdry_tag(const double cent[])
{
    // flat plate configuration
    const int character = 5;
    const int symmetry = 2;
    const int wall = 1;
				
    const double tol = 1e-12;
    const double x = cent[0];
    const double y = cent[1];
    const double z = cent[2];

    uint n_caught = 0;
    uint ans;
			
    // lower side
    if(y<tol)
    {
        // symmetry wall
        if(x<0)
        {
            ans=symmetry;
            n_caught++;
        }
        // flat plate
        if (x>0)
        {
            ans=wall;
            n_caught++;
        }
    }
    // upper side
    else if(fabs(y-1) < tol)
    {
        ans =  character;
        n_caught++;
    }
			
    // input and output
    if( (fabs(x+1/3.) < tol) || (fabs(x-2) < tol) )
    {
        ans =  character;
        n_caught++;
    }
			
    // perpendicular to z direction
    if( (fabs(z) < tol) || (fabs(z-1) < tol) )
    {
        ans =  symmetry;
        n_caught++;
    }

    // check that a bdry tag is assigned
    if(n_caught != 1)
    {
        printf("%e %e %e n_caught = %d \n ", x, y, z, n_caught);
        assert(n_caught == 1);
    }
    return ans;
			
    // all 1 configuration
    // return 1;
}

double p_series(const double q, const int j)
{
    return d0 * (pow(q,j)-1.)/(q-1.);
}

int main(int argc, char *argv[])
{

    GetPot getpot(argc, argv);

    // read the stuff required from input
    CubeMesh mesh;
    const uint clevel = getpot("clevel", 1);
    std::string out = getpot("out", "mesh");
    const int n_vz = getpot("n_ez", 7) + 1;

    /*
      set the coordinates of boundary points based
      on the clevel
    */
	
    // Check that the parameters are correct
    const double x_beg_0 = -2*p_series(s_xl, n_wx-1 );
    const double x_beg_1 = -2*p_series(s_xl, n_wx-2 );
    const double e_x_beg = fabs(-1/3. - x_beg_0) / (x_beg_1 - x_beg_0) * 100;

    const double  x_end_0 = 2*p_series(s_xr, n_vx-n_wx );
    const double x_end_1 = 2*p_series(s_xr, n_vx-n_wx-1 );
    const double e_x_end = fabs(2 - x_end_0) / (x_end_0 - x_end_1) * 100;

    const double y_end_0 = p_series(s_y, n_vy-1 );
    const double y_end_1 = p_series(s_y, n_vy-2 );
    const double e_y_end = fabs(1 - y_end_0) / (y_end_0 - y_end_1) * 100;

    printf("coursening level = %d \n", clevel);
    printf("Error in border lengths: \n");
    printf("x_beg: %lf %lf e: %.2lf%%\n", x_beg_0, x_beg_1, e_x_beg);
    printf("x_end: %lf %lf e: %.2lf%%\n", x_end_0, x_end_1, e_x_end);
    printf("y_end: %lf %lf e: %.2lf%%\n", y_end_0, y_end_1, e_y_end);

    // create the x and y coordinate arrays
    // create one extra member so that we can use
    // matlab indexing (original code came from matlab)
    std::vector<double> x(n_vx+1);
    std::vector<double> y(n_vy+1);
		
    x[1] = -1/3.;
    x[n_wx] = 0.; 
    x[n_vx] = 2.;

    y[1] = 0.;
    y[n_vy] = 1.;

    for (int i = 2 ; i <= (n_wx - 1) ; i++)
    {
        const int j =  n_wx - i;
        x[i] = -2*p_series(s_xl, j);
        // std::cout << x[i] << std::endl;
    }
	
    for (int i = n_wx + 1 ; i < n_vx ; i++)
    {
        const int j = i - n_wx ;
        x[i] = 2*p_series(s_xr, j);
        // std::cout << x[i] << std::endl;
    }

    for (int i = 2 ; i < n_vy ; i++)
    {
        const int j = i - 1;
        y[i] = p_series(s_y, j);
    }

    // now omit the extra coordinates and store everything 
    // in the g struct.
    assert((clevel <= 5) && (clevel >= 1));

    const int step = pow(2.,clevel-1);
    const int _n_wx = double(n_wx - 1)/ step + 1;
    const int _n_vx =  double(n_vx - n_wx)/ step + _n_wx;
    const int _n_vy = double(n_vy - 1)/ step + 1;
    const int _n_vz = n_vz;

    std::vector<double> _x(_n_vx+1);
    std::vector<double> _y(_n_vy+1);
    std::vector<double> _z(_n_vz+1);

    std::cout << "step " << step << std::endl;
    for (int i = 1 ; i <= _n_vx ; i++)
    {
        const int j = 1 + step*(i-1);
        _x[i] = x[j];
        // std::cout << _x[i] <<  std::endl;
    }
	
    for (int i = 1 ; i <= _n_vy ; i++)
    {
        const int j = 1 + step*(i-1);
        _y[i] = y[j];
        // std::cout << _y[i] <<  std::endl;
    }

    for(int i = 1 ; i <= _n_vz ; i++)
    {
        _z[i] = double(i-1) / (_n_vz-1);
    }

    // check that the arrays of x and y are created correctly.
    assert(_x[_n_wx] == 0.0) ;
    //printf("%.16f \n",  _x[_n_wx]);

    // print the grid size
    printf("Grid Size: %d x %d x %d\n", _n_vx-1, _n_vy-1, n_vz-1);


    /*
      Create the mesh and write it out
    */
    const uint sizes[] ={(uint)_n_vx,(uint)_n_vy,(uint)_n_vz};
    const double lengths[] = {1,1,1};
    double *coords[3] = {&_x[1], &_y[1], &_z[1]};
			
    mesh.build_mesh(sizes, lengths, HEX, coords);
	
    mesh.write_grummp((out+".vmesh").c_str(), flateplate_bdry_tag);
    mesh.write_vtk((out+".vtk").c_str());

    return 0;
}
