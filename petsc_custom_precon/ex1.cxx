#include <petscksp.h>
#include <vector>
#include <string>
#include <sstream>
#include <cassert>

#include "precon.hxx"

static char help[] = "Solves a tridiagonal linear system with KSP.\n\n";



// the size of otn must be set correctly
char seq_name[1000];
PetscErrorCode read_seq(std::vector<int>& otn)
{
	//PetscErrorCode ierr;

	// vector to check every one is present
	std::vector<bool> present(otn.size());
	std::fill(present.begin(), present.end(), false);
			  
	// check size to be set
	assert(otn.size());

	// read the otn
	std::stringstream stream;
	stream << seq_name << "_" << otn.size() << ".txt"; 
	FILE *f = fopen (stream.str().c_str(), "r");assert(f);

	std::vector<int>::iterator it = otn.begin();
	const std::vector<int>::iterator it_end = otn.end();
	for (; it != it_end ; ++it)
	{
		int n_read;
		n_read = fscanf(f, "%d", &(*it));assert(n_read);
		(*it)--; // index must start from zero
		present[*it] = true;
	}
	
	//final check			  
	for (uint i = 0 ; i < otn.size() ; i++)
	{
		assert(present[i]);
	}
 	
	fclose(f);

	return 0;
}


// Internal reordering for PC
PetscErrorCode reorder_internally(Mat A, MatOrderingType, IS* rperm, IS* cperm)
{
	PetscFunctionBegin;
	
	PetscErrorCode ierr;
	//IS &rp= *rperm;
	//IS &cp = *cperm;
	int m, n;

	// get the size
	ierr=MatGetSize(A, &m, &n);CHKERRQ(ierr);
	assert(m == n);

	// read the reordering
	std::vector<int> otn(n);
	read_seq(otn);
	
	// create the IS's
	std::vector<int> nto(n);
    
	for (int o = 0 ; o < n ; o++)
	{
		//printf("old:%d new:%d \n", o , otn[o]);
		nto[ otn[o] ] = o;
	}
	// for (int ne = 0 ; ne < n ; ne++)
	// {
	// 	printf("old:%d new:%d \n", nto[ne] , ne);
	// }
	
	ierr=ISCreateGeneral(PETSC_COMM_SELF, n, &nto.front(), PETSC_COPY_VALUES, rperm);CHKERRQ(ierr);
	ierr=ISSetPermutation(*rperm);
	ierr=ISDuplicate(*rperm, cperm);CHKERRQ(ierr);

	// create the reordering
	PetscFunctionReturn(0);
	return 0;
}

int main(int argc,char **args)
{
	Vec            x, b, u;      /* approx solution, RHS, exact solution */
	Mat            A;            /* linear system matrix */
	KSP            ksp;         /* linear solver context */
	PC             pc;           /* preconditioner context */
	PetscReal      norm;
	PetscErrorCode ierr;
	PetscInt       i,n = 10,col[3],its;
	PetscMPIInt    size;
	PetscScalar    neg_one      = -1.0,one = 1.0,value[3];
	PetscBool      nonzeroguess = PETSC_FALSE;
	PetscInt       reorder = 0;
	double diag = 2.05;

	//char           filename[300];

	/*
	  Initialize Petsc and read input
	*/
	PetscInitialize(&argc,&args,(char*)0,help);
	ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
	if (size != 1) SETERRQ(PETSC_COMM_WORLD,1,"This is a uniprocessor example only!");
	ierr = PetscOptionsGetInt(NULL,"-n",&n,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetBool(NULL,"-nonzero_guess",&nonzeroguess,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,"-reorder",&reorder,NULL);CHKERRQ(ierr);
	sprintf(seq_name, "seq");
	ierr = PetscOptionsGetString(NULL,"-seq_name",seq_name, 1000,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(NULL,"-diag",&diag,NULL);CHKERRQ(ierr);
	

	/*
	  Compute the matrix and right-hand-side vector that define
	  the linear system, Ax = b.
	*/
  
	// Create vectors.  Note that we form 1 vector from scratch and
	//  then duplicate as needed.
	ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
	ierr = PetscObjectSetName((PetscObject) x, "Solution");CHKERRQ(ierr);
	ierr = VecSetSizes(x,PETSC_DECIDE,n);CHKERRQ(ierr);
	ierr = VecSetFromOptions(x);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&b);CHKERRQ(ierr);
	ierr = VecDuplicate(x,&u);CHKERRQ(ierr);

  
	// Create matrix.  When using MatCreate(), the matrix format can
	// be specified at runtime.
	ierr = MatCreate(PETSC_COMM_WORLD,&A);CHKERRQ(ierr);
	ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,n,n);CHKERRQ(ierr);
	ierr = MatSetFromOptions(A);CHKERRQ(ierr);
	ierr = MatSetUp(A);CHKERRQ(ierr);

	// Assemble matrix
	value[0] = -1.0; value[1] = diag; value[2] = -1.0;
	for (i=1; i<n-1; i++)
	{
		col[0] = i-1; col[1] = i; col[2] = i+1;
		ierr   = MatSetValues(A,1,&i,3,col,value,INSERT_VALUES);CHKERRQ(ierr);
	}
	i    = n - 1; col[0] = n - 2; col[1] = n - 1;
	ierr = MatSetValues(A,1,&i,2,col,value,INSERT_VALUES);CHKERRQ(ierr);
	i    = 0; col[0] = 0; col[1] = 1; value[0] = diag; value[1] = -1.0;
	ierr = MatSetValues(A,1,&i,2,col,value,INSERT_VALUES);CHKERRQ(ierr);
  
	ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	// reorder if asked
	if (reorder == 1)
	{
		Mat B, C;
		std::vector<int> otn(n);
		read_seq(otn);
		reorder_mat(B, A, otn);

		// swap A and destroy the old one
		C = A;
		A = B;
		ierr = MatDestroy(&C); CHKERRQ(ierr);
		
		// view the matrix
		// ierr = MatView(A, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
    }
	
	// Set exact solution; then compute right-hand-side vector.
	ierr = VecSet(u,one);CHKERRQ(ierr);
	ierr = MatMult(A,u,b);CHKERRQ(ierr);

	/* 
	   Create the linear solver and set various 
	*/
  
	// Create linear solver context 
	ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);

	// Set the operators
	ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);

	// Set linear solver defaults for this problem (optional).
	ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
	ierr = PCSetType(pc,PCILU);CHKERRQ(ierr);
	ierr = PCFactorSetLevels(pc, 0);CHKERRQ(ierr);
	ierr = KSPSetTolerances(ksp,1.e-7,1e-10,PETSC_DEFAULT,5000);CHKERRQ(ierr);
	
	// reorder internally if asked
	switch(reorder)
	{
	case 1:
	{
		ierr=PCFactorSetMatOrderingType(pc , "natural");CHKERRQ(ierr);
		break;
	}
	case 2:
	{
		ierr=MatOrderingRegister("from_file" , reorder_internally);CHKERRQ(ierr);
		ierr=PCFactorSetMatOrderingType(pc , "from_file");CHKERRQ(ierr);
		//ierr=PCFactorSetReuseOrdering(_pc, PETSC_FALSE);CHKERRQ(ierr);
		break;
	}
    case 3:
	{
		ierr=Precon::register_precons();CHKERRQ(ierr);
		PetscOptionsSetValue("-pc_type", "pc_pc");
		ierr=PCSetType(pc, "pc_pc");CHKERRQ(ierr);

		std::vector<int> otn(n);
		read_seq(otn);

		PCInput inp;
		inp.A = &A;
		inp.otn = &otn;

		Precon *precon = (Precon*) pc->user;
		precon->init(&inp);
		break;
	}
	default:
		assert(0);
	}
			
	
	// Set runtime options
	ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);

	if (nonzeroguess)
	{
		PetscScalar p = .5;
		ierr = VecSet(x,p);CHKERRQ(ierr);
		ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);CHKERRQ(ierr);
	}

  
	//Set the convergence history
	int n_iter_max = 1000;
	double res[1000];

	ierr=KSPSetResidualHistory(ksp, res, n_iter_max, PETSC_TRUE);CHKERRQ(ierr);

	/*
	  Solve the linear system
	*/
  
	//Solve linear system
	ierr = KSPSolve(ksp,b,x);CHKERRQ(ierr);

	// view the ksp
	// ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

	// get convergence history
	int n_iter;
  
	ierr=KSPGetResidualHistory(ksp, NULL, &n_iter);CHKERRQ(ierr);

	printf("# n = %d \n", n);
	switch (reorder)
	{
	case 0:
		printf("# natural ordering (the best) \n");
		break;
	case 1:
		printf("# with reordering Bx_r=b_r\n");
		break;
	case 2:
		printf("# with internal reordering \n");
		break;
	case 3:
		printf("# a whole new precon \n");
		break;
	default:
		assert(0);
	}
	printf("# n_iter: %d \n", n_iter);
  
	for (i=0 ; i < n_iter ; i++)
	{
		printf("%.8e \n", res[i]);
	}

	/*
	  Check the solution and clean up
	*/

	// check the error
	ierr = VecAXPY(x,neg_one,u);CHKERRQ(ierr);
	ierr = VecNorm(x,NORM_2,&norm);CHKERRQ(ierr);
	ierr = KSPGetIterationNumber(ksp,&its);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"# Norm of error %g, Iterations %D\n",(double)norm,its);CHKERRQ(ierr);
 
	// free the workspace
	ierr = VecDestroy(&x);CHKERRQ(ierr);
	ierr = VecDestroy(&u);CHKERRQ(ierr);
	ierr = VecDestroy(&b);CHKERRQ(ierr);
	
	ierr = MatDestroy(&A);CHKERRQ(ierr);
	
	ierr = KSPDestroy(&ksp);CHKERRQ(ierr);


	// Finalize petsc
	ierr = PetscFinalize();
	return 0;
}
