#include "precon.hxx"
#include <iostream>
#include <stdio.h>


using std::cin;
using std::cout;
using std::endl;


PetscErrorCode reorder_mat(Mat& B, Mat A, std::vector<int>& otn)
{
	PetscErrorCode ierr;
	const int n = otn.size();
	
	// create the new to old vector
	// std::vector<int> nto(otn.size());
	// for (int iold=0 ; iold < otn.size ; iold++)
	// {
	// 	nto[ otn[i] ] = i;
	// }

	

	// create the new matrix
	ierr = MatCreate(PETSC_COMM_WORLD,&B);CHKERRQ(ierr);
	ierr = MatSetSizes(B,PETSC_DECIDE,PETSC_DECIDE,n,n);CHKERRQ(ierr);
	ierr = MatSetFromOptions(B);CHKERRQ(ierr);
	ierr = MatSetUp(B);CHKERRQ(ierr);

	/*
	  Assemble matrix
	*/
	for  (int i=0; i<n; i++)
	{
		int n_nonzero;
		int const* nonzero;
		double const* value;
		
		ierr = MatGetRow(A, i, &n_nonzero, &nonzero, &value);

		int nonzero_r[1000];
		int i_r = otn[i];
		for (int j = 0 ; j < n_nonzero ; j++)
			nonzero_r[j] = otn[nonzero[j]];
		
		ierr = MatSetValues(B,1,&i_r,n_nonzero,nonzero_r,value,INSERT_VALUES);CHKERRQ(ierr);
		
		ierr = MatRestoreRow(A, i,  &n_nonzero, &nonzero, &value);
	}

	ierr=MatAssemblyBegin(B, MAT_FINAL_ASSEMBLY);
	ierr=MatAssemblyEnd(B, MAT_FINAL_ASSEMBLY);

	return 0;
}


/****************************************************************************
 *                          Factory and Registration                        *
 ****************************************************************************/

PetscErrorCode Precon::create_precon(const string& name, Precon *& precon)
{
	//PetscErrorCode ierr;

	// set the name for each precon here
	if(name == "pc_pc") precon = new PreconPC;
	else
	{
		SETERRQ(PETSC_COMM_WORLD, 1, "developer bug: horrific bug in precon funcs.");
	}
	// ...

	return 0;
}

PetscErrorCode Precon::register_precons()
{
	PetscErrorCode ierr;

	// register all custom preconditioners here!
	ierr=PCRegister("pc_pc", Precon::create_pc_wrapper);CHKERRQ(ierr);
	// ...

	return 0;
}

/****************************************************************************
 *                           PETSc mandatory wrappers                       *
 ****************************************************************************/

PetscErrorCode Precon::create_pc_wrapper(PC pc)
{
	PetscErrorCode ierr;
	Precon *precon = (Precon*) NULL;

	 PetscFunctionBegin;

	// create the preconditioner
	{
		char name[300];
		PetscBool flag_name;
		ierr=PetscOptionsGetString((char*)NULL, "-pc_type", name, 300, &flag_name);CHKERRQ(ierr);
		assert(flag_name);
		Precon::create_precon(name, precon);CHKERRQ(ierr);
	}

	// ask the preconditioner to create the PC
	precon->_pc = pc;
	precon->create_pc();
	pc->user = precon;


	// divert the pc destroy function
	precon->_destroy_original = pc->ops->destroy;
	pc->ops->destroy = Precon::destroy_pc_wrapper;

	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::destroy_pc_wrapper(PC pc)
{
	PetscErrorCode ierr;

	// get the precon inside the pc
	Precon *precon = (Precon*)pc->user; assert(precon);
	// destroy the pc through the precon
	ierr=precon->destroy_pc();CHKERRQ(ierr);
	precon->_pc=NULL;
	// destroy the precon itself
	delete precon;

	PetscFunctionReturn(0);
	return 0;
}


/****************************************************************************
 *                           PETSc optional wrappers                       *
 ****************************************************************************/
PetscErrorCode Precon::setup_pc_wrapper(PC pc)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->setup_pc();CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::apply_pc_wrapper(PC pc,Vec x,Vec y)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->apply_pc(x ,y);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::applyrichardson_pc_wrapper(PC pc,Vec b,Vec x,Vec w,PetscReal rtol,PetscReal abstol, PetscReal dtol,PetscInt its,PetscBool zeroguess,PetscInt *outits,PCRichardsonConvergedReason *reason)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->applyrichardson_pc(b, x, w, rtol, abstol, dtol, its, zeroguess, outits, reason);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::applyBA_pc_wrapper(PC pc,PCSide side,Vec x,Vec y,Vec w)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->applyBA_pc(side, x, y, w);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::applytranspose_pc_wrapper(PC pc,Vec x,Vec y)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->applytranspose_pc(x, y);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::applyBAtranspose_pc_wrapper(PC pc,PetscInt n,Vec x,Vec y,Vec w)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->applyBAtranspose_pc(n, x, y, w);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::setfromoptions_pc_wrapper(PC pc)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->setfromoptions_pc();CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::presolve_pc_wrapper(PC pc, KSP ksp, Vec rhs, Vec x)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->presolve_pc(ksp, rhs, x);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::postsolve_pc_wrapper(PC pc, KSP ksp, Vec rhs, Vec x)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->postsolve_pc(ksp, rhs, x);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::getfactoredmatrix_pc_wrapper(PC pc,Mat* mat)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->getfactoredmatrix_pc(mat);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::applysymmetricleft_pc_wrapper(PC pc, Vec x, Vec y)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->applysymmetricleft_pc(x, y);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::applysymmetricright_pc_wrapper(PC pc, Vec x, Vec y)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->applysymmetricright_pc(x, y);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::setuponblocks_pc_wrapper(PC pc)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->setuponblocks_pc();CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::view_pc_wrapper(PC pc,PetscViewer viewer)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->view_pc(viewer);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::reset_pc_wrapper(PC pc)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->reset_pc();CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}

PetscErrorCode Precon::load_pc_wrapper(PC pc,PetscViewer viewer)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	Precon *precon = (Precon*)pc->user; assert(precon);
	ierr=precon->load_pc(viewer);CHKERRQ(ierr);
	PetscFunctionReturn(0);
	return 0;
}
