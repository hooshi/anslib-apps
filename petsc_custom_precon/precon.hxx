/*
 * precon.h
 *
 *  The main idea is to create a preconditioner framework using
 *  the Precon class.
 *
 *  hooshi
 */


#ifndef PRECON_PRECON_H
#define PRECON_PRECON_H

#include <petscsys.h>
#include <petscpc.h>
#include <assert.h>
#include <string>
#include <petsc/private/pcimpl.h>
//#include <private/pcimpl.h>


#include <vector>

using std::string;

// Reorders a matrix
// A the original matrix
// B the reordered matrix
// otn: otn[cv_old] = cv_new
PetscErrorCode reorder_mat(Mat& B, Mat A, std::vector<int>& otn);

/** --------------------------------------------------------------------------
 *  Preconditioner abstract class
 *  -------------------------------------------------------------------------*/
class Precon
{
public:

	/************************************************************************
	 *                          Factory and Registration                        *
	 ************************************************************************/

	// convert string to precon
	static PetscErrorCode create_precon(const string& name, Precon *& precon);

	// register all precons
	static PetscErrorCode register_precons();

	/***********************************************************************
	 *                           PETSc wrapper functions                   *
	 ************************************************************************/

	/* Create a pc containing a precon
	 * This function is registered into Petsc.
	 * It will:
	 * 1- Put the Precon inside the pc.
	 * 2- Ask the Precon to do its specific creation on pc.
	 * 3- Replace the destroy function of the pc, and also backs it up.
	 *    Note that the backup might store crap. It is up to the Precon
	 *    to use it or not.
	 */
	static PetscErrorCode create_pc_wrapper(PC pc);

	/* Destroy a pc containing a precon
	 * This function has to always replace PC::destroy because the
	 * native PETSc destroy does not destroy the Precon inside it.
	 * So the create_pc_wrapper replaces it instead of create_pc.
	 */
	static PetscErrorCode destroy_pc_wrapper(PC pc);

	// if need be then add (these functions do not necessarily have to
	// replace PC functions. It is up to Precon::create_pc to decide.)
	static PetscErrorCode setup_pc_wrapper(PC);
	static PetscErrorCode apply_pc_wrapper(PC,Vec,Vec);
	static PetscErrorCode applyrichardson_pc_wrapper(PC,Vec,Vec,Vec,PetscReal,PetscReal,PetscReal,PetscInt,PetscBool ,PetscInt*,PCRichardsonConvergedReason*);
	static PetscErrorCode applyBA_pc_wrapper(PC,PCSide,Vec,Vec,Vec);
	static PetscErrorCode applytranspose_pc_wrapper(PC,Vec,Vec);
	static PetscErrorCode applyBAtranspose_pc_wrapper(PC,PetscInt,Vec,Vec,Vec);
	static PetscErrorCode setfromoptions_pc_wrapper(PC);
	static PetscErrorCode presolve_pc_wrapper(PC,KSP,Vec,Vec);
	static PetscErrorCode postsolve_pc_wrapper(PC,KSP,Vec,Vec);
	static PetscErrorCode getfactoredmatrix_pc_wrapper(PC,Mat*);
	static PetscErrorCode applysymmetricleft_pc_wrapper(PC,Vec,Vec);
	static PetscErrorCode applysymmetricright_pc_wrapper(PC,Vec,Vec);
	static PetscErrorCode setuponblocks_pc_wrapper(PC);
	static PetscErrorCode view_pc_wrapper(PC,PetscViewer);
	static PetscErrorCode reset_pc_wrapper(PC);
	static PetscErrorCode load_pc_wrapper(PC,PetscViewer);


	/************************************************************************
	 *                             Member Functions                             *
	 *************************************************************************/

	Precon():
		_pc(NULL),
		_is_initialized(false),
		_destroy_original(NULL)
	{}

	virtual PetscErrorCode create_pc() = 0;
	virtual PetscErrorCode destroy_pc() = 0;

	// this one should be called after KSPSetOperators ...
	virtual PetscErrorCode init(void* inp)
		{
			_is_initialized = true;
			
			std::vector<int> &otn = *(std::vector<int>*) inp;
			_otn.resize(otn.size());
			std::copy(otn.begin(), otn.end(), _otn.begin());

			_nto.resize(_otn.size());
			for (uint o = 0 ; o < _otn.size() ; o++)
			{
				_nto[ _otn[o] ] = o;
			}

			return 0;
		}
	// this one should be called whenever you want to say
	// change the lines on which the solution is solved
	virtual PetscErrorCode reinit()	{ return 0;}


	// these guys cannot be pure virtual because the child class might
	// not want to use them at all, so it has to have some definition
	// ( which will never gonna be used ).
	virtual PetscErrorCode setup_pc()
		{ assert(0); return 0;}
	virtual PetscErrorCode apply_pc(Vec,Vec)
		{ assert(0); return 0;}
	virtual PetscErrorCode applyrichardson_pc(Vec,Vec,Vec,PetscReal,PetscReal,PetscReal,PetscInt,PetscBool ,PetscInt*,PCRichardsonConvergedReason*)		
		{ assert(0); return 0;}
	virtual PetscErrorCode applyBA_pc(PCSide,Vec,Vec,Vec)
		{ assert(0); return 0;}
	virtual PetscErrorCode applytranspose_pc(Vec,Vec)
		{ assert(0); return 0;}
	virtual PetscErrorCode applyBAtranspose_pc(PetscInt,Vec,Vec,Vec)
		{ assert(0); return 0;}
	virtual PetscErrorCode setfromoptions_pc()
		{ assert(0); return 0;}
	virtual PetscErrorCode presolve_pc(KSP,Vec,Vec)
		{ assert(0); return 0;}
	virtual PetscErrorCode postsolve_pc(KSP,Vec,Vec)
		{ assert(0); return 0;}
	virtual PetscErrorCode getfactoredmatrix_pc(Mat*)
		{ assert(0); return 0;}
	virtual PetscErrorCode applysymmetricleft_pc(Vec,Vec)
		{ assert(0); return 0;}
	virtual PetscErrorCode applysymmetricright_pc(Vec,Vec)
		{ assert(0); return 0;}
	virtual PetscErrorCode setuponblocks_pc()
		{ assert(0); return 0;}
	virtual PetscErrorCode view_pc(PetscViewer)
		{ assert(0); return 0;}
	virtual PetscErrorCode reset_pc()
		{ assert(0); return 0;}
	virtual PetscErrorCode load_pc(PetscViewer)
		{ assert(0); return 0;}

	virtual ~Precon() {}

	/****************************************************************************
	 *                                Data members                              *
	 ****************************************************************************/
	PC _pc;
	bool _is_initialized;
	PetscErrorCode (*_destroy_original) (PC pc);
	std::vector<int> _otn, _nto;

};

/*
 * PC containing another PC
 */


struct PCInput
{
	std::vector<int> *otn;
	Mat *A;
};
	
class PreconPC : public Precon
{
public:
	PreconPC(): _pc_a(NULL), _B(NULL), _vx(NULL), _vy(NULL)
		{}

	~PreconPC()
		{
			PetscErrorCode ierr;
			
			if(_pc_a)
			{
				ierr=PCDestroy(&_pc_a);CHKERRV(ierr);
			}
			if(_B)
			{
				ierr=MatDestroy(&_B);CHKERRV(ierr);
			}
			if(_vx)
			{
				ierr=VecDestroy(&_vx);CHKERRV(ierr);
			}
			if(_vy)
			{
				ierr=VecDestroy(&_vy);CHKERRV(ierr);
			}

		}
	
	PetscErrorCode create_pc()
		{
			// set the appropriate functions from the
			// petsc pc
			_pc->ops->setup = 0;
			_pc->ops->apply = Precon::apply_pc_wrapper;
			_pc->ops->applyrichardson = 0;
			_pc->ops->applyBA = 0;
			_pc->ops->applytranspose = 0;
			_pc->ops->applyBAtranspose = 0;
			_pc->ops->setfromoptions = 0;
			_pc->ops->presolve = 0;
			_pc->ops->postsolve = 0;
			_pc->ops->getfactoredmatrix = 0;
			_pc->ops->applysymmetricleft = 0;
			_pc->ops->applysymmetricright = 0;
			_pc->ops->setuponblocks = 0;
			// this is already set _pc->ops->destroy = 0;
			_pc->ops->view = 0;
			_pc->ops->load = 0;
			_pc->ops->reset = 0;

			return 0;
		}
	
	PetscErrorCode destroy_pc()
		{
			// nothing to do
			return 0;
		}

	virtual PetscErrorCode init(void* inp)
		{
			PetscErrorCode ierr;
			
			PCInput *inp_spec = (PCInput*) inp;
			
			// get the ordering
			Precon::init(inp_spec->otn);
			
			// create the matrix
			ierr=reorder_mat(_B, *inp_spec->A, _otn);CHKERRQ(ierr);

			// create the right hand side
			_datax.resize(_otn.size());
			_datay.resize(_otn.size());
			ierr=VecCreateSeqWithArray(PETSC_COMM_SELF, 1, _datax.size(), &_datax.front(), &_vx);CHKERRQ(ierr);
			ierr=VecCreateSeqWithArray(PETSC_COMM_SELF, 1, _datay.size(), &_datay.front(), &_vy);CHKERRQ(ierr);
			ierr=VecSet(_vx,1);CHKERRQ(ierr);
			ierr=VecSet(_vy,1);CHKERRQ(ierr);

			
			// create the PC
			ierr = PCCreate(PETSC_COMM_SELF, &_pc_a);CHKERRQ(ierr);
			ierr = PCSetType(_pc_a, "ilu");CHKERRQ(ierr);
			
			int fill = 0;
			ierr=PetscOptionsGetInt(NULL, "-pc_factor_levels", &fill, NULL);CHKERRQ(ierr);
			ierr = PCFactorSetLevels(_pc_a, fill);CHKERRQ(ierr);
			
			ierr = PCSetOperators(_pc_a, _B, _B);
			ierr = PCFactorSetMatOrderingType(_pc_a, "natural");

			return 0;
		}


	// these guys cannot be pure virtual because the child class might
	// not want to use them at all, so it has to have some definition
	// ( which will never gonna be used ).
	// virtual PetscErrorCode setup_pc()
	// 	{ assert(0); return 0;}
	
	// converts x to y
	PetscErrorCode apply_pc(Vec x,Vec y)
	{
		PetscErrorCode ierr;

		// make sure is initialized
		if(!_is_initialized)
		{
			SETERRQ(PETSC_COMM_SELF, 1, "PreconPC->init() has not been called");
		}

		// convert input to our numbering
		double const* x_a;
		ierr=VecGetArrayRead(x, &x_a);CHKERRQ(ierr);
		ierr=VecSetValues(_vx, _otn.size(), &_otn.front(), x_a, INSERT_VALUES);CHKERRQ(ierr);
		ierr=VecAssemblyBegin(_vx);
		ierr=VecAssemblyEnd(_vx); 	
		ierr=VecRestoreArrayRead(x, &x_a);CHKERRQ(ierr);

		// apply the pc
		ierr = PCApply(_pc_a, _vx, _vy);CHKERRQ(ierr);

		// copy back to Y
		ierr=VecSetValues(y, _nto.size(), &_nto.front(), &_datay.front(), INSERT_VALUES);CHKERRQ(ierr);
		ierr=VecAssemblyBegin(y);
		ierr=VecAssemblyEnd(y);

		return 0;
	}
	
	// virtual PetscErrorCode applyrichardson_pc(Vec,Vec,Vec,PetscReal,PetscReal,PetscReal,PetscInt,PetscBool ,PetscInt*,PCRichardsonConvergedReason*)		
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode applyBA_pc(PCSide,Vec,Vec,Vec)
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode applytranspose_pc(Vec,Vec)
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode applyBAtranspose_pc(PetscInt,Vec,Vec,Vec)
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode setfromoptions_pc()
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode presolve_pc(KSP,Vec,Vec)
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode postsolve_pc(KSP,Vec,Vec)
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode getfactoredmatrix_pc(Mat*)
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode applysymmetricleft_pc(Vec,Vec)
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode applysymmetricright_pc(Vec,Vec)
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode setuponblocks_pc()
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode view_pc(PetscViewer)
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode reset_pc()
	// 	{ assert(0); return 0;}
	// virtual PetscErrorCode load_pc(PetscViewer)
	// 	{ assert(0); return 0;}

	/****************************************************************************
	 *                                Data members                              *
	 ****************************************************************************/
	// The active pc
	PC _pc_a;
	Mat _B;
	Vec _vx, _vy;	
	std::vector<PetscScalar> _datax, _datay;
};



#endif /* PRECON_PRECON_H */
