## An octave code to plot convergence for different cases. 


## Some useful aliasing
C = 'color';
c = 'brgkmcy';
ST = 'linestyle';
st = {'-', '--', '-.'};
LW = 'linewidth';

## lets plot n=10000 and diag=2
n = '1000';
diag = '0';
seq = '0';

## -----------------------------------------
## compare three different numberings
## ----------------------------------------
names = {['result/hist_n', n, '_diag', diag, '_rord1_seq1'], ...
		 ['result/hist_n', n, '_diag', diag, '_rord1_seq2'], ...
		 ['result/hist_n', n, '_diag', diag, '_rord1_seq3']};
 
hist{1} = load(names{1});
hist{2} = load(names{2});
hist{3} = load(names{3});

figure(1), clf; hold on;
for i= 1:3
	m = min(length(hist{1}), length(hist{i}) );
	semilogy(abs(hist{i}(1:m)-hist{1}(1:m))+1e-13, LW, 2, C, c(i), ST, st{i});
endfor

title('Three different reorderings, inital reordering method');

## -----------------------------------------
## for the second numbering compare three methods
## ----------------------------------------
names = {['result/hist_n', n, '_diag', diag, '_rord1_seq', seq], ...
		 ['result/hist_n', n, '_diag', diag, '_rord2_seq',  seq], ...
		 ['result/hist_n', n, '_diag', diag, '_rord3_seq',  seq]};
 
hist{1} = load(names{1});
hist{2} = load(names{2});
hist{3} = load(names{3});

figure(2), clf; hold on;
for i= 1:3
  m = min(length(hist{1}), length(hist{i}) );
   semilogy(abs(hist{i}(1:m)-hist{1}(1:m))+1e-13, LW, 2, C, c(i), ST, st{i});
endfor

legend('init reorder', 'mat reorder', 'pc in pc');
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northwest", "linewidth",2);

#hist3 = load('--asci', names(1));
