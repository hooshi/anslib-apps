#!/bin/bash

# script to run and compare the preconditioners

# naming the out put is like
# hist_nXX_diagXX_rordXX_seqXX.txt

# counters
TWO="0 1"

# the parameters
N="1000 10000"
DIAG=(2 2.1)
REOR="1 2 3"
SEQ="1 2 3"
 
# delete previous parameters
rm -rf result/*

# run the cases
for iN in $N; do 
	for iDIAG in $TWO; do 
		for iREOR in $REOR; do
			for iSEQ in $SEQ; do

				COMMAND="./ex1 -n $iN -diag ${DIAG[iDIAG]} -reorder $iREOR -seq_name seq/seq${iSEQ}"
				FILE=result/hist_n${iN}_diag${iDIAG}_rord${iREOR}_seq${iSEQ}
				
				echo "Running $COMMAND"
				echo "FILE $FILE"
				echo " ----------------------- "
				$COMMAND > $FILE

			done
		done
	done
done

